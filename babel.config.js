module.exports = (api) => {
  // Cache configuration is a required option
  api.cache(false);

  const presets = [
    [ "@babel/preset-env", { useBuiltIns: "usage", corejs: { version: 3 } } ]
  ];

  const plugins = [
    [ "module-resolver", {
      "root": [ "./" ],
      "alias": {
        "@lms": "./src"
      }
    }]
  ];

  return { presets, plugins };
};
