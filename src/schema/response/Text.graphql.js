const { gql } = require('apollo-server');

module.exports = gql`
  type TextResponse implements Response {
    id: ID!
    sectionId: ID!
    lessonId: ID!
    organizationId: ID!
    type: String!
    rev: Int

    createdAt: String
    updatedAt: String

    data: String
  }
`;
