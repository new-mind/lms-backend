const { gql } = require('apollo-server');

module.exports = gql`
  extend type Query {
    getOrganization(id: ID!): Organization
    getOrganizations(ids: [ID!]!): [Organization!]
  }
  extend type Mutation {
    createOrganization(input: CreateOrganizationInput): Organization
  }
  scalar OrganizationID
  type Organization {
    id: OrganizationID!
    title: String
    courses: CourseConnection
    createdAt: String
    updatedAt: String
  }
  input CreateOrganizationInput {
    title: String!
  }
`;
