const { gql } = require('apollo-server');

module.exports = gql`
  type MultiChoiceWidget {
    id: ID!
    title: String
    question: String
    imageKey: String
    answers: [MultiChoiceAnswer!]
  }
  type MultiChoiceAnswer {
    id: ID!
    correct: Boolean
    text: String
  }
  input MultiChoiceAnswerInput {
    id: ID!
    text: String
    correct: Boolean
  }
`;
