const { gql } = require('apollo-server');

module.exports = gql`
  enum ResponseFormType {
    TextWidget
    LinkWidget
    AttachmentWidget
  }

  type ResponseFormWidget {
    id: ID!
    title: String
    formType: ResponseFormType

    answers(userId: ID, lessonId: ID): [StudentAnswer_ResponseFormWidget!] #TODO
    responses: [Response!]
  }
`;
