const { gql } = require('apollo-server');

module.exports = gql`
  type LinkWidget {
    id: ID!
    title: String
    items: [LinkItem!]
  }
  type LinkItem {
    id: ID!
    url: String
    title: String
  }
  input LinkItemInput {
    id: ID!
    url: String!
    title: String
  }
`;
