const { gql } = require('apollo-server');

module.exports = gql`
  type ImageWidget {
    id: ID!
    title: String
    url: String
    width: String
    height: String
  }
`
