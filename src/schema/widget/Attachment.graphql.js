const { gql } = require('apollo-server');

module.exports = gql`
  type AttachmentWidget {
    id: ID!
    title: String
    items: [AttachmentItem!]
  }
  type AttachmentItem {
    id: ID!
    identityId: String
    fileKey: String
    fileName: String
    title: String
  }
  input AttachmentItemInput {
    id: ID!
    identityId: String
    fileKey: String!
    fileName: String
    title: String
  }
`;
