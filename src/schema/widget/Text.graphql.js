const { gql } = require('apollo-server');

module.exports = gql`
  type TextWidget {
    id: ID!
    title: String
    data: String
  }
`
