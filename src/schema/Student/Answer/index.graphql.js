const { gql } = require('apollo-server');

module.exports = gql`
  extend type Mutation {
    updateStudentAnswer(input: UpdateStudentAnswer): StudentAnswer
  }

  union StudentAnswer =
    StudentAnswer_MultiChoiceWidget      |

    # TODO: cannot include union in union
    # StudentAnswer_ResponseFormWidget
    StudentAnswer_AttachmentWidget       |
    StudentAnswer_LinkWidget             |
    StudentAnswer_TextWidget
  
  input UpdateStudentAnswer {
    type: String!,
    organizationId: ID!
    userId: ID!
    sectionId: ID!
    lessonId: ID!

    # ResponseFormWidget
    rev: Int
    # TextWidget
    data: String
    # LinkWidget
    # VideoWidget
    items: [WidgetItemInput]
  }
`;
