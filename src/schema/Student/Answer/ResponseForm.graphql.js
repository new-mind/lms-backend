const { gql } = require('apollo-server');

module.exports = gql`
  # TODO: cannot include union in union
  union StudentAnswer_ResponseFormWidget =
    StudentAnswer_AttachmentWidget  |
    StudentAnswer_LinkWidget        |
    StudentAnswer_TextWidget

  type StudentAnswer_AttachmentWidget {
    id: ID!
    sectionId: ID!
    lessonId: ID!
    organizationId: ID!
    rev: Int

    createdAt: String
    updatedAt: String

    items: [AttachmentItem!]
  }

  type StudentAnswer_LinkWidget {
    id: ID!
    sectionId: ID!
    lessonId: ID!
    organizationId: ID!
    rev: Int

    createdAt: String
    updatedAt: String

    items: [LinkItem!]
  }

  type StudentAnswer_TextWidget {
    id: ID!
    sectionId: ID!
    lessonId: ID!
    organizationId: ID!
    rev: Int

    createdAt: String
    updatedAt: String

    data: String
  }
`;
