const { gql } = require('apollo-server');

module.exports = gql`
  type StudentAnswer_MultiChoiceWidget {
    id: ID!
  }
`;
