const { gql } = require('apollo-server');

module.exports = gql`
  extend type Mutation {
    addLessonComment(input: CreateStudentLessonCommentInput): StudentLessonComment
  }

  type StudentLessonComment {
    id: ID!
    organizationId: ID!

    createdAt: String
    updatedAt: String
    content: String
    userId: ID!
    author: User
  }

  type StudentLessonCommentConnection {
    items: [StudentLessonComment!]
    nextToken: String
  }

  input CreateStudentLessonCommentInput {
    id: ID
    userId: ID!
    lessonId: ID!
    organizationId: ID!
    content: String!
  }
`
