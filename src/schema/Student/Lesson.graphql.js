const { gql } = require('apollo-server');

module.exports = gql`
  extend type Query {
    getStudentLesson(id: ID!, organizationId: ID!): StudentLesson
  }

  extend type Mutation {
    submitStudentLesson(id: ID!, organizationId: ID!): StudentLesson
    acceptStudentLesson(id: ID!, organizationId: ID!): StudentLesson
    reopenStudentLesson(id: ID!, organizationId: ID!): StudentLesson
  }

  type StudentLesson {
    id: ID!
    courseId: ID!
    lessonId: ID!
    organizationId: ID!

    title: String
    description: String
    homework: Boolean
    updatedAt: String
    createdAt: String
    status: String
    rev: Int

    content: [Section!]
    course: Course
    student: Student
    comments(limit: Int, nextToken: String): StudentLessonCommentConnection
  }
`
