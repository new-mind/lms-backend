const { gql } = require('apollo-server');

module.exports = gql`
  extend type Query {
    getStudent(id: ID!): Student
    listStudents(filter: StudentFilterInput, limit: Int, nextToken: String): StudentConnection!
    Student(id: ID!): Student
  }
  extend type Mutation {
    inviteStudent(input: InviteStudentInput): Student
    inviteStudents(input: InviteStudentsInput): [Student!]
    updateStudent(input: UpdateStudentInput): Student
    deleteStudent(input: DeleteStudentInput): Student
    Student(id: ID!): Student
  }
  type Student implements IUser {
    id: ID!
    email: String!

    createdAt: String
    updatedAt: String

    name: String
    surname: String
    phone: String
    age: String

    courses(filter: CourseFilterInput, limit: Int, nextToken: String): CourseConnection!
    courseIds(filter: CourseFilterInput, limit: Int, nextToken: String): [ID!]
    homeworks: [StudentLesson!]
    homeworksInOrg(organizationId: ID): [StudentLesson!]

    getCourse(id: ID!): Course
    getLesson(id: ID!, organizationId: ID): StudentLesson

    addCourses(input: [ID!]): [ID!]
    addLessons(input: [ID!]): [StudentLesson!]
    deleteCourses(input: [ID!]): [ID!]
    deleteLessons(input: [ID!]): [ID!]
  }
  type StudentConnection {
    items: [Student!]
    nextToken: String
  }
  input InviteStudentInput {
    organizationId: ID!
    email: String!
    courses: [ID!]
  }
  input InviteStudentsInput {
    organizationId: ID!
    emails: [String!]!
    courses: [ID!]
  }
  input UpdateStudentInput {
    id: ID!
    name: String
    surname: String
    phone: String
    age: String
  }
  input DeleteStudentInput {
    organizationId: ID!
    id: ID!
  }
  input StudentFilterInput {
    courseId: String
    organizationId: String
  }
`
