const { gql } = require('apollo-server');

module.exports = gql`
  extend type Query {
    getCourse(id: ID!): Course
    listCourses(filter: CourseFilterInput, limit: Int, nextToken: String): CourseConnection!
  }
  extend type Mutation {
    deleteCourse(id: ID!): Course
    createCourse(input: CreateCourseInput): Course
    updateCourse(input: UpdateCourseInput): Course

    Course(id: ID!): Course
  }
  type Course {
    id: ID!
    organizationId: ID!
    title: String!
    description: String
    owner: String
    lessons: LessonConnection
    teachers: [String!]
    imageUrl: String
    students: [Student!]

    createdAt: String
    updatedAt: String

    moveLesson(id: ID!, position: Int!): LessonWithPos
  }
  type CourseConnection {
    items: [Course!]
    nextToken: String
  }
  input CourseFilterInput {
    organizationId: ID!
  }
  input CreateCourseInput {
    id: ID
    organizationId: ID!
    title: String!
    description: String
    imageUrl: String
  }
  input UpdateCourseInput {
    id: ID!
    organizationId: ID!
    title: String
    description: String
    imageUrl: String
  }
`;
