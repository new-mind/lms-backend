const { gql } = require('apollo-server');

module.exports = gql`
  extend type Query {
    listLessons(filter: LessonFilterInput, limit: Int, nextToken: String): LessonConnection!
    getLesson(id: ID!): Lesson
  }
  extend type Mutation {
    deleteLesson(id: ID!): Lesson
    createLesson(input: CreateLessonInput): Lesson
    updateLesson(input: UpdateLessonInput): Lesson
    publishLesson(id: ID!): Lesson
    moveLesson(id: ID!, position: Int!): LessonWithPos

    Lesson(id: ID!): Lesson
  }
  type Lesson {
    id: ID!
    courseId: ID!
    organizationId: ID
    title: String
    description: String
    homework: Boolean
    updatedAt: String
    createdAt: String
    dirty: Boolean
    rev: Int

    content: [Section!]
    course: Course!
    students: [Student!]

    addSection(input: CreateSectionWithPosInput): SectionWithPos
    moveSection(id: ID!, position: Int): SectionWithPos
    deleteSection(id: ID!): SectionWithPos
    addStudents(students: [ID!]): [ID!]

    # StudentLesson
    lessonId: ID
  }
  type LessonWithPos {
    lesson: Lesson!
    position: Int
  }
  type AssignedLesson {
    id: ID!
  }
  type LessonConnection {
    items: [Lesson!]
    nextToken: String
  }
  input LessonFilterInput {
    courseId: ID
  }
  input CreateLessonInput {
    id: ID
    courseId: ID!
    title: String!
    description: String
    homework: Boolean
  }
  input UpdateLessonInput {
    id: ID!
    organizationId: ID!
    title: String
    description: String
    homework: Boolean
  }
`
