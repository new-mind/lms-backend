const { gql } = require('apollo-server');

module.exports = {
  TextWidget: gql`
    fragment TextWidget on TextWidget {
      id
      title
      data
    }
  `,

  VideoWidget: gql`
    fragment VideoWidget on VideoWidget {
      id
      title
      url
      width
      height
    }
  `,

  LinkWidget: gql`
    fragment LinkWidget on LinkWidget {
      id
      title
      items {
        id
        url
        title
      }
    }
  `,

  MultiChoiceWidget: gql`
    fragment MultiChoiceWidget on MultiChoiceWidget {
      id
      title
      question
      imageKey
      answers {
        id
        correct
        text
      }
    }
  `,

  AttachmentWidget: gql`
    fragment AttachmentWidget on AttachmentWidget {
      id
      title
      items {
        id
        identityId
        fileKey
        fileName
        title
      }
    }
  `,

  ImageWidget: gql`
    fragment ImageWidget on ImageWidget {
      id
      title
      url
      width
      height
    }
  `,

  ResponseFormWidget: gql`
    fragment ResponseFormWidget on ResponseFormWidget {
      id
      title
      formType
    }
  `
}
