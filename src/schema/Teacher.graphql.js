const { gql } = require('apollo-server');

module.exports = gql`
  extend type Query {
    getTeacher(id: ID!): Teacher
    listTeachers(filter: TeacherFilterInput, limit: Int, nextToken: String): TeacherConnection!
  }

  extend type Mutation {
    inviteTeacher(input: InviteTeacherInput): Teacher
    deleteTeacher(input: DeleteTeacherInput): Teacher
  }
  type Teacher implements IUser {
    id: ID!
    email: String!

    createdAt: String
    updatedAt: String

    name: String
    surname: String
    phone: String
    age: String

    courses: CourseConnection
  }
  type TeacherConnection {
    items: [Teacher!]
    nextToken: String
  }
  input TeacherFilterInput {
    organizationId: ID!
  }
  input InviteTeacherInput {
    organizationId: ID!
    email: String
  }
  input DeleteTeacherInput {
    organizationId: ID!
    id: ID!
  }
`;
