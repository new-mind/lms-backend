const { gql } = require('apollo-server');

module.exports = gql`
  extend type Mutation {
    updateSection(input: UpdateSectionInput): Section
  }
  union Section =
    AttachmentWidget   |
    LinkWidget         |
    MultiChoiceWidget  |
    ImageWidget        |
    ResponseFormWidget |
    TextWidget         |
    VideoWidget
  type SectionWithPos {
    section: Section
    position: Int
  }
  extend type Query {
    getSection(id: ID!): Section
  }
  type ModelSectionConnection {
    items: [Section]
    nextToken: String
  }
  input SectionFilterInput {
    lessonContentId: ID
  }
  enum SectionType {
    AttachmentWidget
    ImageWidget
    TextWidget
    LinkWidget
    MultiChoiceWidget
    VideoWidget
    ResponseFormWidget
  }
  # TODO
  input WidgetItemInput {
    id: ID!
    title: String
    # LinkItemInput
    url: String
    # AttachmentItemInput
    identityId: String
    fileKey: String
    fileName: String
  }
  # TODO
  input CreateSectionInput {
    id: ID
    lessonId: String
    organizationId: String
    title: String

    type: SectionType!
    # Text
    data: String
    # Video # Image
    url: String
    width: String
    height: String
    # Link
    items: [WidgetItemInput]
    # ResponseForm
    formType: ResponseFormType
    # MultiChoice
    imageKey: String
    question: String
    answers: [MultiChoiceAnswerInput]
  }
  input CreateSectionWithPosInput {
    position: Int
    section: CreateSectionInput!
  }
  input UpdateSectionInput {
    id: ID!
    lessonId: ID!
    organizationId: ID!
    type: SectionType!
    title: String

    # Text
    data: String
    # Image
    url: String
    width: String
    height: String
    # Link # Video
    items: [WidgetItemInput]
    # ResponseForm
    formType: ResponseFormType
    # MultiChoice
    imageKey: String
    question: String
    answers: [MultiChoiceAnswerInput]
  }
`;
