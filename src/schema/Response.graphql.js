const { gql } = require('apollo-server');

module.exports = gql`
  extend type Mutation {
    updateResponse(input: UpdateResponse): Response
  }

  interface Response {
    id: ID!
    sectionId: ID!
    lessonId: ID!
    organizationId: ID!
    type: String!
    rev: Int

    createdAt: String
    updatedAt: String
  }

  input UpdateResponse {
    sectionId: ID!
    lessonId: ID!
    organizationId: ID!
    rev: Int!
    type: String!

    # Text
    data: String
    # Link # Video
    items: [WidgetItemInput]
  }
`
