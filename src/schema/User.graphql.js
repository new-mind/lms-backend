const { gql } = require('apollo-server');

module.exports = gql`
  extend type Query {
    getRoles(id: ID!): UserRoles
    getUser(id: ID!): User
  }
  extend type Mutation {
    updateUser(input: UpdateUserInput): User
  }
  type User implements IUser {
    id: ID!
    email: String!

    createdAt: String
    updatedAt: String

    name: String
    surname: String
    phone: String
    age: String
  }
  interface IUser {
    id: ID!
    email: String!

    createdAt: String
    updatedAt: String

    name: String
    surname: String
    phone: String
    age: String
  }
  type UserRoles {
    admin: [OrganizationID!]
    teacher: [OrganizationID!]
    student: [OrganizationID!]
  }
  input UpdateUserInput {
    id: ID!
    name: String
    surname: String
    phone: String
    age: String
  }
`;
