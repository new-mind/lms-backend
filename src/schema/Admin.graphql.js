const { gql } = require('apollo-server');

module.exports = gql`
  extend type Mutation {
    inviteAdmin(email: String!, organizationId: ID!): Admin
    reinviteAdmin(email: String!, organizationId: ID!): Admin
  }

  type Admin implements IUser {
    id: ID!
    email: String!

    createdAt: String
    updatedAt: String

    name: String
    surname: String
    phone: String
    age: String
  }
`;
