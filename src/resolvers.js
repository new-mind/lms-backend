const _ = require('lodash');
const path = require('path');

const modules = [
  'Admin',
  'Course',
  'Lesson',
  'Student',
  'Student/Answer',
  'Student/Lesson',
  'Student/LessonComment',
  'User',
  'Section',
  'Organization',
  'Teacher',
  'Response',
  'widgets/ResponseForm',
]

const resolvers = _.reduce(modules, (acc, name) => {
  const {Query, Mutation, Resolvers} = require(path.resolve(__dirname, 'resolvers', name));
  acc.Query = _.extend(acc.Query, Query);
  acc.Mutation = _.extend(acc.Mutation, Mutation);
  if (Resolvers) {
    const moduleName = Resolvers.__name || _.last(name.split('/')); //TODO
    acc[moduleName] = Resolvers;
  }
  return acc;
}, {});


async function dev() {
  return '';
}
_.extend(resolvers.Query, { dev });

module.exports = resolvers;
