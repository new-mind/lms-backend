const winston = require('winston');
const config = require('config');
const util = require('util');
const { format } = require('logform');

const splat = Symbol.for('splat');

const logger = winston.createLogger({
  level: config.logLevel,
  format:  format.combine(
    format.errors({ stack: true }),
    format.cli(),
    format.printf(info => {
      const s = info[splat];
      if (!s) {
        if (!info.stack) {
          return `${info.level} ${info.message}`;
        } else {
          return `${info.level} ${info.stack}`;
        }
      }

      return `${info.level} ${info.message} \n${util.inspect(s, {colors: true})}`;
    })
  ),
  transports: [
    new winston.transports.Console(),
  ]
});

module.exports = logger;
