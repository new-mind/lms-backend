const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');
const uuid = require('uuid/v4');
const moment = require('moment');

const Model = require('./base');
const Course = require('./Course');
const Section = require('./Section');
const Student = require('./User/Student');
const logger = require('../logger');

class Lesson extends Model {
  static get spec() {
    return {
      id:             { type: "S", readOnly: true, dynamoDBKey: "lessonId" },
      organizationId: { type: "S", readOnly: true },
      courseId:       { type: "S", readOnly: true },
      title:          { type: "S", required: true },
      homework:       { type: "BOOL", required: true },
      description:    { type: "S" },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S" },

      content:        { type: "L", field: "S", readOnly: true },
      dirty:          { type: "BOOL" },
      rev:            { type: "N", readOnly: true }
    }
  }

  /**
   * @input {CreateSectionInput}
   */
  async addSection(input = {}) {
    const section = await Section
      .config(this.ctx)
      .create({
        ...input.section,
        lessonId: this.id,
        organizationId: this.organizationId
      });

    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": this.organizationId },
        SK:             { "S": `Lesson#${this.id}` },
      },
      AttributesToGet:  [ "content" ]
    }

    logger.info('Lesson.addSection.getItem [DynamoDB]', params);
    let res = await this.dynamodb.getItem(params).promise();
    const content = _.get(res, 'Item.content.L', []);
    let position = input.position || 0;
    if (position < 0) position = 0;
    if (position >= content.length) position = content.length;

    content.splice(position, 0, {"S": section.id});

    const updateParams = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": this.organizationId },
        SK:             { "S": `Lesson#${this.id}` }
      },
      ExpressionAttributeValues: {
        ":content":   { "L": content },
        ":dirty":     { "BOOL": true },
        ":updatedAt": { "S": moment().format() }
      },
      UpdateExpression: "SET content = :content, updatedAt = :updatedAt, dirty = :dirty"
    }
    logger.info('Lesson.addSection.updateItem [DynamoDB]', updateParams);
    res = await this.dynamodb.updateItem(updateParams).promise();
    return [section, position];
  }

  async addStudent(id) {
    const student = await new Student({id})
      .config(this.ctx)
      .addLesson(this);

    return student;
  }

  async moveSection(id, position) {
    let content = this.content || [];
    let oldPosition = _.indexOf(content, id);
    if (oldPosition === -1) {
      throw new Error(`Section doesn't exist ${id}`);
    }
    content.splice(oldPosition, 1);
    content.splice(position, 0, id);

    const organizationId = this.organizationId;
    const lessonId = this.id;
    const updateParams = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": organizationId },
        SK:             { "S": `Lesson#${lessonId}` }
      },
      ExpressionAttributeValues: {
        ":content":  { "L": _.map(content, c => ({"S": c})) },
        ":dirty":     { "BOOL": true },
        ":updatedAt": { "S": moment().format() }
      },
      UpdateExpression: "SET content = :content, updatedAt = :updatedAt, dirty = :dirty"
    }

    logger.info('Lesson.moveSection [DynamoDB]', updateParams);
    const [ res, section ] = await Promise.all([
      this.dynamodb.updateItem(updateParams).promise(),
      Section.config(this.ctx).findOne({organizationId, lessonId, id})
    ]);

    return [section, position];
  }

  async deleteSection(id) {
    let position = null;
    let content = this.content || [];
    for (let i = 0; i < content.length; i++) {
      if (this.content[i] === id) {
        position = i;
        break;
      }
    }
    if (_.isNull(position)) {
      throw new Error(`Section doesn't exist ${id}`);
    }
    content.splice(position, 1);

    const updateParams = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": this.organizationId },
        SK:             { "S": `Lesson#${this.id}` }
      },
      ExpressionAttributeValues: {
        ":content":  { "L": _.map(content, c => ({"S": c})) },
        ":dirty":     { "BOOL": true },
        ":updatedAt": { "S": moment().format() }
      },
      UpdateExpression: "SET content = :content, updatedAt = :updatedAt, dirty = :dirty"
    }
    logger.info('Lesson.deleteSection.updateItem [DynamoDB]', updateParams);

    const deleteParams = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": this.organizationId },
        SK:             { "S": `Lesson#${this.id}#Section#${id}` },
      },
      ReturnValues: "ALL_OLD"
    }
    logger.info('Lesson.deleteSection.deleteItem [DynamoDB]', deleteParams);

    const [__, res] = await Promise.all([
      this.dynamodb.updateItem(updateParams).promise(),
      this.dynamodb.deleteItem(deleteParams).promise()
    ]);

    const section = Section
      .config(this.ctx)
      .fromDynamoDB(res.Attributes);
    return [section, position];
  }

  async getContent() {
    const params = {
      TableName: config.db.tableName,
      ExpressionAttributeValues: {
        ":id": { S: this.organizationId },
        ":SK": { S: `Lesson#${this.id}#Section#` }
      },
      KeyConditionExpression: "organizationId = :id AND begins_with ( SK, :SK )"
    };

    logger.info('Lesson.getContent [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    const dict = _.chain(res)
      .get('Items', [])
      .map(item => Section.fromDynamoDB(item))
      .keyBy(item => item.id)
      .value();

    const sections = [];
    _.forEach(this.content, id => {
      const section = dict[id];
      if (!section) return;

      sections.push(section.serialize());
    });
    return sections;
  }

  async getContentForStudent(userId) {
    return this.getContent();
  }

  async listStudents() {
    const params = {
      TableName: config.db.tableName,
      ExpressionAttributeNames: {
        "#id": "organizationId",
        "#SK": "SK"
      },
      ExpressionAttributeValues: {
        ":id": { "S": this.organizationId },
        ":SK": { "S": `LessonAssignment#${this.id}#Student#` }
      },
      KeyConditionExpression:  "#id = :id AND begins_with ( #SK, :SK )"
    }

    logger.info('Lesson.listStudents [DynamoDB', params);
    const res = await this.dynamodb.query(params).promise();
    const data = _.chain(res.Items)
        .map(item => new Student({id: item.userId.S}).config(this.ctx))
        .value();
    return data;
  }

  static async deleteById(id) {
    const lesson = await this.findById(id);
    if (!lesson) {
      return null;
    }
    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": lesson.organizationId },
        SK: { "S": `Lesson#${lesson.id}` }
      }
    }

    logger.info('Lesson.deleteById [DynamoDB]', params);
    const res = await this.dynamodb.deleteItem(params).promise();
    return lesson;
  }

  /**
   * @param {String} id
   */
  static async publish(id) {
    const lesson = await this.findById(id);
    if (!lesson) {
      throw new Error('Lesson doesn\'t exist');
    }
    if (!lesson.dirty) {
      throw new Error('You cannot publish not dirty Lesson');
    }

    const rev = parseInt(lesson.rev) + 1 || 0;
    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": lesson.organizationId },
        SK: { "S": `Lesson#${id}` }
      },
      ExpressionAttributeValues: {
        ":rev":   { "N": `${rev}` },
        ":dirty": { "BOOL": false }
      },
      UpdateExpression: "SET rev = :rev, dirty = :dirty",
      ReturnValues: "ALL_NEW",
    }
    logger.info('Lesson.publish [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    res.Attributes.dirty.BOOL = false;
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }

  static async create(input = {}) {
    const course = await Course.config(this.ctx).findById(input.courseId);
    if (!course) {
      throw new Error(`Course ${input.courseId} doesn't exist`);
    }

    const id = input.id || uuid();
    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": course.organizationId },
        SK:             { "S": `Lesson#${id}` },
        type:           { "S": `Lesson#${id}` },
        courseId:       { "S": input.courseId },
        id:             { "S": id },
        lessonId:       { "S": id },
        createdAt:      { "S": moment().format() },

        dirty:          { "BOOL": true },
        rev:            { "N": "-1" }
      }
    }

    if (input.title) {
      params.Item.title = { "S": input.title };
    }
    if (input.description) {
      params.Item.description = { "S": input.description };
    }
    if (_.isBoolean(input.homework)) {
      params.Item.homework = { "BOOL": !!input.homework };
    }

    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }

  static async update(input = {}) {
    const organizationId = input.organizationId;
    const id = input.id;

    if (input.content) {
      input.dirty = true;
    }
    const params = this.getUpdateParams(input);
    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK: { "S": `Lesson#${id}` }
    };

    logger.info('Lesson.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }

  static async findById(id, projection) {
    const params = {
      TableName: config.db.tableName,
      IndexName: "ByLesson",
      Limit: 1,
      ExpressionAttributeNames: {
        "#lessonId": "lessonId",
        "#SK": "SK"
      },
      ExpressionAttributeValues: {
        ":lessonId": { S: id },
        ":SK": { "S": `Lesson#${id}` }
      },
      KeyConditionExpression: "#lessonId = :lessonId AND #SK = :SK"
    }
    if (_.isArray(projection)) {
      projection = _.map(projection, field => {
        params.ExpressionAttributeNames[`#${field}`] = field;
        return `#${field}`;
      });
      params.ProjectionExpression = _.join(projection, ', ');
    }
    logger.info('Lesson.findById [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    const data = _.get(res, 'Items[0]');
    if (!data) return null;
    return this.fromDynamoDB(data).config(this.ctx);
  }

  /*
   * @filter {Object}
   *  @name {String} courseId
   */
  static async findAll(filter, limit = null, nextToken = null) {
    const params = {
      TableName: config.db.tableName,
      Limit: limit
    }
    if (nextToken) {
      nextToken = Buffer.from(nextToken, 'base64').toString();
      params.ExclusiveStartKey = {
        id: { S: nextToken }
      }
    }

    if (filter && filter.courseId) {
      params.IndexName = "ByCourse";
      params.ExpressionAttributeValues = {
        ":id": { S: filter.courseId },
        ":SK": { S: "Lesson#" }
      };
      params.KeyConditionExpression = "courseId = :id AND begins_with ( SK, :SK )";
    }
    logger.info('Lesson.findAll [DynamoDB]', params);
    const cmd = filter ? this.dynamodb.query : this.dynamodb.scan;
    const res = await cmd.call(this.dynamodb, params).promise();
    nextToken = _.get(res.LastEvaluatedKey, 'id.S', null);
    if (nextToken) {
      nextToken = Buffer.from(nextToken).toString('base64');
    }
    const data = _.chain(res.Items)
        .map(item => this.fromDynamoDB(item).serialize())
        .value();

    return {
      items: data,
      nextToken
    }
  }
}

module.exports = Lesson;
