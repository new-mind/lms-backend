const config = require('config');
const uuid = require('uuid/v4');
const moment = require('moment');
const _ = require('lodash');

const Model = require('./base');
const dynamodb = require('../db');
const Section = require('./Section');
const logger = require('../logger');

class Response extends Model {
  static getTypeConstructor (type) {
    switch (type) {
      case 'Attachment':
      case 'AttachmentWidget':
      case 'AttachmentResponse':
        return require('./response/Attachment');
      case 'Text':
      case 'TextWidget':
      case 'TextResponse':
        return require('./response/Text');
      case 'Link':
      case 'LinkWidget':
      case 'LinkResponse':
        return require('./response/Link');
      default:
        throw new Error(`Unknown type of widget ${type}`);
    }
  }

  static async update(input = {}) {
    const { rev, lessonId, sectionId, organizationId, type } = input;
    const ResponseType = Response.getTypeConstructor(type).config(this.ctx);

    const response = await this.findOne(input);
    if (!response) {
      return ResponseType.create(input);
    }

    if (ResponseType.update) {
      return ResponseType.update(input);
    }
    const params = ResponseType.getUpdateParams(input);
    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK:             { "S": `Lesson#${lessonId}#Response#${sectionId}#rev${rev || 0}` }
    };

    logger.info('Response.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return ResponseType.fromDynamoDB(res.Attributes).config(this.ctx);
  }

  static async findOne(input = {}, projection) {
    const { rev, lessonId, sectionId, organizationId } = input;

    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": organizationId },
        SK:             { "S": `Lesson#${lessonId}#Response#${sectionId}#rev${rev}` }
      }
    }

    if (_.size(projection)) {
      projection = _.map(projection, field => {
        params.ExpressionAttributeNames[`#${field}`] = field;
        return `#${field}`;
      });
      params.ProjectionExpression = _.join(projection, ', ');
    }

    logger.info('Response.findOne [DynamoDB]', params);
    const res = await this.dynamodb.getItem(params).promise();
    if (!res.Item) return null;

    const ResponseType = Response.getTypeConstructor(res.Item.type.S);
    return ResponseType.fromDynamoDB(res.Item).config(this.ctx);
  }

  /**
   * @param {Object} filter
   *  @key {String} organizationId, required
   *  @key {String} lessonId
   *  @key {String} sectionId
   */
  static async findAll(filter = {}) {
    const { organizationId, lessonId, sectionId } = filter;
    const params = {
      TableName: config.db.tableName,
      ExpressionAttributeValues: {
        ":organizationId": { "S": organizationId },
        ":SK":             { "S": `Lesson#${lessonId}#Response#${sectionId}#rev` }
      },
      KeyConditionExpression: "organizationId = :organizationId AND begins_with ( SK, :SK )",
      ScanIndexForward: false
    }

    logger.info('Response.findAll [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    if (!_.size(res.Items)) return [];

    const type = res.Items[0].type.S;
    const ResponseType = Response.getTypeConstructor(type);
    return _.map(res.Items, item => {
      return ResponseType.fromDynamoDB(item);
    });
  }
}

module.exports = Response;
