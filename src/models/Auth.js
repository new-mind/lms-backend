const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');

const Model = require('./base');
const logger = require('../logger');
const { cognito } = require('../aws');

class Auth extends Model {
  static async getUserRoles(id) {
    const params = {
      TableName: config.db.tableName,
      IndexName: 'ByUser'
    }

    const adminParams = {
      ...params,
      ExpressionAttributeNames: {
        "#id": "userId",
        "#sk": "SK"
      },
      ExpressionAttributeValues: {
        ":id": { S: id },
        ":sk": { S: "Admin#" }
      },
      KeyConditionExpression: "#id = :id AND begins_with ( #sk, :sk )"
    }

    const teacherParams = {
      ...params,
      ExpressionAttributeNames: {
        "#id": "userId",
        "#sk": "SK"
      },
      ExpressionAttributeValues: {
        ":id": { S: id },
        ":sk": { S: "Teacher#" }
      },
      KeyConditionExpression: "#id = :id AND begins_with ( #sk, :sk )"
    }

    const studentParams = {
      ...params,
      ExpressionAttributeNames: {
        "#id": "userId",
        "#sk": "SK"
      },
      ExpressionAttributeValues: {
        ":id": { S: id },
        ":sk": { S: "Student#" }
      },
      KeyConditionExpression: "#id = :id AND begins_with ( #sk, :sk )"
    }

    logger.info('Auth.getUserRoles.admin [DynamoDB]', adminParams);
    logger.info('Auth.getUserRoles.teacher [DynamoDB]', teacherParams);
    logger.info('Auth.getUserRoles.student [DynamoDB]', studentParams);

    return Promise.all([
      this.dynamodb.query(adminParams).promise(),
      this.dynamodb.query(teacherParams).promise(),
      this.dynamodb.query(studentParams).promise()
    ])
    .then(([admin, teacher, student]) => {
      const res = {
        admin: _.chain(admin).get('Items').map('organizationId.S').value(),
        teacher: _.chain(teacher).get('Items').map('organizationId.S').value(),
        student: _.chain(student).get('Items').map('organizationId.S').value()
      }

      return res;
    });
  }
}

module.exports = Auth;
