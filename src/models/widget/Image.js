const _ = require('lodash');
const config = require('config');
const uuid = require('uuid/v4');

const Widget = require('./base');
const logger = require('../../logger');
const dynamodb = require('../../db');

class ImageWidget extends Widget {
  static get spec() {
    return {
      ...Widget.spec,
      url:    { type: "S" },
      width:  { type: "S" },
      height: { type: "S" }
    }
  }

  static async create(input = {}) {
    const id = input.id || uuid();
    const lessonId = input.lessonId;
    const params = {
      TableName: config.db.tableName,
      Item: {
        // common
        organizationId: { "S": input.organizationId },
        SK:             { "S": `Lesson#${lessonId}#Section#${id}` },
        id:             { "S": id },
        type:           { "S": input.type },
        sectionId:      { "S": id },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": (new Date()).toISOString() },

        title:          input.title && { "S": input.title},

        // specific
        url:            input.url && { "S": input.url },
        width:          input.width && { "S": input.width },
        height:         input.height && { "S": input.height }
      }
    };
    logger.info('ImageWidget.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }
}

module.exports = ImageWidget;
