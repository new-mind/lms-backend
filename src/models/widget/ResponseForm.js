const _ = require('lodash');
const config = require('config');
const uuid = require('uuid/v4');
const moment = require('moment');

const Widget = require('./base');
const logger = require('../../logger');
const dynamodb = require('../../db');

class ResponseFormWidget extends Widget {
  static get spec() {
    return {
      ...Widget.spec,
      formType: { type: "S" },
      title:    { type: "S" }
    }
  }

  static async create(input = {}) {
    if (!input.formType) {
      throw new Error('**formType** field is required');
    }
    const id = input.id || uuid();
    const lessonId = input.lessonId;
    const params = {
      TableName: config.db.tableName,
      Item: {
        // common
        organizationId: { "S": input.organizationId },
        SK:             { "S": `Lesson#${lessonId}#Section#${id}` },
        type:           { "S": input.type },
        id:             { "S": id },
        sectionId:      { "S": id },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": moment().format() },

        title:          input.title && { "S": input.title },

        // specific
        formType:       { "S": input.formType }
      }
    };
    logger.info('ResponseForm.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }
}

module.exports = ResponseFormWidget;
