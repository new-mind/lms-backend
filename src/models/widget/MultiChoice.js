const _ = require('lodash');
const moment = require('moment');
const uuid = require('uuid/v4');

const config = require('config');
const logger = require('../../logger');
const Widget = require('./base');

class MultiChoiceWidget extends Widget {
  static get spec() {
    return {
      ...Widget.spec,
      question: { type: "S" },
      imageKey: { type: "S" },
      answers:  { type: "L", field: "M", spec: {
        id:      { type: "S" },
        correct: { type: "BOOL" },
        text:    { type: "S" }
      }}
    }
  }

  static async create(input = {}) {
    const id = input.id || uuid();
    const lessonId = input.lessonId;
    const answers = _.map(input.answers, ({id, correct, text}) => {
      return {
        "M": {
          id:      { "S": id },
          correct: { "BOOL": !!correct },
          text:    text && { "S": text }
        }
      }
    });

    const params = {
      TableName: config.db.tableName,
      Item: {
        // common
        organizationId: { "S": input.organizationId },
        SK:             { "S": `Lesson#${lessonId}#Section#${id}` },
        id:             { "S": id },
        type:           { "S": input.type },
        sectionId:      { "S": id },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": moment().format() },

        title:          input.title && { "S": input.title },
        question:       input.question && { "S": input.question },
        imageKey:       input.imageKey && { "S": input.imageKey },

        // specific
        answers:        answers.length ? { "L": answers } : null
      }
    };
    logger.info('MultiChoice.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }

  /**
   * TODO: answers are overriden
   */
  static async update(input = {}) {
    const id = input.id;
    const lessonId = input.lessonId;
    const organizationId = input.organizationId;

    //TODO: refactor
    const spec = this.spec;
    if (input.answers) {
      input.answers = _.map(input.answers, item => {
        const data = {};
        if (item.id) {
          data.id = { "S": item.id };
        }
        if (item.text) {
          data.text = { "S": item.text };
        }

        return {
          "M": {
            correct: { "BOOL": !!item.correct },
            ...data
          }
        };
      });
    }

    const params = this.getUpdateParams(input);
    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK: { "S": `Lesson#${lessonId}#Section#${id}` }
    };

    logger.info('MultiChoiceWidget.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }
}

module.exports = MultiChoiceWidget;
