const _ = require('lodash');
const config = require('config');
const uuid = require('uuid/v4');

const Model = require('../base');
const logger = require('../../logger');
const dynamodb = require('../../db');

class Widget extends Model {
  static get spec() {
    return {
      id:             { type: "S", readOnly: true, dynamoDBKey: "sectionId" },
      organizationId: { type: "S", readOnly: true },
      lessonId:       { type: "S", readOnly: true },
      type:           { type: "S", readOnly: true },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S", readOnly: true },
      title:          { type: "S" }
    }
  }
}

module.exports = Widget;
