const _ = require('lodash');
const config = require('config');
const uuid = require('uuid/v4');

const Widget = require('./base');
const logger = require('../../logger');
const dynamodb = require('../../db');

class TextWidget extends Widget {
  static get spec() {
    return {
      ...Widget.spec,
      title:          { type: "S" },
      data:           { type: "S" },

      //TODO for Response
      rev:            { type: "N" },
      lessonId:       { type: "S" },
      sectionId:      { type: "S" }
    }
  }

  static async create(input = {}) {
    const id = input.id || uuid();
    const lessonId = input.lessonId;
    const params = {
      TableName: config.db.tableName,
      Item: {
        // common
        organizationId: { "S": input.organizationId },
        SK:             { "S": `Lesson#${lessonId}#Section#${id}` },
        type:           { "S": input.type },
        id:             { "S": id },
        sectionId:      { "S": id },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": (new Date()).toISOString() },

        // specific
        data:           input.data && { "S": input.data }
      }
    };
    logger.info('TextWidget.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }
}

module.exports = TextWidget;
