const _ = require('lodash');
const config = require('config');
const uuid = require('uuid/v4');

const Widget = require('./base');
const logger = require('../../logger');
const dynamodb = require('../../db');

class AttachmentWidget extends Widget {
  static get spec() {
    return {
      ...Widget.spec,
      items: { type: "L", field: "M", spec: {
        id:         { type: "S" },
        identityId: { type: "S" },
        fileKey:    { type: "S" },
        fileName:   { type: "S" },
        title:      { type: "S" }
      }},
    }
  }

  static async create(input = {}) {
    const id = input.id || uuid();
    const lessonId = input.lessonId;

    const items = _.map(input.items, ({id, identityId, fileName, fileKey, title}) => {
      return {
        "M": {
          id:         { "S": id },
          identityId: identityId && { "S": identityId },
          title:      title && { "S": title },
          fileName:   fileName && { "S": fileName },
          fileKey:    fileKey && { "S": fileKey }
        }
      }
    });
    const params = {
      TableName: config.db.tableName,
      Item: {
        // common
        organizationId: { "S": input.organizationId },
        SK:             { "S": `Lesson#${lessonId}#Section#${id}` },
        id:             { "S": id },
        type:           { "S": input.type },
        sectionId:      { "S": id },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": (new Date()).toISOString() },

        title:          input.title && { "S": input.title},

        // specific
        items:          items.length ? { "L": items } : null
      }
    };
    logger.info('AttachmentWidget.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }

  static async update(input = {}) {
    const id = input.id;
    const lessonId = input.lessonId;
    const organizationId = input.organizationId;
    //TODO: refactor
    const spec = AttachmentWidget.spec;
    if (input.items) {
      input.items = _.map(input.items, item => {
        const data = {};
        if (item.id) {
          data.id = { "S": item.id };
        }
        if (item.identityId) {
          data.identityId = { "S": item.identityId };
        }
        if (item.fileKey) {
          data.fileKey = { "S": item.fileKey };
        }
        if (item.fileName) {
          data.fileName = { "S": item.fileName };
        }
        if (item.title) {
          data.title = { "S": item.title };
        }
        return {
          "M": data
        };
      });
    }

    const params = this.getUpdateParams(input);
    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK: { "S": `Lesson#${lessonId}#Section#${id}` }
    };

    logger.info('AttachmentWidget.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }
}

module.exports = AttachmentWidget;
