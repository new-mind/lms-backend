const _ = require('lodash');
const config = require('config');
const uuid = require('uuid/v4');

const Model = require('./base');
const logger = require('../logger');

const dynamodb = require('../db');

class Section extends Model {
  static get spec() {
    return {
      id:             { type: "S", readOnly: true, dynamoDBKey: "sectionId" },
      organizationId: { type: "S", readOnly: true },
      lessonId:       { type: "S", readOnly: true },
      type:           { type: "S", readOnly: true },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S", readOnly: "S" },
    }
  }

  static getTypeConstructor (type) {
    switch (type) {
      case 'AttachmentWidget':
        return require('./widget/Attachment');
      case 'ImageWidget':
        return require('./widget/Image');
      case 'TextWidget':
        return require('./widget/Text');
      case 'LinkWidget':
        return require('./widget/Link');
      case 'MultiChoiceWidget':
        return require('./widget/MultiChoice');
      case 'VideoWidget':
        return require('./widget/Video');
      case 'ResponseFormWidget':
        return require('./widget/ResponseForm');
      default:
        throw new Error(`Unknown type of widget ${type}`);
    }
  }
  /**
   * @input {CreateSectionInput}
   */
  static async create(input = {}) {
    const Widget = this.getTypeConstructor(input.type).config(this.ctx);
    return Widget.create(input);
  }

  static async update(input = {}) {
    const Widget = this.getTypeConstructor(input.type).config(this.ctx);
    if (Widget.update) {
      return Widget.update(input);
    }
    const id = input.id;
    const lessonId = input.lessonId;
    const organizationId = input.organizationId;

    const params = Widget.getUpdateParams(input);
    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK: { "S": `Lesson#${lessonId}#Section#${id}` }
    };

    logger.info('Section.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes);
  }

  static fromDynamoDB(Item) {
    const Widget = this.getTypeConstructor(Item.type.S);
    if (!Widget) {
      throw new Error(`${Item.type.S} is not supported type of widget`);
    }

    return Widget.fromDynamoDB(Item);
  }

  static async findOne(filter = {}, projection) {
    const { lessonId, id, organizationId } = filter;
    if (!id) {
      throw new Error('MUST provide filter.id');
    }
    if (!lessonId) {
      throw new Error('MUST provide filter.lessonId');
    }
    if (!organizationId) {
      throw new Error('MUST provide filter.organizationId');
    }
    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": organizationId },
        SK:             { "S": `Lesson#${lessonId}#Section#${id}` }
      }
    }

    if (_.size(projection)) {
      params.ExpressionAttributeNames = {};
      projection = _.map(projection, field => {
        params.ExpressionAttributeNames[`#${field}`] = field;
        return `#${field}`;
      });
      params.ProjectionExpression = _.join(projection, ', ');
    }
    logger.info('Section.findOne [DynamoDB]', params);
    const res = await this.dynamodb.getItem(params).promise();
    return this.fromDynamoDB(res.Item).config(this.ctx);
  }
  /*
   * @filter {Object}
   *  @name {String} lessonId
   */
  static async findAll(filter, limit = null, nextToken = null) {
    const params = {
      TableName: config.db.table.section,
      Limit: limit
    }

    if (nextToken) {
      nextToken = Buffer.from(nextToken, 'base64').toString();
      params.ExclusiveStartKey = {
        id: { S: nextToken }
      }
    }

    if (filter) {
      params.IndexName = "gsi-Lesson.content";
      params.ExpressionAttributeValues = {
        ":lessonId": { S: filter.lessonId }
      };
      params.KeyConditionExpression = "lessonContentId = :lessonId";
    }
    const cmd = filter ? this.dynamodb.query : this.dynamodb.scan;
    const res = await cmd.call(this.dynamodb, params).promise();
    nextToken = _.get(res.LastEvaluatedKey, 'id.S', null);
    if (nextToken) {
      nextToken = Buffer.from(nextToken).toString('base64');
    }
    const data = _.chain(res.Items)
        .map(item => this.fromDynamoDB(item).toJSON())
        .value();

    return {
      items: data,
      nextToken
    }
  }
}

module.exports = Section;
