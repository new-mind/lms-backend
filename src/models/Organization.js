const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');
const uuid = require('uuid/v4');

const Model = require('./base');

class Organization extends Model {
  static get spec() {
    return {
      id:    { type: "S", readOnly: true, dynamoDBKey: "organizationId" },
      title: { type: "S" }
    }
  }

  static async create(title) {
    const id = uuid();
    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": id },
        SK:             { "S": `Organization#${id}` },
        id:             { "S": id },
        createdAt:      { "S": (new Date()).toISOString() },
        title:          title && { "S": title }
      }
    };

    const res = await this.dynamodb.putItem(params).promise();
    const data = _.get(res, 'Item');
    return new Organization({ id }).config(this.ctx);
  }

  static async findById(id) {
    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { S: id },
        SK: { "S": `Organization#${id}` }
      }
    }
    const res = await this.dynamodb.getItem(params).promise();
    const data = _.get(res, 'Item');
    return this.fromDynamoDB(data);
  }
}

module.exports = Organization;
