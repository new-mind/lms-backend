const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');
const uuid = require('uuid/v4');
const moment = require('moment');

const logger = require('../logger');
const Model = require('./base');
const Student = require('./User/Student');

class Course extends Model {
  static get spec() {
    return {
      id:             { type: "S", readOnly: true, dynamoDBKey: "courseId" },
      organizationId: { type: "S", readOnly: true },
      owner:          { type: "S", readOnly: true },
      title:          { type: "S", required: true },
      description:    { type: "S" },
      imageUrl:       { type: "S" },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S", readOnly: "S" },

      content:        { type: "L", field: "S", readOnly: true }
    }
  }

  /**
   * @return {Course} course
   */
  static async create(input = {}) {
    const id = input.id || uuid();
    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": input.organizationId },
        SK:             { "S": `Course#${id}` },
        type:           { "S": `Course#${id}` },
        id:             { "S": id },
        courseId:       { "S": id },
        createdAt:      { "S": (new Date()).toISOString() }
      }
    }

    if (input.title) {
      params.Item.title = { "S": input.title };
    }
    if (input.description) {
      params.Item.description = { "S": input.description };
    }
    if (input.imageUrl) {
      params.Item.imageUrl = { "S": input.imageUrl };
    }

    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }

  /**
   * @return {Array} courses, serialized
   */
  static async findAll(filter, limit = 10, nextToken = null) {
    const params = {
      TableName: config.db.tableName,
      IndexName: 'ByType',
      Limit: limit
    }

    if (nextToken) {
      nextToken = Buffer.from(nextToken, 'base64').toString();
      params.ExclusiveStartKey = {
        CourseId: { S: nextToken }
      }
    }

    if (filter && filter.organizationId) {
      params.ExpressionAttributeNames = {
        "#type": "type"
      };
      params.ExpressionAttributeValues = {
        ":id": { S: filter.organizationId },
        ":type": { S: "Course#" },
      };
      params.KeyConditionExpression = "organizationId = :id AND begins_with ( #type, :type )";
    }
    logger.info('Course.findAll [DynamoDB]', params);
    const cmd = filter ? this.dynamodb.query : this.dynamodb.scan;
    const res = await cmd.call(this.dynamodb, params).promise();
    nextToken = _.get(res.LastEvaluatedKey, 'CourseId.S', null);
    if (nextToken) {
      nextToken = Buffer.from(nextToken).toString('base64');
    }
    const data = _.chain(res.Items)
        .map(item => this.fromDynamoDB(item).serialize())
        .value();

    return {
      items: data,
      nextToken
    }
  }

  /**
   * @return {Course} course
   */
  static async findById(id, projection) {
    const params = {
      TableName: config.db.tableName,
      IndexName: "ByCourse",
      Limit: 1,
      ExpressionAttributeNames: {
        "#id": "courseId",
        "#sk": "SK"
      },
      ExpressionAttributeValues: {
        ":id": { S: id },
        ":sk": { "S": `Course#${id}` }
      },
      KeyConditionExpression: "#id = :id AND #sk = :sk"
    }
    if (_.isArray(projection)) {
      projection = _.map(projection, field => {
        params.ExpressionAttributeNames[`#${field}`] = field;
        return `#${field}`;
      });
      params.ProjectionExpression = _.join(projection, ', ');
    }
    logger.info('Course.findById [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    const data = _.get(res, 'Items[0]');
    if (!data) {
      logger.warn(`Course doesn't exist ${id}`);
      return null;
    }
    return this.fromDynamoDB(data).config(this.ctx);
  }

  static async deleteLesson(lessonId) {
    const Lesson = require('./Lesson');
    const lesson = await Lesson.config(this.ctx).deleteById(lessonId);
    if (!lesson) return null;
    const course = await this.findById(lesson.courseId, ["courseId", "content"]);

    const items = _.filter(course.content, id => id !== lesson.id);
    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": lesson.organizationId },
        SK:             { "S": `Course#${lesson.courseId}` }
      },
      ExpressionAttributeValues: {
        ":content":   { "L": _.map(items, (id) => ({"S": id})) },
        ":updatedAt": { "S": moment().format() }
      },
      UpdateExpression: "SET content = :content, updatedAt = :updatedAt"
    }

    logger.info('Course.deleteLesson [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return lesson.config(this.ctx);
  }

  static async update(input = {}) {
    input.updatedAt = moment().format();

    const expSet = {};
    const expRemove = [];
    const expValues = {};

    _.forIn(input, (v, k) => {
      const field = Course.spec[k];
      if (!field || field.readOnly) return;

      if (_.isNull(v)) {
        if (field.required) return;
        expRemove.push(k);
      } else {
        expValues[`:${k}`] = { [field.type]: v };
        expSet[v] = k;
      }
    });

    const expressions = [];
    if (expRemove.length) {
      expressions.push(`REMOVE ${expRemove.join(',')}`);
    }

    if (!_.isEmpty(expSet)) {
      let exps = _.map(expSet, (k, v) => `${k} = :${k}`);
      expressions.push(`SET ${exps.join(',')}`);
    }

    if (!expressions.length) return;

    const params = {
      TableName: config.db.tableName,
      ExpressionAttributeValues: expValues,
      UpdateExpression: expressions.join(' '),
      Key: {
        organizationId: { "S": input.organizationId },
        SK: { "S": `Course#${input.id}` }
      },
      ReturnValues: "ALL_NEW"
    }

    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }

  static async removeById(id) {
    const queryParams = {
      TableName: config.db.tableName,
      IndexName: 'ByCourse',
      ExpressionAttributeNames: {
        "#id": "courseId",
        "#sk": "SK"
      },
      ExpressionAttributeValues: {
        ":id": { S: id },
        ":sk": { S: `Course#${id}` }
      },
      KeyConditionExpression: "#id = :id AND begins_with ( #sk, :sk )"
    };

    logger.info('Course.removeById.queryParams [DynamoDB]', queryParams);
    const [res, course] = await Promise.all([
      this.dynamodb.query(queryParams).promise(),
      this.findById(id)
    ])

    const items = _.chain(res)
      .get('Items')
      .map(item => (
        {
          organizationId: item.organizationId,
          SK: item.SK
        }
      ))
      .value();
    await this.batchDelete(items, config.db.tableName);
    return course;
  }

  static async listByIds(organizationId, ids) {
    if (!_.size(ids)) return [];

    const keys = _.map(ids, id => (
      {
        organizationId: { S: organizationId },
        SK:       { S: `Course#${id}` }
      }
    ));
    const params = {
      ReturnConsumedCapacity: "TOTAL",
      RequestItems: {
        [config.db.tableName]: {
          Keys: keys
        }
      }
    }

    logger.info('Coures.listByIds [DynamoDB]', params);
    const res =  await this.dynamodb.batchGetItem(params).promise();
    return _.chain(res.Responses[config.db.tableName])
      .map(data => this.fromDynamoDB(data).serialize())
      .value();
  }

  async listStudents() {
    const params = {
      TableName: config.db.tableName,
      ExpressionAttributeNames: {
        "#id": "organizationId",
        "#SK": "SK"
      },
      ExpressionAttributeValues: {
        ":id": { "S": this.organizationId },
        ":SK": { "S": `CourseAssignment#${this.id}#Student#` }
      },
      ProjectionExpression: "userId",
      KeyConditionExpression: "#id = :id AND begins_with ( #SK, :SK )"
    }

    logger.info('course.listStudents [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    const ConnectedStudent = Student.config(this.ctx);
    const qs = _.map(res.Items, (item) => {
      return ConnectedStudent.findById(item.userId.S);
    });

    const students = await Promise.all(qs);
    return _.filter(students);
  }

  async moveLesson(id, position) {
    const Lesson = require('./Lesson');
    const lesson = await Lesson.config(this.ctx).findById(id, ['lessonId']);
    if (!lesson) {
      throw new Error(`Lesson ${id} doesn't exist`);
    }
    const items = this.content;
    const index = this.content.indexOf(lesson.id);
    if (index === -1) {
      throw new Error(`CRITICAL. Unable to find Lesson ${lesson.id} into Course ${this.id}`);
    }
    const length = _.size(this.content);
    if (!(position >= 0)) position = 0;
    if (position >= length) position = length;
    items.splice(index, 1);
    items.splice(position, 0, lesson.id);

    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": this.organizationId },
        SK:             { "S": `Course#${this.id}` }
      },
      ExpressionAttributeValues: {
        ":content":   { "L": _.map(items, (id) => ({"S": id})) },
        ":updatedAt": { "S": moment().format() }
      },
      UpdateExpression: "SET content = :content, updatedAt = :updatedAt"
    }

    logger.info('Course.moveLesson [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();

    return {
      lesson,
      position
    }
  }

  async createLesson(input = {}) {
    const Lesson = require('./Lesson');
    const lesson = await Lesson.config(this.ctx).create(input);
    if (!lesson) {
      throw new Error(`Unable to create lesson`);
    }

    const items = _.map(this.content, id => ({ "S": id }));
    items.push({"S": lesson.id});
    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": lesson.organizationId },
        SK:             { "S": `Course#${this.id}` }
      },
      ExpressionAttributeValues: {
        ":content": { "L": items },
        ":updatedAt": { "S": moment().format() }
      },
      UpdateExpression: "SET content = :content, updatedAt = :updatedAt"
    }
    logger.info('Course.createLesson.updateItem [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return lesson;
  }

  async listLessons() {
    const Lesson = require('./Lesson');
    const {items, nextToken} = await Lesson.config(this.ctx).findAll({courseId: this.id});
    var map = _.keyBy(items, 'id');
    const lessons = [];
    _.forEach(this.content, id => {
      const lesson = map[id];
      if (!lesson) return;
      lessons.push(lesson);
    });

    return {
      items: lessons,
      nextToken
    }
  }
}

module.exports = Course;
