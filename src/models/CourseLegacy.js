const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');

const Model = require('./base');

const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

class Course extends Model {
  constructor(params) {
    super();

    this.id = params.id;
    this.title = params.title;
    this.description = params.description || null;
    this.imageUrl = params.imageUrl || null;
    this.owner = params.owner || null;
    this.teachers = []; //TODO

    this.updatedAt = params.updatedAt || new Date();
    this.createdAt = params.createdAt || new Date();
  }

  serialize() {
    return {
      id: this.id,
      title: this.title,
      description: this.description,
      imageUrl: this.imageUrl,
      owner: this.owner,
      teachers: this.teachers
    }
  }

  static fromDynamoDB(Item) {
    return new Course({
      id: _.get(Item, 'id.S'),
      title: _.get(Item, 'title.S'),
      description: _.get(Item, 'description.S'),
      imageUrl: _.get(Item, 'imageUrl.S'),
      owner: _.get(Item, 'owner.S'),
    })
  }

  static async findById(id) {
    const params = {
      TableName: config.db.table.course,
      Key: {
        id: { S: id }
      }
    }
    const res = await dynamodb.getItem(params).promise();
    const data = _.get(res, 'Item');
    return Course.fromDynamoDB(data);
  }

  static async findAll(filter, limit = 10, nextToken = null) {
    const params = {
      TableName: config.db.table.course,
      Limit: limit
    }

    if (nextToken) {
      nextToken = Buffer.from(nextToken, 'base64').toString();
      params.ExclusiveStartKey = {
        id: { S: nextToken }
      }
    }

    const cmd = filter ? dynamodb.query : dynamodb.scan;
    const res = await cmd.call(dynamodb, params).promise();
    nextToken = _.get(res.LastEvaluatedKey, 'id.S', null);
    if (nextToken) {
      nextToken = Buffer.from(nextToken).toString('base64');
    }
    const data = _.chain(res.Items)
        .map(item => Course.fromDynamoDB(item).serialize())
        .value();

    return {
      items: data,
      nextToken
    }
  }
}

module.exports = Course;
