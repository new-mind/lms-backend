const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');
const uuid = require('uuid/v4');
const generator = require('generate-password');
const moment = require('moment');

const User = require('./index');
const logger = require('../../logger');
const { cognito } = require('../../aws');

class Teacher extends User {
  /**
   * @param {Object} input
   *  @key {String} email
   */
  static async invite(input = {}) {
    const email = _.toLower(input.email);
    let user = await this.findByEmail(email);
    if (!user) {
      user = await this.create(input);
    }

    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": input.organizationId },
        SK:             { "S": `Teacher#${user.id}` },
        userId:         { "S": user.id },
        createdAt:      { "S": moment().format() },
      }
    }

    logger.info('Teacher.invite [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return user;
  }

  static async create(input = {}) {
    const id = uuid();
    const password = generator.generate({ length: 10, numbers: true });

    const params = {
      TableName: config.db.userTableName,
      Item: {
        id:        { "S": id },
        createdAt: { "S": moment().format() },
        email:     { "S": input.email }
      }
    }

    if (input.name) {
      params.Item.name = { "S": input.name };
    }
    if (input.surname) {
      params.Item.surname = { "S": input.surname };
    }
    if (input.phone) {
      params.Item.phone = { "S": input.phone };
    }
    if (input.age) {
      params.Item.age = { "S": input.age };
    }

    const cognitoParams = {
      UserPoolId: config.cognito.pool,
      Username: id,
      TemporaryPassword: password,
      DesiredDeliveryMediums: ["EMAIL"],
      UserAttributes: [
        {
          Name: "email_verified",
          Value: 'True'
        },
        {
          Name: "email",
          Value: _.toLower(input.email)
        }
      ]
    }

    logger.info('Teacher.create [DynamoDB]', params);
    logger.info('Teacher.create [Cognito]', _.extend({}, cognitoParams, {TemporaryPassword: "***"}));
    await Promise.all([
      this.dynamodb.putItem(params).promise(),
      cognito.adminCreateUser(cognitoParams).promise()
    ]);

    return this.fromDynamoDB(params.Item);
  }

  /**
   * @param {Object} input
   *  @key {ID} organizationId
   *  @key {ID} userId
   */
  static async delete(input = {}) {
    const params = {
      TableName: config.db.tableName,
      Key: {
        "organizationId": { S: input.organizationId },
        "SK":             { S: `Teacher#${input.id}` }
      }
    }

    logger.info('Teacher.delete [DynamoDB]', params);
    const res = await this.dynamodb.deleteItem(params).promise();
    return this.findById(input.id);
  }

  static async findById(id) {
    const params = {
      TableName: config.db.userTableName,
      Limit: 1,
      ExpressionAttributeValues: {
        ":id": { S: id }
      },
      KeyConditionExpression: "id = :id"
    }

    logger.info('Teacher.findById [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    const data = _.get(res, 'Items[0]');
    if (!data) return null;
    return this.fromDynamoDB(data);
  }

  static async findAll(filter, limit = 10, nextToken = null) {
    return User.config(this.ctx).findAll('Teacher', filter, limit, nextToken);
  }
}

module.exports = Teacher;
