require('module-alias/register')
const config = require('config');
const uuid = require('uuid/v4');
const moment = require('moment');
const _ = require('lodash');

const logger = require('@lms/logger');
const Model = require('@lms/models/base');

class LessonComment extends Model {
  static get spec() {
    return {
      id:             { type: "S", readOnly: true },
      userId:         { type: "S", readOnly: true },
      organizationId: { type: "S", readOnly: true },
      createdAt:      { type: "S", readOnly: true },
      updatedAt:      { type: "S" },
      content:        { type: "S" }
    }
  }

  /**
   * @params {Object} input
   *  @key {String} id
   *  @key {String} userId, required
   *  @key {String} organizationId, required
   *  @key {String} lessonId, required
   *  @key {String} content, required
   */
  static async create(input = {}) {
    const { organizationId, userId, lessonId, content } = input;
    const id = input.id || uuid();
    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { S: organizationId },
        SK:             { S: `LessonInstanceComment#${lessonId}#${id}` },
        id:             { S: id },
        userId:         { S: userId },
        content:        { S: content },
        createdAt:      { S: moment().format() }
      }
    }

    logger.info('Student.LessonComment.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item);
  }

  /**
   * @params {Object} filter
   *  @key {String} lessonId, required
   *  @key {String} organizationId, required
   * @params {Number} limit
   * @params {String} nextToken
   */
  static async findAll(filter = {}, limit = null, nextToken = null) {
    const params = {
      TableName: config.db.tableName,
      Limit: limit,
      ExpressionAttributeValues: {
        ":id": { S: filter.organizationId },
        ":SK": { S: `LessonInstanceComment#${filter.lessonId}#` }
      },
      KeyConditionExpression: "organizationId = :id AND begins_with ( SK, :SK )"
    }
    if (nextToken) {
      params.ExclusiveStartKey = {
        organizationId: { S: filter.organizationId },
        SK: { S: await this.decodeNextToken(nextToken) }
      }
    }

    logger.info('Student.LessonComment.findAll [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    nextToken = _.get(res.LastEvaluatedKey, 'SK.S', null);
    if (nextToken) {
      nextToken = await this.encodeNextToken(nextToken);
    }

    const items = _.chain(res.Items)
      .sortBy('createdAt.S') // TODO
      .map(item => this.fromDynamoDB(item).serialize())
      .value()

    return {
      items,
      nextToken
    }
  }
}

module.exports = LessonComment;
