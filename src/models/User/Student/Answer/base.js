const config = require('config');
const uuid = require('uuid/v4');
const moment = require('moment');
const _ = require('lodash');

const logger = require('@lms/logger');
const Model = require('@lms/models/base');
const Section = require('@lms/models/Section');

class StudentAnswerBase extends Model {
  /**
   * @param {Object} input
   *  @param {UUID} input.organizationId
   *  @param {UUID} input.userId
   *  @param {UUID} input.sectionId
   *  @param {number} input.rev=0
   * @param {Object} [projection]
   */
  static async findOne(input, projection) {
    const { sectionId, organizationId, userId } = input;
    const { rev = 0 } = input;

    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": organizationId },
        SK:             { "S": `StudentAnswer#${userId}#Section#${sectionId}#rev${rev}` },
      }
    }

    if (_.size(projection)) {
      projection = _.map(projection, field => {
        params.ExpressionAttributeNames[`#${field}`] = field;
        return `#${field}`;
      });
      params.ProjectionExpression = _.join(projection, ', ');
    }

    logger.info('StudentAnswer.findOne [DynamoDB]', params);
    const res = await this.dynamodb.getItem(params).promise();
    if (!res.Item) return null;

    return this.fromDynamoDB(res.Item).config(this.ctx);
  }
}

module.exports = StudentAnswerBase;
