const uuid = require('uuid/v4');
const moment = require('moment');
const config = require('config');

const StudentAnswerBase = require('./base');
const logger = require('@lms/logger');

class TextAnswer extends StudentAnswerBase {
  static get spec() {
    return {
      __typename:     { type: "S", readOnly: true },

      id:             { type: "S", readOnly: true },
      organizationId: { type: "S", readOnly: true },
      lessonId:       { type: "S", readOnly: true },
      sectionId:      { type: "S", readOnly: true },
      userId:         { type: "S", readOnly: true },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S", readOnly: true },
      rev:            { type: "N", readOnly: true },

      data:           { type: "S" }
    }
  }

  /**
   *  @param {Object} input
   *    @param {UUID} input.lessonId
   *    @param {UUID} input.organizationId
   *    @param {UUID} input.sectionId
   *    @param {UUID} input.userId
   *    @param {number} [input.rev=0]
   *    @param {string} [input.data]
   */
  static async create(input = {}) {
    const { lessonId, sectionId, organizationId, userId } = input;
    const { rev = 0, data } = input;

    const id = uuid();
    const params = {
      TableName: config.db.tableName,
      Item: {
        __typename:     { "S": "StudentAnswer_TextWidget" },

        organizationId: { "S": organizationId },
        SK:             { "S": `StudentAnswer#${userId}#Section#${sectionId}#rev${rev}` },
        id:             { "S": id },
        lessonId:       { "S": lessonId },
        sectionId:      { "S": sectionId },
        userId:         { "S": userId },
        createdAt:      { "S": moment().format() },
        rev:            { "N": `${rev}` },

        data:           data && { "S": data },
      }
    }

    logger.info('TextAnswer.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }

  /**
   *  @param {Object} input
   *    @param {UUID} input.organizationId
   *    @param {UUID} input.sectionId
   *    @param {UUID} input.userId
   *    @param {number} [input.rev=0]
   *    @param {string} [input.data]
   */
  static async update(input = {}) {
    const { organizationId, sectionId, userId } = input;
    const { rev = 0 } = input;
    input.rev = rev;

    const params = this.getUpdateParams(input);
    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK:             { "S": `StudentAnswer#${userId}#Section#${sectionId}#rev${rev}` },
    };

    logger.info('TextAnswer.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }
}

module.exports = TextAnswer;
