const uuid = require('uuid/v4');
const moment = require('moment');
const config = require('config');
const _ = require('lodash');

const StudentAnswerBase = require('./base');
const logger = require('@lms/logger');

class LinkAnswer extends StudentAnswerBase {
  static get spec() {
    return {
      __typename:     { type: "S", readOnly: true },

      id:             { type: "S", readOnly: true },
      organizationId: { type: "S", readOnly: true },
      lessonId:       { type: "S", readOnly: true },
      userId:         { type: "S", readOnly: true },
      sectionId:      { type: "S", readOnly: true },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S", readOnly: true },
      rev:            { type: "N", readOnly: true },

      items:          { type: "L", field: "M", spec: {
        id:    { type: "S" },
        url:   { type: "S" },
        title: { type: "S" }
      }}
    }
  }

  /**
   *  @param {Object} input
   *    @param {UUID} input.lessonId
   *    @param {UUID} input.organizationId
   *    @param {UUID} input.userId
   *    @param {UUID} input.sectionId
   *    @param {number} [input.rev=0]
   *    @param {items[]} [input.items]
   *      @param {UUID} input.items[].id
   *      @param {string} input.items[].title
   *      @param {string} input.items[].url
   */
  static async create(input) {
    const { lessonId, sectionId, organizationId, userId } = input;
    const { rev = 0 } = input;

    const id = uuid();
    const items = _.map(input.items, ({id, title, url}) => {
      return {
        "M": {
          id: { "S": id },
          title: title && { "S": title },
          url:   url && { "S": url }
        }
      }
    });
    const params = {
      TableName: config.db.tableName,
      Item: {
        __typename:     { "S": "StudentAnswer_LinkWidget" },

        organizationId: { "S": organizationId },
        SK:             { "S": `StudentAnswer#${userId}#Section#${sectionId}#rev${rev}` },
        id:             { "S": id },
        userId:         { "S": userId },
        sectionId:      { "S": sectionId },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": moment().format() },
        rev:            { "N": `${rev}` },

        items:          items.length ? { "L": items } : null
      }
    }

    logger.info('LinkAnswer.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }

  /**
   *  @param {Object} input
   *    @param {UUID} input.userId
   *    @param {UUID} input.sectionId
   *    @param {UUID} input.organizationId
   *    @param {number} [input.rev=0]
   *    @param {items[]} [input.items]
   *      @param {UUID} input.items[].id
   *      @param {string} input.items[].title
   *      @param {string} input.items[].url
   *
   * TODO: items are overriden
   */
  static async update(input) {
    const { organizationId, sectionId, userId } = input;
    const { rev = 0 } = input;
    input.rev = rev;

    // TODO: move to base
    if (input.items) {
      input.items = _.map(input.items, item => {
        const data = {};
        if (item.id) {
          data.id = { "S": item.id };
        }
        if (item.url) {
          data.url = { "S": item.url };
        }
        if (item.title) {
          data.title = { "S": item.title };
        }
        return {
          "M": data
        };
      });
    }
    const params = this.getUpdateParams(input);
    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK:             { "S": `StudentAnswer#${userId}#Section#${sectionId}#rev${rev}` },
    };

    logger.info('LinkAnswer.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }
}

module.exports = LinkAnswer;
