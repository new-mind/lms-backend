const _ = require('lodash');
const uuid = require('uuid/v4');
const moment = require('moment');
const config = require('config');

const StudentAnswerBase = require('./base');
const logger = require('@lms/logger');

class AttachmentAnswer extends StudentAnswerBase {
  static get spec() {
    return {
      __typename:     { type: "S", readOnly: true },

      id:             { type: "S", readOnly: true },
      organizationId: { type: "S", readOnly: true },
      lessonId:       { type: "S", readOnly: true },
      sectionId:      { type: "S", readOnly: true },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S", readOnly: true },
      rev:            { type: "N", readOnly: true },

      items:          { type: "L", field: "M", spec: {
        id:         { type: "S" },
        fileKey:    { type: "S" },
        fileName:   { type: "S" },
        identityId: { type: "S" },
        title:      { type: "S" }
      }}
    }
  }

  /**
   *  @param {Object} input
   *    @param {UUID} input.organizationId
   *    @param {UUID} input.sectionId
   *    @param {UUID} input.lessonId
   *    @param {UUID} input.userId
   *    @param {number} [input.rev=0]
   *    @param {items[]} [input.items]
   *      @param {UUID} input.items[].id
   *      @param {string} input.items[].identityId
   *      @param {string} input.items[].title
   *      @param {string} input.items[].fileName
   *      @param {string} input.items[].fileKey
   */
  static async create(input) {
    const { lessonId, sectionId, organizationId, userId } = input;
    const { rev = 0 } = input;

    const id = uuid();
    const items = _.map(input.items, ({id, title, fileName, fileKey, identityId}) => {
      return {
        "M": {
          id:         { "S": id },
          identityId: identityId && { "S": identityId },
          title:      title && { "S": title },
          fileName:   fileName && { "S": fileName },
          fileKey:    fileKey && { "S": fileKey }
        }
      }
    });
    const params = {
      TableName: config.db.tableName,
      Item: {
        __typename:     { "S": "StudentAnswer_AttachmentWidget" },

        organizationId: { "S": organizationId },
        SK:             { "S": `StudentAnswer#${userId}#Section#${sectionId}#rev${rev}` },
        id:             { "S": id },
        sectionId:      { "S": sectionId },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": moment().format() },
        rev:            { "N": `${rev}` },

        items:          items.length ? { "L": items } : null
      }
    }

    logger.info('AttachmentAnswer.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }

  /**
   *  @param {Object} input
   *    @param {UUID} input.sectionId
   *    @param {UUID} input.organizationId
   *    @param {UUID} input.userId
   *    @param {number} [input.rev=0]
   *    @param {items[]} [input.items]
   *      @param {UUID} input.items[].id
   *      @param {string} input.items[].identityId
   *      @param {string} input.items[].title
   *      @param {string} input.items[].fileName
   *      @param {string} input.items[].fileKey
   */
  static async update(input) {
    const { organizationId, sectionId, userId } = input;
    const { rev = 0 } = input;
    input.rev = rev;

    if (input.items) {
      input.items = _.map(input.items, item => {
        const data = {};
        if (item.id) {
          data.id = { "S": item.id };
        }
        if (item.identityId) {
          data.identityId = { "S": item.identityId };
        }
        if (item.fileKey) {
          data.fileKey = { "S": item.fileKey };
        }
        if (item.fileName) {
          data.fileName = { "S": item.fileName };
        }
        if (item.title) {
          data.title = { "S": item.title };
        }
        return {
          "M": data
        };
      });
    }
    const params = this.getUpdateParams(input);

    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK:             { "S": `StudentAnswer#${userId}#Section#${sectionId}#rev${rev}` },
    };

    logger.info('AttachmentAnswer.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }
}

module.exports = AttachmentAnswer;
