const _ = require('lodash');
const config = require('config');

const Model = require('@lms/models/base');
const logger = require('@lms/logger');

const modules = {
  StudentAnswer_TextWidget: require('./Text'),
  StudentAnswer_LinkWidget: require('./Link'),
  StudentAnswer_AttachmentWidget: require('./Attachment')
}

class ResponseFormAnswer extends Model {
  /**
   *  @params {Object} filter
   *    @param {UUID} filter.organizationId
   *    @param {UUID} filter.userId
   *    @param {UUID} filter.sectionId
   */
  static async findAll(filter = {}) {
    const { organizationId, userId, sectionId } = filter;
    const params = {
      TableName: config.db.tableName,
      ExpressionAttributeValues: {
        ":organizationId": { "S": organizationId },
        ":SK":             { "S": `StudentAnswer#${userId}#Section#${sectionId}` }
      },
      KeyConditionExpression: "organizationId = :organizationId AND begins_with ( SK, :SK )",
      ScanIndexForward: false
    }

    logger.info('ResponseFormAnswer.findAll [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    if (!_.size(res.Items)) return [];

    return _.map(res.Items, item => {
      const type = item.__typename.S;
      const StudentAnswerType = modules[type];
      if (!StudentAnswerType) {
        logger.error(`Unknown StudentAnswerType ${type}`);
      }
      return StudentAnswerType.fromDynamoDB(item);
    });
  }
}

module.exports = ResponseFormAnswer;
