const _ = require('lodash');
const moment = require('moment');
const config = require('config');
const uuid = require('uuid/v4');

const Model = require('../../base');
const dynamodb = require('../../../db');
const logger = require('../../../logger');

const OPEN = 'OPEN';
const REOPEN = 'REOPEN';

class Lesson extends Model {
  static get spec() {
    return {
      id:             { type: "S", readOnly: true },
      lessonId:       { type: "S", readOnly: true },
      userId:         { type: "S" },
      organizationId: { type: "S", readOnly: true },
      courseId:       { type: "S", readOnly: true },
      title:          { type: "S", required: true },
      homework:       { type: "BOOL", required: true },
      description:    { type: "S" },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S" },

      content:        { type: "L", field: "S", readOnly: true },

      rev:            { type: "N" },
      status:         { type: "S" }
    }
  }

  /**
   * @param {String} userId, required
   * @param {BaseLesson} lesson, required
   * @param {Number} rev
   *
   * @param {Lesson} lesson
   */
  static async create(userId, lesson, rev = 0) {
    const id = uuid();
    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": lesson.organizationId },
        SK:             { "S": `LessonInstance#${id}` },
        id:             { "S": id },
        userId:         { "S": userId },
        courseId:       { "S": lesson.courseId },
        lessonId:       { "S": lesson.id },
        createdAt:      { "S": moment().format() },

        rev:            { "N": `${rev}` },
        content:        { "L": _.map(lesson.content, id => ({"S": id})) },
        status:         { "S": OPEN },

        //TODO: get from parent
        title:          lesson.title && { "S": lesson.title },
        description:    lesson.description && { "S": lesson.description },
        homework:       { "BOOL": !!lesson.homework }
      }
    }

    logger.info('Student.Lesson.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item);
  }

  static async submit(id, organizationId) {
    const lesson = await this.findById(id, organizationId, ["status"]);
    if (!lesson) {
      throw new Error('Lesson doesnt exist');
    }

    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": organizationId },
        SK:             { "S": `LessonInstance#${id}` },
      },
      ExpressionAttributeNames: {
        "#status": "status",
        "#updatedAt": "updatedAt"
      },
      ExpressionAttributeValues: {
        ":status":    { "S": "SUBMITTED" },
        ":updatedAt": { "S": moment().format() }
      },
      UpdateExpression: "SET #status = :status, #updatedAt = :updatedAt",
      ReturnValues: "ALL_NEW"
    }

    logger.info('Student.Lesson.submit [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes);
  }

  static async accept(id, organizationId) {
    const lesson = await this.findById(id, organizationId, ["status"]);
    if (!lesson) {
      throw new Error('Lesson doesnt exist');
    }

    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": organizationId },
        SK:             { "S": `LessonInstance#${id}` },
      },
      ExpressionAttributeNames: {
        "#status": "status",
        "#updatedAt": "updatedAt"
      },
      ExpressionAttributeValues: {
        ":status":    { "S": "COMPLETED" },
        ":updatedAt": { "S": moment().format() }
      },
      UpdateExpression: "SET #status = :status, #updatedAt = :updatedAt",
      ReturnValues: "ALL_NEW"
    }

    logger.info('Student.Lesson.accept [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes);
  }

  static async reopen(id, organizationId) {
    const lesson = await this.findById(id, organizationId, ["status", "rev"]);
    if (!lesson) {
      throw new Error('Lesson doesnt exist');
    }

    if (lesson.status !== "SUBMITTED") {
      throw new Error('You cannot open only submitted lesson');
    }

    const rev = (parseInt(lesson.rev) || 0) + 1;
    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": organizationId },
        SK:             { "S": `LessonInstance#${id}` },
      },
      ExpressionAttributeNames: {
        "#status": "status",
        "#rev": "rev",
        "#updatedAt": "updatedAt"
      },
      ExpressionAttributeValues: {
        ":status":    { "S": REOPEN },
        ":updatedAt": { "S": moment().format() },
        ":rev":       { "N": `${rev}` }
      },
      UpdateExpression: "SET #status = :status, #updatedAt = :updatedAt, #rev = :rev",
      ReturnValues: "ALL_NEW"
    }

    logger.info('Student.Lesson.open [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes);
  }

  /**
   * @param {String} userId, required
   * @param {String} lessonId, required
   * @param {Array} projection
   *
   * @return {Lesson} lesson
   */
  static async findByLesson(userId, lessonId, projection) {
    const params = {
      TableName: config.db.tableName,
      IndexName: "ByUser",
      ExpressionAttributeNames: {
        "#id": "userId",
        "#SK": "SK",
        "#lessonId": "lessonId"
      },
      ExpressionAttributeValues: {
        ":id": { "S": userId },
        ":SK": { "S": `LessonInstance#` },
        ":lessonId": { "S": lessonId }
      },
      KeyConditionExpression: "#id = :id AND begins_with ( #SK, :SK )",
      FilterExpression: "#lessonId = :lessonId"
    }

    if (_.isArray(projection)) {
      projection = _.map(projection, field => {
        params.ExpressionAttributeNames[`#${field}`] = field;
        return `#${field}`;
      });
      params.ProjectionExpression = _.join(projection, ', ');
    }

    logger.info('Student.Lesson.findByLesson [DynamoDB]', params);
    const {Items} = await this.dynamodb.query(params).promise();
    if (!Items.length) return null;
    return this.fromDynamoDB(Items[0]);
  }

  static async findById(id, projection) {
    const params = {
      TableName: config.db.tableName,
      IndexName: "BySK",
      Limit: 1,
      ExpressionAttributeNames: {
        "#SK": "SK",
      },
      ExpressionAttributeValues: {
        ":SK": { "S": `LessonInstance#${id}` }
      },
      KeyConditionExpression: "#SK = :SK"
    }

    if (_.isArray(projection)) {
      projection = _.map(projection, field => {
        params.ExpressionAttributeNames[`#${field}`] = field;
        return `#${field}`;
      });
      params.ProjectionExpression = _.join(projection, ', ');
    }

    logger.info('Student.Lesson.findById [DynamoDB]', params);
    const {Items} = await this.dynamodb.query(params).promise();
    if (!Items.length) return null;
    return this.fromDynamoDB(Items[0]);
  }

  /*
   * @filter {Object}
   *  @name {String} courseId [required]
   */
  static async findAll(userId, filter, limit = null, nextToken = null) {
    const params = {
      TableName: config.db.tableName,
      Limit: limit
    }
    if (nextToken) {
      throw new Error('Pagination is not implemented yet');
    }

    if (filter && filter.courseId) {
      params.IndexName = "ByUser";
      params.ExpressionAttributeValues = {
        ":id":       { S: userId },
        ":SK":       { S: "LessonAssignment#" },
        ":courseId": { S: filter.courseId }
      };
      params.KeyConditionExpression = "userId = :id AND begins_with ( SK, :SK )";
      params.FilterExpression = "courseId = :courseId";
    }

    logger.info('Student.Lesson.findAll [DynamoDB]', params);
    const cmd = filter ? this.dynamodb.query : this.dynamodb.scan;
    const res = await cmd.call(this.dynamodb, params).promise();
    nextToken = _.get(res.LastEvaluatedKey, 'id.S', null);
    if (nextToken) {
      throw new Error('Pagination is not implemented yet');
    }

    const qs = _.map(res.Items, async (item) => {
      const lessonId = item.lessonId.S;
      const lesson = await this.findByLesson(userId, lessonId);
      if (!lesson) return null;
      return lesson.serialize();
    });
    const data = await Promise.all(qs);

    return {
      items: _.filter(data),
      nextToken
    }
  }

  static async findAllHomeworks(userId, organizationId) {
    const params = {
      TableName: config.db.tableName,
      IndexName: "ByUser",
      ExpressionAttributeNames: {
        "#id": "userId",
        "#SK": "SK",
      },
      ExpressionAttributeValues: {
        ":id":             { "S": userId },
        ":SK":             { "S": `LessonInstance#` },
        ":homework":       { "BOOL": true }
      },
      KeyConditionExpression: "#id = :id AND begins_with (#SK, :SK)",
      FilterExpression: "homework = :homework"
    }

    if (organizationId) {
      params.ExpressionAttributeValues[":organizationId"] = { S: organizationId };
      params.FilterExpression = "organizationId = :organizationId AND homework = :homework";
    }

    logger.info('Student.Lesson.findAllHomeworks [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    return  _.chain(res.Items)
      .map(item => this.fromDynamoDB(item))
      .orderBy('createdAt', 'desc')
      .value();
  }
}

module.exports = Lesson;
