const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');
const uuid = require('uuid/v4');
const generator = require('generate-password');
const moment = require('moment');

const User = require('../index');
const logger = require('../../../logger');
const { cognito } = require('../../../aws');
const ses = require('../../../services/email');

const Lesson = require('./Lesson');
const LessonComment = require('./LessonComment');

class Student extends User {
  static get Lesson() { return Lesson; }
  static get LessonComment() { return LessonComment; }
  /*
   * @param {Array} input
   *  @item {ID} courseId
   * @return {Array} ids
   */
  static async addCourses(userId, organizationId, input = [], notify = true) {
    if (!_.size(input)) {
      return [];
    }

    const requests = _.map(input, id => (
      {
        PutRequest: {
          Item: {
            organizationId: { "S": organizationId },
            SK:             { "S": `CourseAssignment#${id}#Student#${userId}` },
            userId:         { "S": userId },
            courseId:       { "S": id },
            createdAt:      { "S": moment().format() },
          }
        }
      }
    ));
    let params = {
      RequestItems: {
        [config.db.tableName]: requests
      }
    }

    logger.info('Student.addCourses [DynamoDB]', params);
    const res = await this.dynamodb.batchWriteItem(params).promise();

    //TODO: move out
    if (notify) {
      const user = await User.config(this.ctx).findById(userId);
      ses.send([user.email], 'student-assignCourse', {
        course_url: "${config.site_url}/courses/${input[0]}",
        site_url: config.site_url,
        eduway_email: config.email.support
      }, this.ctx.language);
    }

    return input;
  }

  static async deleteCourses(userId, organizationId, courseIds = []) {
    if (!_.size(courseIds)) {
      return [];
    }

    const items = _.map(courseIds, id => (
      {
        organizationId: { "S": organizationId },
        SK:             { "S": `CourseAssignment#${id}#Student#${userId}` }
      }
    ));
    const res = await this.batchDelete(items, config.db.tableName);
    return courseIds;
  }

  /*
   * @param {Array} lessons
   *  @item {Lesson}
   * @return {Array} lessons
   */
  async addLessons(lessons) {
    if (!_.size(lessons)) {
      return [];
    }
    lessons = _.uniqBy(lessons, 'id');

    return Promise.all(
      _.map(lessons, async (lesson) => this.addLesson(lesson))
    );
  }

  /*
   * @param {Lesson} lesson
   * @return {Lesson} lesson
   */
  async addLesson(lesson) {
    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": lesson.organizationId },
        SK:             { "S": `LessonAssignment#${lesson.id}#Student#${this.id}` },
        userId:         { "S": this.id },
        courseId:       { "S": lesson.courseId },
        lessonId:       { "S": lesson.id },
        createdAt:      { "S": moment().format() },
      }
    }

    const createPromise = Student.Lesson
      .config(this.ctx)
      .create(this.id, lesson, (lesson.rev) || 0);

    const [item, s] = await Promise.all([
      createPromise,
      this.dynamodb.putItem(params).promise()
    ])

    return item;
  }

  /*
   * @param {String} organizationId
   */
  async listHomeworks(organizationId) {
    return Student.Lesson
      .config(this.ctx)
      .findAllHomeworks(this.id, organizationId);
  }

  async listCourses(filter = {}, limit = null, nextToken = null) {
    const Course = require('../../Course');
    const params = {
      TableName: config.db.tableName,
      Limit: limit,
      IndexName: "ByUser",
      ExpressionAttributeValues: {
        ":userId": { S: this.id },
        ":SK":     { S: "CourseAssignment#" }
      },
      KeyConditionExpression: "userId = :userId AND begins_with ( SK, :SK )",
      ProjectionExpression: "courseId"
    }

    if (nextToken) {
      params.ExclusiveStartKey = await Student.decodeNextToken(nextToken);
    }

    logger.info('Student.listCourses [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    nextToken = _.get(res, 'LastEvaluatedKey', null);
    if (nextToken) {
      nextToken = await Student.encodeNextToken(nextToken);
    }

    const qs = _.map(res.Items, async (item) => {
      const course = await Course.config(this.ctx).findById(item.courseId.S);
      if (!course) return null;
      return course.serialize();
    })

    const items = await Promise.all(qs);
    return {
      items: _.filter(items),
      nextToken
    }
  }

  async listCourseIds(filter = {}, limit = null, nextToken = null) {
    const params = {
      TableName: config.db.tableName,
      Limit: limit,
      IndexName: "ByUser",
      ExpressionAttributeValues: {
        ":userId": { S: this.id },
        ":sk": { S: "CourseAssignment#" }
      },
      KeyConditionExpression: "userId = :userId AND begins_with ( SK, :sk )"
    }

    if (nextToken) {
      params.ExclusiveStartKey = await Student.decodeNextToken(nextToken);
    }

    logger.info('Student.listCourses [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    nextToken = _.get(res, 'LastEvaluatedKey', null);
    if (nextToken) {
      nextToken = await Student.encodeNextToken(nextToken);
    }

    const items = _.chain(res)
      .get('Items', [])
      .map(item => ({id: item.courseId.S}))
      .value();

    return {
      items: items,
      nextToken
    }
  }

  /*
   * @param {Array} input
   *  @item {ID} lessonId
   * @return {Array} ids
   */
  static async deleteLessons(userId, organizationId, input) {
    if (!_.size(input)) {
      return [];
    }
    input = _.uniq(input);

    const items = _.map(input, (id) => ({
      organizationId: { "S": organizationId },
      SK:             { "S": `LessonAssignment#${id}#Student#${userId}` }
    }))
    await this.batchDelete(items, config.db.tableName);
    return input;
  }

  /**
   * @param {Object} input
   *  @key {String} email
   *  @key {ID} organizationId
   */
  static async invite(input = {}) {
    let user = await this.findByEmail(input.email);
    if (!user) {
      user = await this.create(input);
    }

    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": input.organizationId },
        SK:             { "S": `Student#${user.id}` },
        userId:         { "S": user.id },
        createdAt:      { "S": moment().format() },
      }
    }

    logger.info('Student.invite [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return user;
  }

  /**
   * @param {Object} input
   *  @key {ID} organizationId
   *  @key {ID} id
   */
  static async delete(input = {}) {
    const params = {
      TableName: config.db.tableName,
      Key: {
        "organizationId": { S: input.organizationId },
        "SK":             { S: `Student#${input.id}` }
      }
    }

    logger.info('Student.delete [DynamoDB]', params);
    const res = await this.dynamodb.deleteItem(params).promise();
    return this.findById(input.id);
  }

  static async create(input = {}) {
    const id = uuid();
    const password = generator.generate({ length: 10, numbers: true });

    const params = {
      TableName: config.db.userTableName,
      Item: {
        id:        { "S": id },
        createdAt: { "S": (new Date()).toISOString() },
        email:     { "S": _.toLower(input.email) }
      }
    }

    if (input.name) {
      params.Item.name = { "S": input.name };
    }
    if (input.surname) {
      params.Item.surname = { "S": input.surname };
    }
    if (input.phone) {
      params.Item.phone = { "S": input.phone };
    }
    if (input.age) {
      params.Item.age = { "S": input.age };
    }

    const cognitoParams = {
      UserPoolId: config.cognito.pool,
      Username: id,
      TemporaryPassword: password,
      MessageAction: "SUPPRESS",
      UserAttributes: [
        {
          Name: "email_verified",
          Value: 'True'
        },
        {
          Name: "email",
          Value: _.toLower(input.email)
        }
      ]
    }

    logger.info('Student.create [DynamoDB]', params);
    logger.info('Student.create [Cognito]', _.extend({}, cognitoParams, {TemporaryPassword: "***"}));
    await Promise.all([
      this.dynamodb.putItem(params).promise(),
      cognito.adminCreateUser(cognitoParams).promise()
    ]);

    ses.send([input.email], 'student-invite', {
      credentials: {
        email: input.email,
        password
      },
      site_url: config.site_url,
      eduway_email: config.email.support
    }, this.ctx.language);

    return this.fromDynamoDB(params.Item);
  }

  static async findById(id) {
    const params = {
      TableName: config.db.userTableName,
      Limit: 1,
      ExpressionAttributeValues: {
        ":id": { S: id }
      },
      KeyConditionExpression: "id = :id"
    }

    logger.info('Student.findById [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    const data = _.get(res, 'Items[0]');
    if (!data) return null;
    return this.fromDynamoDB(data);
  }

  static async update(input = {}) {
    const params = this.getUpdateParams(input);
    params.TableName = config.db.userTableName;
    params.Key = {
      id: { "S": input.id }
    };

    logger.info('Student.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes);
  }

  /**
   * @param {Object} filter
   *  @key {String} organizationId
   */
  static async findAll(filter, limit = 10, nextToken = null) {
    return User
      .config(this.ctx)
      .findAll('Student', filter, limit, nextToken);
  }
}

module.exports = Student;
