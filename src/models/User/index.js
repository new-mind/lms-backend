const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');
const uuid = require('uuid/v4');
const moment = require('moment');
const generator = require('generate-password');

const Model = require('../base');
const logger = require('../../logger');
const { cognito } = require('../../aws');

class User extends Model {
  static get spec() {
    return {
      id:        { type: "S", readOnly: true },
      email:     { type: "S", readOnly: true },
      name:      { type: "S" },
      surname:   { type: "S" },
      phone:     { type: "S" },
      age:       { type: "S" },
      updatedAt: { type: "S" },
      createdAt: { type: "S", readOnly: "S" }
    };
  }

  static async create(input = {}) {
    const id = uuid();
    const password = generator.generate({ length: 10, numbers: true });

    const params = {
      TableName: config.db.userTableName,
      Item: {
        id:        { "S": id },
        createdAt: { "S": moment().format() },
        email:     { "S": _.toLower(input.email) }
      }
    }

    if (input.name) {
      params.Item.name = { "S": input.name };
    }
    if (input.surname) {
      params.Item.surname = { "S": input.surname };
    }
    if (input.phone) {
      params.Item.phone = { "S": input.phone };
    }
    if (input.age) {
      params.Item.age = { "S": input.age };
    }

    const cognitoParams = {
      UserPoolId: config.cognito.pool,
      Username: id,
      TemporaryPassword: password,
      MessageAction: "SUPPRESS",
      UserAttributes: [
        {
          Name: "email_verified",
          Value: 'True'
        },
        {
          Name: "email",
          Value: _.toLower(_.trim(input.email))
        }
      ]
    }

    logger.info('User.create [DynamoDB]', params);
    logger.info('User.create [Cognito]', _.extend({}, cognitoParams, {TemporaryPassword: "***"}));
    await Promise.all([
      this.dynamodb.putItem(params).promise(),
      cognito.adminCreateUser(cognitoParams).promise()
    ]);

    const user = this.fromDynamoDB(params.Item);
    user.password = password; //TODO
    return user;
  }

  static async update(input = {}) {
    input.updatedAt = moment().format()
    const params = User.getUpdateParams(input);
    params.TableName = config.db.userTableName;
    params.Key = {
      id: { S: input.id }
    };

    logger.info('User.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes);
  }

  static async findById(id) {
    const params = {
      TableName: config.db.userTableName,
      Limit: 1,
      ExpressionAttributeValues: {
        ":id": { S: id }
      },
      KeyConditionExpression: "id = :id"
    }

    logger.info('User.findById [DynamoDB]', params);
    const res = await this.dynamodb.query(params).promise();
    const data = _.get(res, 'Items[0]');
    if (!data) return null;
    return this.fromDynamoDB(data);
  }

  static async findByEmail(email) {
    email = _.toLower(_.trim(email));
    const params = {
      TableName: config.db.userTableName,
      IndexName: "ByEmail",
      Limit: 1,
      ExpressionAttributeValues: {
        ":email": { S: email }
      },
      KeyConditionExpression: "email = :email"
    }

    const res = await this.dynamodb.query(params).promise();
    const data = _.get(res, 'Items[0]');
    if (!data) return null;
    return this.fromDynamoDB(data);
  }

  /**
   * @param {Object} filter
   *  @key {String} organizationId
   */
  static async findAll(prefix, filter, limit = 10, nextToken = null) {
    let params = {
      TableName: config.db.tableName,
      Limit: limit
    }

    if (nextToken) {
      params.ExclusiveStartKey = await this.decodeNextToken(nextToken);
    }

    if (filter && filter.organizationId) {
      params.ExpressionAttributeValues = {
        ":id": { S: filter.organizationId },
        ":sk": { S: `${prefix}#` }
      };
      params.KeyConditionExpression = "organizationId = :id AND begins_with ( SK, :sk )";
    }
    if (!filter) {
      throw new Error('filter MUST be provided');
    }

    logger.info(`${this.name}.findAll.1 [DynamoDB]`, params);
    const cmd = filter ? this.dynamodb.query : this.dynamodb.scan;
    let res = await cmd.call(this.dynamodb, params).promise();

    if (res.LastEvaluatedKey) {
      nextToken = await this.encodeNextToken(res.LastEvaluatedKey);
    }

    //TODO
    const keys = _.map(res.Items, item => ({id: item.userId}) );
    if (!keys.length) {
      return {
        items: [],
        nextToken: null
      }
    }

    params = {
      RequestItems: {
        [config.db.userTableName]: {
          Keys: keys
        }
      }
    }

    logger.info(`${this.name}.findAll.2 [DynamoDB]`, params);
    res = await this.dynamodb.batchGetItem(params).promise();
    const data = _.chain(res)
        .get(`Responses.${config.db.userTableName}`)
        .map(item => this.fromDynamoDB(item).serialize())
        .value();

    return {
      items: data,
      nextToken
    }
  }
}

module.exports = User;
