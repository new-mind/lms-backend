const config = require('config');
const moment = require('moment');
const _ = require('lodash');
const generator = require('generate-password');

const User = require('./index');
const Organization = require('../Organization');
const logger = require('../../logger');
const { cognito } = require('../../aws');
const ses = require('../../services/email');

class Admin extends User {
  static async invite(email, organizationId) {
    email = _.toLower(email);
    let [user, organization] = await Promise.all([
      this.findByEmail(email),
      Organization.config(this.ctx).findById(organizationId)
    ]);

    if (!organization) {
      throw new Error(`There is no such organization ${organizationId}`);
    }
    if (!user) {
      user = await this.create({email});
      await ses.send([user.email], 'admin-invite', {
        credentials: {
          email: user.email,
          password: user.password
        },
        site_url: config.site_url,
        eduway_email: config.email.support
      }, this.ctx.language);
    } else {
      await ses.send([user.email], 'admin-inviteRegistered', {
        site_url: config.site_url,
        eduway_email: config.email.support,
        org_name: organization.name
      }, this.ctx.language);
    }

    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": organizationId },
        SK:             { "S": `Admin#${user.id}` },
        userId:         { "S": user.id },
        createdAt:      { "S": moment().format() }
      }
    }

    logger.info('Admin.invite [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return user;
  }

  static async reinvite(email, organizationId) {
    email = _.toLower(email);
    let [user, organization, membership] = await Promise.all([
      this.findByEmail(email),
      Organization.config(this.ctx).findById(organizationId),
    ]);
    if (!user) {
      throw new Error(`There is no such user with email ${email}`);
    }
    if (!organization) {
      throw new Error(`There is no such organization ${organizationId}`);
    }

    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { "S": organizationId },
        SK:             { "S": `Admin#${user.id}` }
      }
    }
    let res = await this.dynamodb.getItem(params).promise();
    if (!res.Item) {
      throw new Error(`The user is not an admin of organization`);
    }

    const password = generator.generate({ length: 10, numbers: true });
    const cognitoParams = {
      UserPoolId: config.cognito.pool,
      Username: user.id,
      Password: password,
      Permanent: false,
    };

    res = await cognito.adminSetUserPassword(cognitoParams).promise()

    await ses.send([email], 'admin-invite', {
      site_url: config.site_url,
      eduway_email: config.email.support,
      org_name: organization.name,
      credentials: {
        email: email,
        password: password
      }
    }, this.ctx.language);
    return user;
  }
}

module.exports = Admin;
