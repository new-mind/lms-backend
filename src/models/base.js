const AWS = require('aws-sdk');
const _ = require('lodash');
const zlib = require('zlib');

const logger = require('../logger');
const { DynamoDB } = require('../aws');

class Model {
  static get dynamodb() {
    throw new Error('MUST be connected before usage');
  }

  static get ctx() {
    throw new Error('MUST be configured before usage');
  }

  static get spec() {
    throw new Error('MUST be implemented in the subclass');
  }

  /**
   * @param {Object} ctx
   *  @key {DynamoDB} dynamodb
   *  @key {Object} credentials
   *  @key {String} language
   */
  static config(ctx) {
    const dynamodb = ctx.dynamodb instanceof AWS.DynamoDB ?
      ctx.dynamodb : DynamoDB(ctx.credentials);

    let ConfiguredModel = class extends this {
      static get dynamodb() { return dynamodb; }

      static get ctx() {
        return {
          dynamodb,
          language: ctx.language
        };
      }
    }
    Object.defineProperty(ConfiguredModel, 'name', {value: `Configured${this.name}`});

    return ConfiguredModel;
  }

  get dynamodb() { return this.ctx.dynamodb; }
  get ctx() { return this._ctx; }

  constructor(params) {
    this._fields = {};

    if (!params) return;

    const Class = this.constructor;
    const spec = Class.spec;
    _.forIn(params, (v, k) => {
      if (!spec[k]) return;

      this._fields[k] = v || this.getDefaultValue(spec[k]);
      Object.defineProperty(this, k, {
        value: this._fields[k]
      });
    })
  }

  getDefaultValue(spec) {
    if (spec.default) return spec.default;
    if (spec.type === "BOOL") return false;
    return null;
  }

  config(ctx) {
    const dynamodb = ctx.dynamodb instanceof AWS.DynamoDB ?
      ctx.dynamodb : DynamoDB(ctx.credentials);

    this._ctx = {
      dynamodb,
      language: ctx.language
    };
    return this;
  }

  static async encodeNextToken(nextToken) {
    return new Promise((resolve, reject) => {
      zlib.deflateRaw(JSON.stringify(nextToken), (err, buffer) => {
        if (err) {
          return reject(err);
        }

        return resolve(buffer.toString('base64'));
      });
    });
  }

  static async decodeNextToken(nextToken) {
    return new Promise((resolve, reject) => {
      nextToken = Buffer.from(nextToken, 'base64');
      zlib.inflateRaw(nextToken, (err, buffer) => {
        if (err) {
          return reject(err);
        }
        return resolve(JSON.parse(buffer.toString()));
      });
    });
  }

  static getUpdateParams(input = {}) {
    input.updatedAt = (new Date()).toISOString();

    const expSet = {};
    const expRemove = [];
    const expValues = {};
    const expNames = {};

    _.forIn(input, (v, k) => {
      const field = this.spec[k];
      if (!field || field.readOnly) return;

      if (_.isNull(v)) {
        if (field.required) return;
        expRemove.push(`#${k}`);
        expNames[`#${k}`] = `${k}`;
      } else {
        expValues[`:${k}`] = { [field.type]: v };
        expNames[`#${k}`] = `${k}`;
        expSet[v] = k;
      }
    });

    const expressions = [];
    if (expRemove.length) {
      expressions.push(`REMOVE ${expRemove.join(',')}`);
    }

    if (!_.isEmpty(expSet)) {
      let exps = _.map(expSet, (k, v) => `#${k} = :${k}`);
      expressions.push(`SET ${exps.join(', ')}`);
    }

    if (!expressions.length) return;

    const params = {
      ExpressionAttributeNames: expNames,
      ExpressionAttributeValues: expValues,
      UpdateExpression: expressions.join(' '),
      ReturnValues: "ALL_NEW"
    }

    return params;
  }

  static async batchDelete(items, tableName) {
    const chunks = _.chunk(items, 25);
    const qs = [];

    _.forEach(chunks, chunk => {
      if (!_.size(chunk)) return;
      const requests = []

      _.forEach(chunk, item => {
        const request = {
          DeleteRequest: { Key: item }
        };
        requests.push(request);
      })

      const params = {
        RequestItems: {
          [tableName]: requests
        }
      }

      logger.info(`${this.name}.batchDelete [DynamoDB]`, params);
      qs.push(this.dynamodb.batchWriteItem(params).promise());
    });

    return Promise.all(qs);
  }

  serialize() {
    return _.extend({}, this._fields);
  }

  static fromDynamoDB(Item) {
    const Class = this;
    const data = parseSpec(Item, Class.spec);

    function parseSpec(root, spec) {
      const params = {};
      _.forIn(spec, (v, k) => {
        let key = v.dynamoDBKey || k;
        let type = v.type;

        switch (type) {
          case 'L':
            const list = _.get(root, `${key}.${type}`);
            const f = v.field;
            params[k] = _.map(list, (item) => {
              if (f === "M") {
                return parseSpec(item[f], v.spec);
              }
              return item[f];
            });
            break;
          default:
            params[k] = _.get(root, `${key}.${type}`);
            break;
        }
      });
      return params;
    }

    return new Class(data);
  }
}

module.exports = Model;
