const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');
const uuid = require('uuid/v4');

const Model = require('./base');

const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

class Lesson extends Model {
  static get spec() {
    return {
      id: { type: "S", readOnly: true },
      organizationId: { type: "S", readOnly: true },
      courseId: { type: "S", readOnly: true },

      title: { type: "S", required: true },
      homework: { type: "B", required: true },
      description: { type: "S" },
      updatedAt: { type: "S" },
      createdAt: { type: "S" }
    }
  }

  constructor(params) {
    super();

    this.id = params.id;
    this.courseId = params.courseId;
    this.courseLessonId = params.courseId; // TODO
    this.title = params.title;
    this.homework = params.homework || false;
    this.updatedAt = params.updatedAt || new Date();
    this.createdAt = params.createdAt || new Date();
    this.description = params.description || null;
    this.contentOrder = params.contentOrder || [];
  }

  serialize() {
    return {
      id: this.id,
      courseId: this.courseId,
      courseLessonsId: this.courseId, //TODO
      title: this.title,
      updatedAt: this.updatedAt,
      createdAt: this.createdAt,
      description: this.description,
      contentOrder: this.contentOrder
    }
  }

  static async create(input = {}) {
    const id = uuid();
    const params = {
      TableName: config.db.tableName,
      Item: {
        OrganizationId: { "S": input.organizationId },
        SK: { "S": `Lesson#${id}` },
        CourseId: { "S": courseId },
        LessonId: { "S": id },
        createdAt: { "S": (new Date()).toISOString() }
      }
    }

    if (input.title) {
      params.Item.title = { "S": input.title };
    }
    if (input.description) {
      params.Item.description = { "S": input.description };
    }
    if (_.isBoolean(input.homework)) {
      params.Item.homework = { "B": !!input.homework };
    }

    const res = await dynamodb.putItem(params).promise();
    return Course.fromDynamoDB(params.Item);
  }

  static fromDynamoDB(Item) {
    return new Lesson({
      id: _.get(Item, 'id.S'),
      courseId: _.get(Item, 'courseLessonsId.S'), //TODO
      title: _.get(Item, 'title.S'),
      updatedAt: _.get(Item, 'updatedAt.S'),
      createdAt: _.get(Item, 'createdAt.S'),
      description: _.get(Item, 'description.S'),
      contentOrder: _.chain(Item).get('contentOrder.L').map('S').value()
    })
  }

  static async findById(id) {
    const params = {
      TableName: config.db.table.lesson,
      Key: {
        id: { S: id }
      }
    }
    const res = await dynamodb.getItem(params).promise();
    const data = _.get(res, 'Item');
    return Lesson.fromDynamoDB(data);
  }

  /*
   * @filter {Object}
   *  @name {String} courseId
   */
  static async findAll(filter, limit = 10, nextToken = null) {
    const params = {
      TableName: config.db.table.lesson,
      Limit: limit
    }
    if (nextToken) {
      nextToken = Buffer.from(nextToken, 'base64').toString();
      params.ExclusiveStartKey = {
        id: { S: nextToken }
      }
    }

    if (filter && filter.courseId) {
      params.IndexName = "gsi-Course.lessons";
      params.ExpressionAttributeValues = {
        ":id": { S: filter.courseId }
      };
      params.KeyConditionExpression = "courseLessonsId = :id";
    }
    const cmd = filter ? dynamodb.query : dynamodb.scan;
    const res = await cmd.call(dynamodb, params).promise();
    nextToken = _.get(res.LastEvaluatedKey, 'id.S', null);
    if (nextToken) {
      nextToken = Buffer.from(nextToken).toString('base64');
    }
    const data = _.chain(res.Items)
        .map(item => Lesson.fromDynamoDB(item).serialize())
        .value();

    return {
      items: data,
      nextToken
    }
  }
}

module.exports = Lesson;
