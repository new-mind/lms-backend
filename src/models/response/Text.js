const uuid = require('uuid/v4');
const moment = require('moment');
const config = require('config');
const _ = require('lodash');

const Model = require('../base');
const logger = require('../../logger');
const dynamodb = require('../../db');

class Text extends Model {
  static get spec() {
    return {
      id:             { type: "S", readOnly: true },
      organizationId: { type: "S", readOnly: true },
      lessonId:       { type: "S", readOnly: true },
      sectionId:      { type: "S", readOnly: true },
      type:           { type: "S", readOnly: true },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S", readOnly: true },

      data:           { type: "S" }
    }
  }

  /**
   * @params {Object} input
   *  @key {Number} rev, default = 0
   *  @key {String} lessonId, required
   *  @key {String} sectionId, required
   *  @key {String} organizationId, required
   */
  static async create(input = {}) {
    const { rev, lessonId, sectionId, organizationId } = input;

    const id = uuid();
    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": organizationId },
        SK:             { "S": `Lesson#${lessonId}#Response#${sectionId}#rev${rev}` },
        type:           { "S": "TextResponse" },
        id:             { "S": id },
        sectionId:      { "S": sectionId },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": moment().format() },
        rev:            { "N": `${rev || 0}` },

        data:           input.data && { "S": input.data }
      }
    }

    logger.info('Response.Text.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }
}

module.exports = Text;
