const uuid = require('uuid/v4');
const moment = require('moment');
const config = require('config');
const _ = require('lodash');

const Model = require('../base');
const logger = require('../../logger');
const dynamodb = require('../../db');

class Attachment extends Model {
  static get spec() {
    return {
      id:             { type: "S", readOnly: true },
      organizationId: { type: "S", readOnly: true },
      lessonId:       { type: "S", readOnly: true },
      sectionId:      { type: "S", readOnly: true },
      type:           { type: "S", readOnly: true },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S", readOnly: true },

      items:          { type: "L", field: "M", spec: {
        id:         { type: "S" },
        fileKey:    { type: "S" },
        fileName:   { type: "S" },
        identityId: { type: "S" },
        title:      { type: "S" }
      }}
    }
  }

  /**
   * @params {Object} input
   *  @key {Number} rev, default = 0
   *  @key {String} lessonId, required
   *  @key {String} sectionId, required
   *  @key {String} organizationId, required
   *
   *  @key {Array} items
   */
  static async create(input = {}) {
    const { rev, lessonId, sectionId, organizationId } = input;

    const id = uuid();

    const items = _.map(input.items, ({id, title, fileName, fileKey, identityId}) => {
      return {
        "M": {
          id:         { "S": id },
          identityId: identityId && { "S": identityId },
          title:      title && { "S": title },
          fileName:   fileName && { "S": fileName },
          fileKey:    fileKey && { "S": fileKey }
        }
      }
    });
    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": organizationId },
        SK:             { "S": `Lesson#${lessonId}#Response#${sectionId}#rev${rev}` },
        type:           { "S": "AttachmentResponse" },
        id:             { "S": id },
        sectionId:      { "S": sectionId },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": moment().format() },
        rev:            { "N": `${rev || 0}` },

        items:          items.length ? { "L": items } : null
      }
    }

    logger.info('Response.Attachment.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }

  /**
   * @params {Object} input
   *  @key {Number} rev, default = 0
   *  @key {String} lessonId, required
   *  @key {String} sectionId, required
   *  @key {String} organizationId, required
   *
   *  TODO: items are overriden
   */
  static async update(input = {}) {
    const { organizationId, lessonId, sectionId, rev } = input;

    if (input.items) {
      input.items = _.map(input.items, item => {
        const data = {};
        if (item.id) {
          data.id = { "S": item.id };
        }
        if (item.identityId) {
          data.identityId = { "S": item.identityId };
        }
        if (item.fileKey) {
          data.fileKey = { "S": item.fileKey };
        }
        if (item.fileName) {
          data.fileName = { "S": item.fileName };
        }
        if (item.title) {
          data.title = { "S": item.title };
        }
        return {
          "M": data
        };
      });
    }
    const params = this.getUpdateParams(input);

    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK:             { "S": `Lesson#${lessonId}#Response#${sectionId}#rev${rev || 0}` }
    };

    logger.info('Response.Attachment.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }
}

module.exports = Attachment;
