const uuid = require('uuid/v4');
const moment = require('moment');
const config = require('config');
const _ = require('lodash');

const Model = require('../base');
const logger = require('../../logger');
const dynamodb = require('../../db');

class Link extends Model {
  static get spec() {
    return {
      id:             { type: "S", readOnly: true },
      organizationId: { type: "S", readOnly: true },
      lessonId:       { type: "S", readOnly: true },
      sectionId:      { type: "S", readOnly: true },
      type:           { type: "S", readOnly: true },
      updatedAt:      { type: "S" },
      createdAt:      { type: "S", readOnly: true },
      rev:            { type: "S", readOnly: true },

      items:          { type: "L", field: "M", spec: {
        id:    { type: "S" },
        url:   { type: "S" },
        title: { type: "S" }
      }},
    }
  }

  /**
   * @params {Object} input
   *  @key {Number} rev, default = 0
   *  @key {String} lessonId, required
   *  @key {String} sectionId, required
   *  @key {String} organizationId, required
   */
  static async create(input = {}) {
    const { rev, lessonId, sectionId, organizationId } = input;

    const id = uuid();

    const items = _.map(input.items, ({id, title, url}) => {
      return {
        "M": {
          id: { "S": id },
          title: title && { "S": title },
          url:   url && { "S": url }
        }
      }
    });
    const params = {
      TableName: config.db.tableName,
      Item: {
        organizationId: { "S": organizationId },
        SK:             { "S": `Lesson#${lessonId}#Response#${sectionId}#rev${rev}` },
        type:           { "S": "LinkResponse" },
        id:             { "S": id },
        sectionId:      { "S": sectionId },
        lessonId:       { "S": lessonId },
        createdAt:      { "S": moment().format() },
        rev:            { "N": `${rev || 0}` },

        items:          items.length ? { "L": items } : null
      }
    }

    logger.info('Response.Link.create [DynamoDB]', params);
    const res = await this.dynamodb.putItem(params).promise();
    return this.fromDynamoDB(params.Item).config(this.ctx);
  }

  /**
   * @params {Object} input
   *  @key {Number} rev, default = 0
   *  @key {String} lessonId, required
   *  @key {String} sectionId, required
   *  @key {String} organizationId, required
   *
   * TODO: items are overriden
   */
  static async update(input = {}) {
    const { organizationId, lessonId, sectionId, rev } = input;

    // TODO: move to base
    if (input.items) {
      input.items = _.map(input.items, item => {
        const data = {};
        if (item.id) {
          data.id = { "S": item.id };
        }
        if (item.url) {
          data.url = { "S": item.url };
        }
        if (item.title) {
          data.title = { "S": item.title };
        }
        return {
          "M": data
        };
      });
    }
    const params = this.getUpdateParams(input);
    params.TableName = config.db.tableName;
    params.Key = {
      organizationId: { "S": organizationId },
      SK:             { "S": `Lesson#${lessonId}#Response#${sectionId}#rev${rev || 0}` }
    };

    logger.info('Response.Link.update [DynamoDB]', params);
    const res = await this.dynamodb.updateItem(params).promise();
    return this.fromDynamoDB(res.Attributes).config(this.ctx);
  }
}

module.exports = Link;
