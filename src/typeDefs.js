const { gql } = require('apollo-server');

const typeDefs = [
  gql`
    type Query {
      dev: String
    }
    type Mutation {
      dev: String
    }
  `,
  require('./schema/Course.graphql.js'),
  require('./schema/Lesson.graphql.js'),
  require('./schema/Organization.graphql.js'),

  require('./schema/Student/Student.graphql.js'),
  require('./schema/Student/Lesson.graphql.js'),
  require('./schema/Student/LessonComment.graphql.js'),
  ...require('./schema/Student/Answer'),

  require('./schema/Admin.graphql.js'),
  require('./schema/Teacher.graphql.js'),
  require('./schema/User.graphql.js'),

  require('./schema/Section.graphql.js'),
  require('./schema/widget/Attachment.graphql.js'),
  require('./schema/widget/Image.graphql.js'),
  require('./schema/widget/Link.graphql.js'),
  require('./schema/widget/MultiChoice.graphql.js'),
  require('./schema/widget/ResponseForm.graphql.js'),
  require('./schema/widget/Text.graphql.js'),
  require('./schema/widget/Video.graphql.js'),

  require('./schema/Response.graphql.js'),
  require('./schema/response/Attachment.graphql.js'),
  require('./schema/response/Link.graphql.js'),
  require('./schema/response/Text.graphql.js'),
]

module.exports = typeDefs
