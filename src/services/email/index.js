const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('config');
const Handlebars = require('handlebars');
const fs = require('fs');

const { ses } = require('../../aws');

const templates = {
  ru: {
    'student-invite': {
      subject: 'Добро пожаловать',
      data: require('./templates/ru/student/invite.hbs')
    },
    'student-assignCourse': {
      subject: 'Вам добавлен курс',
      data: require('./templates/ru/student/assignCourse.hbs')
    },
    'admin-invite': {
      subject: 'Добро пожаловать',
      data: require('./templates/ru/admin/invite.hbs')
    },
    'admin-inviteRegistered': {
      subject: 'Вы добавлены в организацию',
      data: require('./templates/ru/admin/inviteRegistered.hbs')
    }
  },
  en: {
    'student-invite': {
      subject: 'Welcome',
      data: require('./templates/en/student/invite.hbs')
    },
    'student-assignCourse': {
      subject: 'The course was assigned to you',
      data: require('./templates/en/student/assignCourse.hbs')
    },
    'admin-invite': {
      subject: 'Welcome',
      data: require('./templates/en/admin/invite.hbs')
    },
    'admin-inviteRegistered': {
      subject: 'You have beed added to organization',
      data: require('./templates/en/admin/inviteRegistered.hbs')
    }
  }
};

class SES {
  get templates() {
    return templates;
  }
  async send(to, templateName, data, lng="en") {
    if (!_.size(to)) return null;

    lng = _.toLower(lng).slice(0, 2);
    let tmpl = templates[lng];
    if (!tmpl) {
      console.error(`The language is not supported ${lng}`);
      return;
    }
    tmpl = tmpl[templateName];
    if (!tmpl) {
      console.error(`There is no such email template ${templateName}`);
      return;
    }

    let html;
    let subject;
    try {
      html = tmpl.data(data);
      subject = tmpl.subject;
    } catch (e) {
      console.error(`There is an error during compiling email template ${e}`);
      return;
    }
    const params = {
      Destination: {
        ToAddresses: to
      },
      Source: config.email.source,
      Message: {
        Subject: {
          Charset: 'UTF-8',
          Data: subject
        },
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: html
          }
        }
      }
    };

    const res = await ses.sendEmail(params).promise();
  }
}

module.exports = new SES();
