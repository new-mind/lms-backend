require('module-alias/register');

const { ApolloServer, gql } = require('apollo-server');
const AWS = require('aws-sdk');
const config = require('config');

const logger = require('./logger');
const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  formatError: (error) => {
    logger.error(error.originalError);
    return error;
  },
  context: async ({ req }) => {
    const jwtToken = req.headers.authorization || null;
    const params = {
      IdentityPoolId: config.cognito.identityPool,
      Logins: {
        [`cognito-idp.${config.region}.amazonaws.com/${config.cognito.pool}`]: jwtToken
      }
    }
    const credentials = new AWS.CognitoIdentityCredentials(params, {region: config.region});
    const language = req.headers['accept-language'];

    return { credentials, language };
  }
});

server.listen().then(({url}) => {
  logger.info(`Server is ready on ${url}`);
});
