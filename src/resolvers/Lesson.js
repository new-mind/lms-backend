const _ = require('lodash');

const Lesson = require('../models/Lesson');
const Course = require('../models/Course');
const Section = require('../models/Section');

const Resolvers = {
  content: async (parent, args, ctx) => {
    const lesson = new Lesson(parent).config(ctx);
    return lesson.getContent();
  },
  course: async (parent, args, ctx) => {
    const course = await Course.config(ctx).findById(parent.courseId);
    return course.serialize();
  },
  students: async (parent, args, ctx) => {
    const lesson = new Lesson(parent).config(ctx);
    return lesson.listStudents();
  }
}

const Query = {
  listLessons: async (parent, args, ctx) => {
    return Lesson.config(ctx).findAll(args.filter, args.limit, args.nextToken);
  },
  getLesson: async (parent, args, ctx) => {
    const lesson = await Lesson.config(ctx).findById(args.id);
    if (!lesson) return null;
    return lesson.serialize();
  }
}

const Mutation = {
  createLesson: async (parent, args, ctx) => {
    const course = await Course.config(ctx).findById(args.input.courseId, ['courseId', 'content']);
    const lesson = await course.config(ctx).createLesson(args.input);
    return lesson.serialize();
  },
  updateLesson: async (parent, args, ctx) => {
    const model = await Lesson.config(ctx).update(args.input);
    return model.serialize();
  },
  publishLesson: async (parent, args, ctx) => {
    const model = await Lesson.config(ctx).publish(args.id);
    return model.serialize();
  },
  deleteLesson: async (parent, args, ctx) => {
    const model = await Course.config(ctx).deleteLesson(args.id);
    if (!model) return null;
    return model.serialize();
  },
  Lesson: async (parent, args, ctx) => {
    const lesson = await Lesson.config(ctx).findById(args.id);
    if (!lesson) {
      throw new Error('Current lesson doesn\'t exist');
    }

    return {
      ...lesson,
      addSection: async (args) => {
        if (!args.input) {
          throw new Error('MUST provide input field');
        }

        const [section, position] = await lesson.addSection(args.input);
        return {
          section: section.serialize(),
          position
        }
      },
      addStudents: async (args) => {
        const qs = _.map(args.students, async (id) => {
          await lesson.addStudent(id)
          return id;
        });

        return Promise.all(qs);
      },
      moveSection: async (args) => {
        const [section, position] = await lesson.moveSection(args.id, args.position);
        return {
          section: section.serialize(),
          position
        }
      },
      deleteSection: async (args) => {
        const [section, position] = await lesson.deleteSection(args.id);
        return {
          section: section.serialize(),
          position
        }
      }
    }
  }
}

module.exports = { Resolvers, Query, Mutation }
