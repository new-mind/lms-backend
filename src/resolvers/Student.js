const _ = require('lodash');

const Student = require('../models/User/Student');
const Course = require('../models/Course');
const Lesson = require('../models/Lesson');

const Resolvers = {
  //TODO: merge with courseIds
  courses: async (student, args, ctx) => {
    const res = await student.config(ctx)
      .listCourses(args.filter, args.limit, args.nextToken);
    return res;
  },
  homeworks: async (student, args, ctx) => {
    student = new Student(student).config(ctx);

    const res = await student.listHomeworks();
    if (!res) return null;
    return _.map(res, homework => homework.serialize());
  },
  homeworksInOrg: async (student, args, ctx) => {
    student = new Student(student).config(ctx);

    const res = await student.listHomeworks(args.organizationId);
    if (!res) return null;
    return _.map(res, homework => homework.serialize());
  },
  courseIds: async (data, args, ctx) => {
    const student = new Student(data).config(ctx);
    const res = await student.listCourseIds(args.filter, args.limit, args.nextToken);
    return _.map(res.items, 'id') || [];
  },
}

const Query = {
  listStudents: async (parent, args, ctx) => {
    return Student
      .config(ctx)
      .findAll(args.filter, args.limit, args.nextToken);
  },
  getStudent: async (parent, args, ctx) => {
    return Student
      .config(ctx)
      .findById(args.id);
  },
  Student: async (parent, args, ctx) => {
    const userId = args.id;

    return {
      id: userId,
      getCourse: async (args) => {
        const course = await Course.config(ctx).findById(args.id);
        course.scoped = {
          scope: "Student",
          userId
        }
        return course;
      },
      getLesson: async (args) => {
        let lesson;
        lesson = await Student.Lesson
          .config(ctx)
          .findById(args.id);
        if (!lesson) return null;
        return lesson.serialize();
      }
    }
  }
}

const Mutation = {
  inviteStudent: async (parent, args, ctx) => {
    const model = await Student
      .config(ctx)
      .invite(args.input);
    return model.serialize();
  },
  inviteStudents: async (parent, {input}, ctx) => {
    const ConfiguredStudent = Student.config(ctx);
    const organizationId = input.organizationId;
    const qs = _.map(input.emails, async (email) => {
      const model = await ConfiguredStudent.invite({email, organizationId});

      if (input.courses) {
        await ConfiguredStudent
          .addCourses(model.id, organizationId, input.courses, false);
      }
      return model.serialize();
    });

    return Promise.all(qs).catch(console.log);
  },
  updateStudent: async (parent, args, ctx) => {
    const model = await Student
      .config(ctx)
      .update(args.input);
    return model.serialize();
  },
  deleteStudent: async (parent, args, ctx) => {
    const model = await Student
      .config(ctx)
      .delete(args.input);
    return model.serialize();
  },
  Student: async (parent, args, ctx) => {
    const userId = args.id;

    return {
      id: userId,
      addCourses: async (args)  => {
        if (!args.input) {
          return [];
        }

        const course = await Course
          .config(ctx)
          .findById(args.input[0]); // TODO

        return Student
          .config(ctx)
          .addCourses(userId, course.organizationId, args.input);
      },
      deleteCourses: async (args) => {
        if (!args.input) {
          return [];
        }

        const course = await Course
          .config(ctx)
          .findById(args.input[0]); // TODO
        return Student
          .config(ctx)
          .deleteCourses(userId, course.organizationId, args.input);
      },
      addLessons: async (args) => {
        if (!args.input) {
          return [];
        }

        const ConfiguredLesson = Lesson.config(ctx);
        const lessons = await Promise.all(
          _.map(args.input, (id) =>  ConfiguredLesson.findById(id))
        );
        const student = new Student({id: userId}).config(ctx);
        return student.addLessons(lessons);
      },
      deleteLessons: async (args) => {
        if (!args.input) {
          return [];
        }

        const lesson = await Lesson
          .config(ctx)
          .findById(args.input[0]); // TODO
        return Student
          .config(ctx)
          .deleteLessons(userId, lesson.organizationId, args.input);
      }
    }
  }
}

module.exports = { Resolvers, Query, Mutation }
