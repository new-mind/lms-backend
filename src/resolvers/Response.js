const _ = require('lodash');

const Response = require('../models/Response');

const Resolvers = {
  __resolveType: (obj, context, info) => {

    switch (obj.type) {
      case 'Text':
      case 'TextResponse':
      case 'TextWidget':
        return 'TextResponse'
      case 'Link':
      case 'LinkResponse':
      case 'LinkWidget':
        return 'LinkResponse'
      case 'Attachment':
      case 'AttachmentResponse':
      case 'AttachmentWidget':
        return 'AttachmentResponse'
      default:
        throw new Error(`Unknown type ${obj.type}`);
    }
  }
}

const Mutation = {
  updateResponse: async (parent, args, ctx) => {
    const model = await Response.config(ctx).update(args.input);
    return model.serialize();
  }
}

module.exports = { Mutation, Resolvers }
