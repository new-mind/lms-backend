const Course = require('../models/Course');
const Student = require('../models/User/Student');
const Lesson = require('../models/Lesson');

const Resolvers = {
  lessons: async (parent, args, ctx) => {
    if (parent.scoped) {
      switch (parent.scoped.scope) {
        case "Student":
          const studentId = parent.scoped.userId;
          const course = parent;
          return await Student.Lesson.config(ctx).findAll(studentId, {courseId: course.id});
          break;
        default:
          throw new Error(`Uknown scope ${parent.scoped.scope}`);
      }
    }
    const course = await Course.config(ctx).findById(parent.id, ['courseId', 'organizationId', 'content']);
    return course.listLessons();
  },
  students: async (parent, args, ctx) => {
    const course = new Course(parent).config(ctx);
    return await course.listStudents();
  }
}

const Query = {
  listCourses: async(parent, args, ctx) => {
    return Course
      .config(ctx)
      .findAll(args.filter, args.limit, args.nextToken);
  },
  getCourse: async (parent, args, ctx) => {
    const model = await Course.config(ctx).findById(args.id);
    if (!model) return null;
    return model.serialize();
  }
}

const Mutation = {
  createCourse: async (parent, args, ctx) => {
    const model = await Course.config(ctx).create(args.input);
    return model.serialize();
  },
  deleteCourse: async (parent, args, ctx) => {
    const model = await Course.config(ctx).removeById(args.id);
    return model.serialize();
  },
  updateCourse: async (parent, args, ctx) => {
    const model = await Course.config(ctx).update(args.input);
    return model.serialize();
  },
  Course: async (parent, args, ctx) => {
    const id = args.id;

    return {
      id,
      moveLesson: async (args) => {
        let course = await Course.config(ctx).findById(id, ['courseId', 'organizationId', 'content']);
        if (!course) {
          throw new Error(`Course ${id} doesn't exist`);
        }
        return course.config(ctx).moveLesson(args.id, args.position);
      }
    }
  }
}

module.exports = { Resolvers, Query, Mutation }
