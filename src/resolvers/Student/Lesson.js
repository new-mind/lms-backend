const _ = require('lodash');

const Student = require('@lms/models/User/Student');
const Course = require('@lms/models/Course');
const Lesson = require('@lms/models/Lesson');

const Resolvers = {
  __name: 'StudentLesson',

  content: async (parent, args, ctx, info) => {
    ctx.userId = parent.userId;
    const lesson = new Lesson({
      id: parent.lessonId,
      organizationId: parent.organizationId,
      content: parent.content
    }).config(ctx);

    return lesson.getContent();
  },
  course: async (parent, args, ctx) => {
    const course = await Course.config(ctx).findById(parent.courseId);
    if (!course) return null;
    return course.serialize();
  },
  student: async (parent, args, ctx) => {
    const model = await Student.config(ctx).findById(parent.userId);
    return model.serialize();
  },
  comments: async (parent, args, ctx) => {
    return Student.LessonComment.config(ctx).findAll({
      organizationId: parent.organizationId,
      lessonId: parent.id,
    }, args.limit, args.nextToken);
  }
}

const Query = {
  getStudentLesson: async (parent, args, ctx) => {
    const lesson = await Student.Lesson
      .config(ctx)
      .findById(args.id, args.organizationId);
    return lesson.serialize();
  }
}

const Mutation = {
  acceptStudentLesson: async (parent, args, ctx) => {
    const model = await Student.Lesson
      .config(ctx)
      .accept(args.id, args.organizationId);
    return model.serialize();
  },
  reopenStudentLesson: async (parent, args, ctx) => {
    const model = await Student.Lesson
      .config(ctx)
      .reopen(args.id, args.organizationId);
    return model.serialize();
  },
  submitStudentLesson: async (parent, args, ctx) => {
    const model = await Student.Lesson
      .config(ctx)
      .submit(args.id, args.organizationId);
    return model.serialize();
  },
}

module.exports = { Resolvers, Query, Mutation }
