require('module-alias/register')

const LessonComment = require('@lms/models/User/Student/LessonComment');

const Resolvers = {
  __name: 'StudentLessonComment',

  author: async () => {
  }
}

const Mutation = {
  addLessonComment: async (parent, args, ctx) => {
    return LessonComment.config(ctx).create(args.input);
  }
}

module.exports = { Mutation, Resolvers };
