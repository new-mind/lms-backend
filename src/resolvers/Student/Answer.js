const requireAnswer = (type) => {
  return require(`@lms/models/User/Student/Answer/${type}`);
}

const modules = {
  TextWidget: requireAnswer('Text'),
  LinkWidget: requireAnswer('Link'),
  AttachmentWidget: requireAnswer('Attachment')
}

const Mutation = {
  updateStudentAnswer: async (parent, args, ctx) => {
    const { type } = args.input;
    let StudentAnswerType = modules[type]; 
    if (!StudentAnswerType)
      throw new Error('Unknown answer type');

    StudentAnswerType = StudentAnswerType.config(ctx);
    let answer = await StudentAnswerType.findOne(args.input);
    if (!answer) {
      answer = await StudentAnswerType.create(args.input);
      return answer.serialize();
    }
    answer = await StudentAnswerType.update(args.input);
    return answer.serialize();
  }
}

module.exports = { Mutation }
