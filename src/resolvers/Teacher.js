const Teacher = require('../models/User/Teacher');

const Query = {
  getTeacher: async (parent, args) => {
  },
  listTeachers: async (parent, args, ctx) => {
    return Teacher
      .config(ctx)
      .findAll(args.filter, args.limit, args.nextToken);
  }
}

const Mutation = {
  inviteTeacher: async (parent, args, ctx) => {
    const model = await Teacher.config(ctx).invite(args.input);
    return model.serialize();
  },
  deleteTeacher: async (parent, args, ctx) => {
    const model = await Teacher.config(ctx).delete(args.input);
    return model.serialize();
  }
}

module.exports = { Query, Mutation }
