const Admin = require('../models/User/Admin');

const Mutation = {
  inviteAdmin: async (parent, args, ctx) => {
    const model = await Admin
      .config(ctx)
      .invite(args.email, args.organizationId);
    return model.serialize();
  },

  reinviteAdmin: async (parent, args, ctx) => {
    const model = await Admin
      .config(ctx)
      .reinvite(args.email, args.organizationId);
    return model.serialize();
  }
}

module.exports = { Mutation }
