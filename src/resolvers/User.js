const Auth = require('../models/Auth');
const User = require('../models/User');

const Query = {
  getRoles: async(parent, args, ctx) => {
    return Auth.config(ctx).getUserRoles(args.id);
  },
  getUser: async (parent, args, ctx) => {
    const user = await User.config(ctx).findById(args.id);
    if (!user) return null;
    return user.serialize();
  }
}

const Resolvers = {
  __name: 'IUser',
  __resolveType: async (obj) => {
    return 'User'; // TODO
  }
}

const Mutation = {
  updateUser: async (parent, args, ctx) => {
    return User.config(ctx).update(args.input);
  }
}

module.exports = { Mutation, Query, Resolvers }
