const Response = require('../../models/Response');
const ResponseFormAnswer = require('@lms/models/User/Student/Answer/ResponseForm');

const Resolvers = {
  __name: 'ResponseFormWidget',

  answers: async (section, args, ctx, info) => {
    const userId = args.userId || ctx.userId;
    if (!userId) {
      return null;
    }

    return ResponseFormAnswer.config(ctx).findAll({
      organizationId: section.organizationId,
      sectionId: section.id,
      userId: userId
    });
  },

  responses: async (section, args, ctx, info) => {
    if (!ctx.userId) {
      return null;
    }

    return Response.config(ctx).findAll({
      organizationId: section.organizationId,
      sectionId: section.id,
      lessonId: section.lessonId
    });
  }
}

module.exports = { Resolvers };
