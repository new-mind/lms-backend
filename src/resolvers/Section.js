const _ = require('lodash');

const Section = require('../models/Section');

const Resolvers = {
  __resolveType: (obj, context, info) => {
    return obj.type;
  },
};

const Query = {
  getSection: async (parent, args) => {
  }
};

const Mutation = {
  updateSection: async (parent, args, ctx) => {
    const model = await Section.config(ctx).update(args.input);
    return model.serialize();
  },
}

module.exports = { Resolvers, Query, Mutation }
