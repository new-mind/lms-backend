const _ = require('lodash');

const Organization = require('../models/Organization');
const Course = require('../models/Course');

const Resolvers = {
  courses: async (parent, args, ctx) => {
    return await Course
      .config(ctx)
      .findAll({ organizationId: parent.id });
  }
}

const Query = {
  getOrganization: async(parent, args, ctx) => {
    const org = await Organization
      .config(ctx)
      .findById(args.id);

    if (!org) return null; // TODO throw 404
    return org.serialize();
  },

  getOrganizations: async(parent, args, ctx) => {
    const qs = _.map(args.ids, async (id) => {
      const org = await Organization
        .config(ctx)
        .findById(id);
      if (!org) return null;
      return org.serialize();
    })

    const res = await Promise.all(qs);
    return _.filter(res);
  }
}

const Mutation = {
  createOrganization: async (parent, args, ctx) => {
    const org = await Organization.config(ctx).create(args.input && args.input.title);
    return org.serialize();
  }
}

module.exports = { Resolvers, Query, Mutation }
