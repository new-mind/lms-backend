const AWS = require('aws-sdk');
const config = require('config');
const logger = require('./logger');

const dynamodb = new AWS.DynamoDB({
  apiVersion: '2012-08-10',
  endpoint: config.db.endpoint || null
});

const ses = new AWS.SES({
  apiVersion: '2010-12-01',
  region: config.region
});

const cognito = new AWS.CognitoIdentityServiceProvider({
  region: config.region
});

module.exports = {
  DynamoDB: function (credentials) {
    const params = config.db.endpoint ?
      // for tests
      {
        apiVersion: '2012-08-10',
        endpoint: config.db.endpoint,
        accessKeyId: config.db.accessKeyId || null,
        secretAccessKey: config.db.secretAccessKey || null,
        logger: {
          log: (...args) => logger.debug.apply(logger, args)
        }
      }: {
        apiVersion: '2012-08-10',
        region: config.region,
        credentials,
        logger: {
          log: (...args) => logger.debug.apply(logger, args)
        }
      };

    return new AWS.DynamoDB(params);
  },
  dynamodb,
  ses,
  cognito
}
