#!/usr/bin/env node
/**
 * https://redmine.eduway.today/issues/376
 * Fill content field for **Course** row into database
 * with structure
 *
 *  L: [
 *    { "S": "lessonId" },
 *    { "S": "lessonId" },
 *    { "S": "lessonId" },
 *    { "S": "lessonId" }
 *  ]
 */
const process = require('process');
const path = require('path');
const winston = require('winston');
const { format } = require('logform');
const _ = require('lodash');
process.env["NODE_CONFIG_DIR"] = path.resolve(__dirname, '../../config');

const config = require('config');
const { dynamodb } = require('../../src/aws');
const logger = winston.createLogger({
  format: format.cli(),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'output.log' })
  ]
});

logger.info(`Migrating ${config.db.tableName} database`);
logger.info(`0001-lessons-order`);

const Lesson = require('../../src/models/Lesson');

const queryParams = {
  TableName: config.db.tableName,
  IndexName: 'ByCourse',
  ExpressionAttributeNames: {
    "#courseId": "courseId",
    "#organizationId": "organizationId"
  },
  ProjectionExpression: "#courseId, #organizationId"
}

dynamodb.scan(queryParams)
.promise()
.then(async (courses) => {
  courses = _.uniqBy(courses.Items, 'courseId.S');

  await Promise.all(_.map(courses, updateCourse));
});

async function updateCourse (course) {
  const courseId = course.courseId.S;
  const organizationId = course.organizationId.S;
  logger.info(`Update course: ${courseId}`);
  const lessons = await Lesson.findAll({courseId});

  const items = _.chain(lessons.items).filter('id').map(lesson => ({
    "S": lesson.id
  })).value();

  logger.info(`Found ${items.length} lessons: ${JSON.stringify(items)}`);

  const params = {
    TableName: config.db.tableName,
    Key: {
      organizationId: { "S": organizationId },
      SK:             { "S": `Course#${courseId}` },
    },
    ExpressionAttributeValues: {
      ":content": { "L": items }
    },
    UpdateExpression: "SET content = :content"
  }

  try {
    const res = await dynamodb.updateItem(params).promise();
  } catch (e){
    console.log(e, params);
    throw e;
  }
  logger.info(`${courseId} was updated succesfully`);
}
