#!/usr/bin/env node
/**
 * Current script removes all created studdents with assigned courses
 * Use: ./delete-students.js file-with-emails.txt
 */
const process = require('process');
const fs = require('fs');
const path = require('path');

const winston = require('winston');
const { format } = require('logform');
const _ = require('lodash');
const env = process.env["NODE_ENV"];
if (env !== "development" && env) {
  throw new Error(`NODE_ENV can be only development, but its ${env}`);
}
process.env["NODE_CONFIG_DIR"] = path.resolve(__dirname, '../config');
const config = require('config');
config.logLevel = 'info';

const { dynamodb, cognito } = require('../src/aws');
const User = require('../src/models/User');
const Student = require('../src/models/User/Student');

const logger = winston.createLogger({
  format: format.cli(),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'output.log' })
  ]
});

logger.info(`Removing users from database`);

const args = process.argv.slice(2);
const input = args[0];
if (!input) {
  throw new Error("Provide input file");
}

const emails = fs.readFileSync(input).toString().split('\n');
const qs = [];

for (let email of emails) {
  qs.push(() => {
    return removeUser(email).catch(logger.error)
  });
}

seq(qs).catch(logger.error)

/*
const queryParams = {
  TableName: config.db.tableName,
  IndexName: 'ByCourse',
  ExpressionAttributeNames: {
    "#courseId": "courseId",
    "#organizationId": "organizationId"
  },
  ProjectionExpression: "#courseId, #organizationId"
}

dynamodb.scan(queryParams)
.promise()
.then(async (courses) => {
  courses = _.uniqBy(courses.Items, 'courseId.S');

  await Promise.all(_.map(courses, updateCourse));
});
*/

async function removeUser (email) {
  if (!email) return;
  logger.info(`Remove user ${email}`);
  const user = await User.config({dynamodb}).findByEmail(email);
  if (!user) {
    logger.info(`\tThere is no such user. Skipping...`);
    return;
  }
  logger.info(`\tFound user`);

  const queryParams = {
    TableName: config.db.tableName,
    IndexName: 'ByUser',
    ExpressionAttributeValues: {
      ":userId": { S: user.id }
    },
    KeyConditionExpression: "userId = :userId",
    ProjectionExpression: "organizationId, SK"
  }

  const res = await dynamodb.query(queryParams).promise();
  if (!res.Items) {
    logger.info(`\tSkipping, no items in ${config.db.tableName}`);
    return;
  }

  logger.info(`\tFound: ${JSON.stringify(res.Items)}`);
  //process.exit(0);
  if (res.Items.length) {
    const organizationId = res.Items[0].organizationId.S;

    const assignments = res.Items.filter((item) => {
      return item.SK.S.indexOf("CourseAssignment#") === 0;
    });

    console.log(assignments);

    const qs = [];
    for (let assignment of assignments) {
      qs.push(async () => deleteCourseAssignment(organizationId, assignment.SK));
    }
    await seq(qs).catch(e => { throw new Error(e); });

    const res1 = await Student.config({dynamodb}).delete({organizationId, id: user.id});
    if (res1.error) {
      throw new Error(res1.error.message);
    }
  }
  const userParams = {
    TableName: config.db.userTableName,
    Key: {
      id: { S: user.id }
    }
  }
  console.log(userParams)
  const res2 = await dynamodb.deleteItem(userParams).promise();
  if (res2.error) {
    throw new Error(res2.error.message);
  }

  const cognitoParams = {
    UserPoolId: config.cognito.pool,
    Username: user.id
  }
  const res3 = cognito.adminDeleteUser(cognitoParams).promise()
  if (res3.error) {
    throw new Error(res3.error.message);
  }

  async function deleteCourseAssignment (organizationId, SK) {
    const params = {
      TableName: config.db.tableName,
      Key: {
        organizationId: { S: organizationId },
        SK: SK
      }
    }

    console.log(`\tDelete assignment: `, params);
    const res = await dynamodb.deleteItem(params).promise();
    return res;
  }
}

async function seq(qs) {
  for (let q of qs) {
    await q();
  }
}

/*
async function updateCourse (course) {
  const courseId = course.courseId.S;
  const organizationId = course.organizationId.S;
  logger.info(`Update course: ${courseId}`);
  const lessons = await Lesson.findAll({courseId});

  const items = _.chain(lessons.items).filter('id').map(lesson => ({
    "S": lesson.id
  })).value();

  logger.info(`Found ${items.length} lessons: ${JSON.stringify(items)}`);

  const params = {
    TableName: config.db.tableName,
    Key: {
      organizationId: { "S": organizationId },
      SK:             { "S": `Course#${courseId}` },
    },
    ExpressionAttributeValues: {
      ":content": { "L": items }
    },
    UpdateExpression: "SET content = :content"
  }

  try {
    const res = await dynamodb.updateItem(params).promise();
  } catch (e){
    console.log(e, params);
    throw e;
  }
  logger.info(`${courseId} was updated succesfully`);
}
*/
