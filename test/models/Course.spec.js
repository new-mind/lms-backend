const { expect } = require('chai');
const AWS = require('aws-sdk-mock');

const f = '../../src/models/Course';

describe('Course', () => {
  describe('.listStudents()', () => {
    it('queries correct', async () => {
      let i = 0;
      const organizationId = "007";
      AWS.mock('DynamoDB', 'query', async (params) => {
        i++;
        if (i === 1) {
          expect(params.ExpressionAttributeValues[":SK"].S)
            .to.equal("CourseAssignment#1#Student#");

          return {
            Count: 2,
            Items: [
              {
                organizationId,
                userId: { "S": "11" }
              },
              {
                organizationId,
                userId: { "S": "12" },
              }
            ]
          };
        } else if (i === 2) {
          expect(params.ExpressionAttributeValues[":id"].S)
            .to.equal("11")
        } else if (i === 3) {
          expect(params.ExpressionAttributeValues[":id"].S)
            .to.equal("12")
        }
      });

      const Course = require(f);
      const course = new Course({
        id: "1",
        organizationId
      });

      await course.listStudents();
      AWS.restore('DynamoDB');
    });
  });
});
