const { expect } = require('chai');
const AWS = require('aws-sdk-mock');

const f = '../../src/models/Lesson';

describe('Lesson', () => {
  describe('.listStudents()', () => {

    it('queries correct', async () => {
      AWS.mock('DynamoDB', 'query', async (params) => {
        const values = params.ExpressionAttributeValues;
        expect(values[":SK"].S).to.equal("LessonAssignment#123#Student#");
        return {
          Count: 2,
          Items: [
            {
              userId: { "S": "1" }
            },
            {
              userId: { "S": "2" },
            }
          ]
        }
      });

      const Lesson = require(f);
      const lesson = new Lesson({
        id: "123",
        organizationId: "123"
      });
      const students = await lesson.listStudents();
      expect(students.length).to.equal(2);
      expect(students[0].id).to.equal("1");

      AWS.restore('DynamoDB');
    });
  });
})
