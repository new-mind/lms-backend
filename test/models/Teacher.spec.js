const { expect } = require('chai');
const AWS = require('aws-sdk-mock');

const Teacher = require('../../src/models/User/Teacher');

describe('Teacher', () => {
  describe('.create()', () => {
    before(() => {
      AWS.mock('DynamoDB', 'putItem', async (params) => params);
      AWS.mock('CognitoIdentityServiceProvider', 'adminCreateUser', async (params) => {});
    });
    after(() => {
      AWS.restore('DynamoDB');
      AWS.restore('CognitoIdentityServiceProvider');
    });

    it('works successfully', async () => {
      const teacher = await Teacher.create({
        email: "test@eduway.today"
      });
      expect(teacher.email).to.equal("test@eduway.today");
    });
  });

  describe('.invite()', () => {
    before(() => {
      AWS.mock('DynamoDB', 'putItem', async (params) => params);
      AWS.mock('DynamoDB', 'query', async (params) => {
        console.log('params', params);
      });
      AWS.mock('CognitoIdentityServiceProvider', 'adminCreateUser', async (params) => {});
    });
    after(() => {
      AWS.restore('DynamoDB');
      AWS.restore('CognitoIdentityServiceProvider');
    });

    it('works successfully, existed user', async () => {
      AWS.mock('DynamoDB', 'query', async (params) => {
        return {
          id: "007"
        }
      });
      const teacher = await Teacher.invite({
        email: "test@eduway.today",
        organizationId: "008"
      });
      AWS.restore('DynamoDB', 'query');

      expect(teacher.email).to.equal("test@eduway.today");
    });
    it('works successfully, not existed user', async () => {
      AWS.mock('DynamoDB', 'query', async (params) => {
        return null;
      });
      const teacher = await Teacher.invite({
        email: "test@eduway.today",
        organizationId: "008"
      });
      AWS.restore('DynamoDB', 'query');

      expect(teacher.email).to.equal("test@eduway.today");
    });
  });

  describe('.findAll()', () => {
    it('list in Organization', async () => {
      AWS.mock('DynamoDB', 'query', async (params) => {
        expect(params.ExpressionAttributeValues[':id'].S).to.equal("007");
        return {
          Items: [{id: "007"}, {id: "008"}]
        }
      });
      AWS.mock('DynamoDB', 'batchGetItem', async (params) => {
        return {
          Responses: {
            'eduway-users-dev': [ //TODO
              {id: "007"},
              {id: "008"}
            ]
          }
        }
      });
      const data = await Teacher.findAll({ organizationId: "007" });
      AWS.restore('DynamoDB');

      expect(data.items.length).to.equal(2);
    })
  });
});
