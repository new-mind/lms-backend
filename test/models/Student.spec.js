const { expect } = require('chai');
const _ = require('lodash');
const AWS = require('aws-sdk-mock');

const f = '../../src/models/User/Student';
const config = require('config');

describe('Student', () => {
  describe('.addLessons()', () => {
    it('adds 2 lessons', async () => {
      delete require.cache[require.resolve(f)];

      AWS.mock('DynamoDB', 'batchWriteItem', async (params) => {
        expect(params.RequestItems[config.db.tableName].length).to.equal(2);
      });

      const Student = require(f);
      await Student.addLessons("1", [
        {id: "2", courseId: "1", organizationId: "007"},
        {id: "3", courseId: "1", organizationId: "007"}
      ]);

      AWS.restore('DynamoDB');
    });

    it('adds 27 lessons', async () => {
      delete require.cache[require.resolve(f)];

      let i = 0;
      AWS.mock('DynamoDB', 'batchWriteItem', async (params) => {
        i++;
        if (i == 1) {
          expect(params.RequestItems[config.db.tableName].length).to.equal(25);
        } else {
          expect(params.RequestItems[config.db.tableName].length).to.equal(2);
        }
      });

      const Student = require(f);
      await Student.addLessons("1", _.range(27).map(i => ({id: `${i}`, courseId: "1", organizationId: "007"}) ));

      AWS.restore('DynamoDB');
    });

    it('adds 0 lessons', async () => {
      const Student = require(f);
      const res = await Student.addLessons("1", []);
      expect(res.length).to.equal(0);
    });

    it('adds 1 lesson with 2 duplicates', async () => {
      delete require.cache[require.resolve(f)];

      AWS.mock('DynamoDB', 'batchWriteItem', async (params) => {
        expect(params.RequestItems[config.db.tableName].length).to.equal(1);
      });

      const Student = require(f);
      await Student.addLessons("1", [
        {id: "2", courseId: "1", organizationId: "007"},
        {id: "2", courseId: "1", organizationId: "007"},
        {id: "2", courseId: "1", organizationId: "007"}
      ]);

      AWS.restore('DynamoDB');
    });
  });

  describe('.deleteLessons()', () => {
    delete require.cache[require.resolve(f)];

    it('deletes 2 lessons', async () => {
      AWS.mock('DynamoDB', 'batchWriteItem', async (params) => {
        expect(params.RequestItems[config.db.tableName].length).to.equal(2);
      });

      const Student = require(f);
      await Student.deleteLessons("1", "007", ["2", "3"]);

      AWS.restore('DynamoDB');
    });
  });
});
