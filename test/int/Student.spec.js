const gql = require('graphql-tag');
const { query, mutate } = require('./db');
const { expect } = require('chai');
const config = require('config');

const { organizationId } = require('./constant');
const { cognito, ses } = require('@lms/aws');
const fragments = require('@lms/schema/Fragments.graphql.js');
const { NO_ERRORS } = require('./utils');

const ADD_COURSES = gql`
  mutation AddCourses($id: ID!, $input: [ID!]) {
    Student(id: $id) {
      id
      addCourses(input: $input)
    }
  }
`;

const ADD_LESSON_COMMENT = gql`
  mutation AddLessonCommment($input: CreateStudentLessonCommentInput!) {
    addLessonComment(input: $input) {
      id
      content
      createdAt
      userId
      author {
        id
      }
    }
  }
`;

const ADD_LESSONS = gql`
  mutation AddLessons($id: ID!, $input: [ID!]) {
    Student(id: $id) {
      id
      addLessons(input: $input) {
        id
        title
        lessonId
      }
    }
  }
`;

const UNASSIGN_LESSONS = gql`
  mutation DeleteLessons($id: ID!, $input: [ID!]) {
    Student(id: $id) {
      id
      deleteLessons(input: $input)
    }
  }
`;

const GET_LESSON = gql`
  query GetLesson($id: ID!, $lessonId: ID!) {
    Student(id: $id) {
      getLesson(id: $lessonId) {
        id
        lessonId
        title
        rev
        content {
          ...TextWidget
          ...VideoWidget
          ...LinkWidget
          ...AttachmentWidget
          ...ImageWidget
          ...on ResponseFormWidget {
            id
            title
            formType
            answers(userId: $id) {
              ...on StudentAnswer_TextWidget {
                id
                rev
                updatedAt
                data
              }
              ...on StudentAnswer_LinkWidget {
                id
                rev
                updatedAt

                items {
                  id
                  title
                  url
                }
              }
              ...on StudentAnswer_AttachmentWidget {
                id
                rev
                updatedAt

                items {
                  id
                  title
                  fileKey
                  fileName
                }
              }
            }
          }
        }
      }
    }
  }
  ${fragments.TextWidget}
  ${fragments.VideoWidget}
  ${fragments.LinkWidget}
  ${fragments.AttachmentWidget}
  ${fragments.ImageWidget}
`;

const GET_LESSON_COMMENTS = gql`
  query GetLesson($userId: ID!, $lessonId: ID!, $commentsLimit: Int, $commentsNextToken: String) {
    Student(id: $userId) {
      getLesson(id: $lessonId) {
        comments(limit: $commentsLimit, nextToken: $commentsNextToken) {
          items {
            id
            content
            userId
          }
          nextToken
        }
      }
    }
  }
`;

const CREATE_LESSON = gql`
  mutation CreateLesson($input: CreateLessonInput!) {
    createLesson(input: $input) {
      id
      courseId
      organizationId
      title
      description
      homework
      dirty
      rev
    }
  }
`;

const INVITE_STUDENT = gql`
  mutation InviteStudent($input: InviteStudentInput) {
    inviteStudent(input: $input) {
      id
      email
      name
      surname
      phone
      age
    }
  }
`;

const INVITE_STUDENTS = gql`
  mutation InviteStudents($input: InviteStudentsInput) {
    inviteStudents(input: $input) {
      id
      email
      name
      surname
      phone
      age
    }
  }
`;

const PUBLISH_LESSON = gql`
  mutation PublishLesson($id: ID!) {
    publishLesson(id: $id) {
      id
      courseId
      organizationId
      title
      description
      homework
      dirty
      rev
    }
  }
`

const DELETE_LESSON = gql`
  mutation DeleteLesson($id: ID!) {
    deleteLesson(id: $id) {
      id
    }
  }
`

const LIST_HOMEWORKS_IN_ORG = gql`
  query ListHomeworks($id: ID!, $organizationId: ID) {
    Student(id: $id) {
      homeworksInOrg(organizationId: $organizationId) {
        id
        lessonId
        title
      }
    }
  }
`;

const LIST_HOMEWORKS = gql`
  query ListHomeworks($userId: ID!) {
    Student(id: $userId) {
      homeworks {
        id
        lessonId
        title
      }
    }
  }
`;

const SUBMIT_LESSON = gql`
  mutation SubmitLesson($id: ID!, $organizationId: ID!) {
    submitStudentLesson(id: $id, organizationId: $organizationId) {
      id
      status
    }
  }
`;

const CREATE_SECTION = gql`
  mutation AddSection($lessonId: ID!, $input: CreateSectionWithPosInput!) {
    Lesson(id: $lessonId) {
      addSection(input: $input) {
          section {
          ...ResponseFormWidget
        }
      }
    }
  }
  ${fragments.ResponseFormWidget}
`;

const UPDATE_RESPONSE = gql`
  mutation UpdateResponse($input: UpdateResponse) {
    updateResponse(input: $input) {
      id
      sectionId
      lessonId
      organizationId
      type
      rev

      ...on TextResponse {
        data
      }

      ...on LinkResponse {
        items {
          id
          url
          title
        }
      }

      ... on AttachmentResponse {
        items {
          id
          fileKey
          fileName
          title
        }
      }
    }
  }
`;

const UPDATE_STUDENT_ANSWER = gql`
  mutation UpdateStudentAnswer($input: UpdateStudentAnswer) {
    updateStudentAnswer(input: $input) {
      ...on StudentAnswer_TextWidget {
        id
        sectionId
        lessonId
        organizationId

        rev
        data
      }

      ...on StudentAnswer_LinkWidget {
        id
        sectionId
        lessonId
        organizationId

        rev
        items {
          id
          url
          title
        }
      }

      ...on StudentAnswer_AttachmentWidget {
        id
        sectionId
        lessonId
        organizationId

        rev
        items {
          id
          fileKey
          fileName
          title
        }
      }
    }
  }
`;

let responseFormId3_1;
let responseFormId3_2;
let responseFormId3_3;
let homeworkId;

describe('Student', () => {
  const INVITE_STUDENT_WITH_COURSE_IDS = gql`
    mutation InviteStudent($input: InviteStudentInput) {
      inviteStudent(input: $input) {
        id
        email
        name
        surname
        phone
        age
        courseIds
      }
    }
  `;

  let restore = (function () {
    let adminCreateUser = cognito.adminCreateUser;
    let sendEmail = ses.sendEmail;

    return function () {
      cognito.adminCreateUser = adminCreateUser;
      ses.sendEmail = sendEmail;
      cognito.adminSetUserPassword;
    }
  })();
  after(restore);

  it('invites existed user without courses', async () => {
    const input = {
      organizationId,
      email: "test@example.com"
    }

    const res = await mutate({query: INVITE_STUDENT, variables: {input}});
    expect(res.errors, res.errors).to.be.undefined;
  });

  it('invites existed user with courses', async () => {
    const input = {
      organizationId,
      email: "test@example.com",
      courses: [
        "01", "02"
      ]
    }
    const res = await mutate({query: INVITE_STUDENT_WITH_COURSE_IDS, variables: {input}});
    expect(res.errors, res.errors).to.be.undefined;
    const {data: {inviteStudent}} = res;
    //expect(inviteStudent.courseIds.length).to.equal(2);
  });

  it('invites 2 students witout courses', async () => {
    cognito.adminCreateUser = (params) => ({
      promise: async () => {}
    })
    ses.sendEmail = (params) => ({
      promise: async () => {
        expect(params.Source).to.equal(config.email.source);
      }
    });

    const input = {
      organizationId,
      emails: ["test100@example.com", "test200@example.com"],
    }

    const res = await mutate({
      query: INVITE_STUDENTS,
      variables: { input }
    });
    expect(res.errors, res.errors).to.be.undefined;
    expect(res.data.inviteStudents).to.be.lengthOf(2);
  });

  it('invites 2 students with courses', async () => {
    cognito.adminCreateUser = (params) => ({
      promise: async () => {}
    })
    ses.sendEmail = (params) => ({
      promise: async () => {
        expect(params.Source).to.equal(config.email.source);
      }
    });
    const input = {
      organizationId,
      emails: ["test101@example.com", "test201@example.com"],
      courses: [ "01", "02" ]
    }

    const res = await mutate({
      query: INVITE_STUDENTS,
      variables: { input }
    });
    expect(res.errors, res.errors).to.be.undefined;
    expect(res.data.inviteStudents).to.be.lengthOf(2);
  });

  describe('Lesson', () => {
    let lessonId;
    let restore = (function () {
      let adminCreateUser = cognito.adminCreateUser;
      let sendEmail = ses.sendEmail;

      return function () {
        cognito.adminCreateUser = adminCreateUser;
        ses.sendEmail = sendEmail;
      }
    })();
    before(async () => {
      ses.sendEmail = (params) => ({
        promise: async () => {
          expect(params.Source).to.equal(config.email.source);
        }
      });

      const res = await mutate({
        query: ADD_COURSES,
        variables: {
          id: "03",
          input: ["01", "02"]
        }
      });
      expect(res.errors, res.errors).to.be.undefined;

      const lesson1 = await mutate({
        query: CREATE_LESSON,
        variables: {
          input: {
            id: "010",
            courseId: "02",
            title: "Lesson created in before section"
          }
        }
      });
      expect(lesson1.errors, lesson1.errors).to.be.undefined;
      const lesson2 = await mutate({
        query: CREATE_LESSON,
        variables: {
          input: {
            id: "011",
            courseId: "03",
            title: "Lesson created in before section in not assigned course"
          }
        }
      });
      expect(lesson2.errors, lesson2.errors).to.be.undefined;
      const lesson3 = await mutate({
        query: CREATE_LESSON,
        variables: {
          input: {
            id: "012",
            courseId: "01",
            homework: true,
            title: "Homework created in before section"
          }
        }
      });
      expect(lesson3.errors, lesson3.errors).to.be.undefined;
      expect(lesson3.data.createLesson.homework).to.equal(true);
      const lesson4 = await mutate({
        query: CREATE_LESSON,
        variables: {
          input: {
            id: "012-01",
            courseId: "01-01",
            homework: true,
            title: "Homework created in before section"
          }
        }
      });
      expect(lesson4.errors, lesson4.errors).to.be.undefined;
      expect(lesson4.data.createLesson.homework).to.equal(true);

      const widget3_1 = await mutate({
        query: CREATE_SECTION,
        variables: {
          lessonId: "012",
          input: {
            section: {
              type: 'ResponseFormWidget',
              formType: 'TextWidget'
            }
          }
        }
      });
      expect(widget3_1.errors, widget3_1.errors).to.be.undefined;
      responseFormId3_1 = widget3_1.data.Lesson.addSection.section.id;

      const widget3_2 = await mutate({
        query: CREATE_SECTION,
        variables: {
          lessonId: "012",
          input: {
            section: {
              type: 'ResponseFormWidget',
              formType: 'LinkWidget'
            }
          }
        }
      });
      expect(widget3_2.errors, widget3_2.errors).to.be.undefined;
      responseFormId3_2 = widget3_2.data.Lesson.addSection.section.id;

      const widget3_3 = await mutate({
        query: CREATE_SECTION,
        variables: {
          lessonId: "012",
          input: {
            section: {
              type: 'ResponseFormWidget',
              formType: 'AttachmentWidget'
            }
          }
        }
      });
      expect(widget3_3.errors, widget3_3.errors).to.be.undefined;
      responseFormId3_3 = widget3_3.data.Lesson.addSection.section.id;

      const publish1 = await mutate({
        query: PUBLISH_LESSON,
        variables: {
          id: "010"
        }
      });
      expect(publish1.errors, publish1.errors).to.be.undefined;
      const publish2 = await mutate({
        query: PUBLISH_LESSON,
        variables: {
          id: "011"
        }
      });
      expect(publish2.errors, publish2.errors).to.be.undefined;
      const publish3 = await mutate({
        query: PUBLISH_LESSON,
        variables: {
          id: "012"
        }
      });
      expect(publish3.errors, publish3.errors).to.be.undefined;
      const publish4 = await mutate({
        query: PUBLISH_LESSON,
        variables: {
          id: "012-01"
        }
      });
      expect(publish4.errors, publish4.errors).to.be.undefined;
    });
    after(async () => {
      restore();
      const res = await mutate({
        query: DELETE_LESSON,
        variables: {
          id: "010"
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
      const res1 = await mutate({
        query: DELETE_LESSON,
        variables: {
          id: "011"
        }
      });
      expect(res1.errors, res1.errors).to.be.undefined;
      const res2 = await mutate({
        query: DELETE_LESSON,
        variables: {
          id: "012"
        }
      });
      expect(res2.errors, res2.errors).to.be.undefined;
    });

    it('adds one lesson', async () => {
      const res = await mutate({
        query: ADD_LESSONS,
        variables: {
          id: "03",
          input: ["01"]
        }
      });
      lessonId = res.data.Student.addLessons[0].id;
      NO_ERRORS(res);
      const res1 = await mutate({
        query: GET_LESSON,
        variables: {
          id: "03",
          lessonId
        }
      });
      NO_ERRORS(res1);
      const { getLesson } = res1.data.Student;
      expect(getLesson.rev).to.equal(0);
      expect(getLesson.lessonId).to.equal("01");
      expect(getLesson.content).to.be.lengthOf(3);
    });

    it('adds 2 lessons', async () => {
      const res = await mutate({
        query: ADD_LESSONS,
        variables: {
          id: "03",
          input: ["02", "03"]
        }
      });
      NO_ERRORS(res);
    });

    describe('Homework', () => {
      before(async () => {
        let res = await mutate({
          query: ADD_LESSONS,
          variables: {
            id: "03",
            input: ["012"]
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        homeworkId = res.data.Student.addLessons[0].id
        res = await mutate({
          query: ADD_LESSONS,
          variables: {
            id: "03",
            input: ["012-01"]
          }
        });
        expect(res.errors, res.errors).to.be.undefined;
      });

      it('list homeworks', async () => {
        const res = await mutate({
          query: LIST_HOMEWORKS_IN_ORG,
          variables: {
            id: "03",
            organizationId
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        expect(res.data.Student.homeworksInOrg).to.be.lengthOf(1);
        expect(res.data.Student.homeworksInOrg[0].lessonId).to.equal("012");
      });

      it('list homeworks in all organizations', async () => {
        const res = await mutate({
          query: LIST_HOMEWORKS,
          variables: { userId: "03" }
        })

        expect(res.errors, res.errors).to.be.undefined;
        expect(res.data.Student.homeworks).to.be.lengthOf(2);
      });

      it('fills ResponseForm with TextWidget', async () => {
        const data = "My response";
        const res = await mutate({
          query: UPDATE_STUDENT_ANSWER,
          variables: {
            input: {
              organizationId,
              lessonId: "012",
              userId: "03",
              rev: 0,
              sectionId: responseFormId3_1,
              type: 'TextWidget',
              data
            }
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        expect(res.data.updateStudentAnswer.data).to.equal(data);
      });

      it('fills ResponseForm with LinkWidget', async () => {
        const url = "https://ya.ru";
        const title = "Yandex";
        const res = await mutate({
          query: UPDATE_STUDENT_ANSWER,
          variables: {
            input: {
              organizationId,
              lessonId: "012",
              userId: "03",
              rev: 0,
              sectionId: responseFormId3_2,
              type: 'LinkWidget',
              items: [{ id: "01", url, title }]
            }
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        expect(res.data.updateStudentAnswer.items).to.lengthOf(1);
        expect(res.data.updateStudentAnswer.items[0].url).to.equal(url);
        expect(res.data.updateStudentAnswer.rev).to.equal(0);
        expect(res.data.updateStudentAnswer.items[0].title).to.equal(title);
      });

      it('fills ResponseForm with AttachmentWidget', async () => {
        let fileKey = "fileKey";
        let fileName = "fileName";
        let title = "file title";
        const res = await mutate({
          query: UPDATE_STUDENT_ANSWER,
          variables: {
            input: {
              organizationId,
              lessonId: "012",
              userId: "03",
              rev: 1,
              sectionId: responseFormId3_3,
              type: 'AttachmentWidget',
              items: [{ id: "01", fileKey, fileName, title }]
            }
          }
        });

        NO_ERRORS(res);
        expect(res.data.updateStudentAnswer.items).to.lengthOf(1);
        expect(res.data.updateStudentAnswer.rev).to.equal(1);
        expect(res.data.updateStudentAnswer.items[0].fileKey).to.equal(fileKey);
        expect(res.data.updateStudentAnswer.items[0].fileName).to.equal(fileName);
        expect(res.data.updateStudentAnswer.items[0].title).to.equal(title);
      });

      it('updates ReponseForm with TextWidget', async () => {
        let data = "My 2nd response";
        let res = await mutate({
          query: UPDATE_STUDENT_ANSWER,
          variables: {
            input: {
              organizationId,
              lessonId: "012",
              userId: "03",
              rev: 1,
              sectionId: responseFormId3_1,
              type: 'TextWidget',
              data
            }
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        expect(res.data.updateStudentAnswer.data).to.equal(data);
        data = "My 3rd response";
        res = await mutate({
          query: UPDATE_STUDENT_ANSWER,
          variables: {
            input: {
              organizationId,
              lessonId: "012",
              userId: "03",
              rev: 1,
              sectionId: responseFormId3_1,
              type: 'TextWidget',
              data
            }
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        expect(res.data.updateStudentAnswer.data).to.equal(data);
      });

      it('updates ReponseForm with LinkWidget', async () => {
        let url = "http://test.link";
        let title = "title1";
        let res = await mutate({
          query: UPDATE_STUDENT_ANSWER,
          variables: {
            input: {
              organizationId,
              lessonId: "012",
              userId: "03",
              rev: 0,
              sectionId: responseFormId3_2,
              type: 'LinkWidget',

              items: [{ id: "01", url, title }]
            }
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        let { updateStudentAnswer } = res.data;
        expect(updateStudentAnswer.items[0].url).to.equal(url);
        expect(updateStudentAnswer.items[0].title).to.equal(title);

        url = "http://test_____test.link";
        title = null;
        res = await mutate({
          query: UPDATE_STUDENT_ANSWER,
          variables: {
            input: {
              organizationId,
              lessonId: "012",
              userId: "03",
              rev: 0,
              sectionId: responseFormId3_2,
              type: 'LinkWidget',

              items: [{ id: "01", url, title }]
            }
          }
        });
        expect(res.errors, res.errors).to.be.undefined;
        updateStudentAnswer = res.data.updateStudentAnswer;
        expect(updateStudentAnswer.items[0].url).to.equal(url);
        expect(updateStudentAnswer.items[0].title).to.be.null;
      });

      it('updates ReponseForm with AttachmentWidget', async () => {
        let fileKey = "fileKey";
        let fileName = "fileName";
        let title = "file title";
        let res = await mutate({
          query: UPDATE_STUDENT_ANSWER,
          variables: {
            input: {
              organizationId,
              lessonId: "012",
              userId: "03",
              rev: 0,
              sectionId: responseFormId3_3,
              type: 'AttachmentWidget',
              items: [ { id: "01", fileKey, fileName, title } ]
            }
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        let { updateStudentAnswer } = res.data;
        expect(updateStudentAnswer.items[0].fileKey).to.equal(fileKey);
        expect(updateStudentAnswer.items[0].fileName).to.equal(fileName);
        expect(updateStudentAnswer.items[0].title).to.equal(title);

        fileKey = "fileKey1";
        fileName = "fileName1";
        title = "file title >>>>>>>>>>";
        res = await mutate({
          query: UPDATE_STUDENT_ANSWER,
          variables: {
            input: {
              organizationId,
              lessonId: "012",
              userId: "03",
              rev: 0,
              sectionId: responseFormId3_3,
              type: 'AttachmentWidget',
              items: [ { id: "01", fileKey, fileName, title } ]
            }
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        updateStudentAnswer = res.data.updateStudentAnswer;
        expect(updateStudentAnswer.items[0].fileKey).to.equal(fileKey);
        expect(updateStudentAnswer.items[0].fileName).to.equal(fileName);
        expect(updateStudentAnswer.items[0].title).to.equal(title);
      });

      it('returns correct data', async () => {
        const res = await mutate({
          query: GET_LESSON,
          variables: {
            id: "03",
            lessonId: homeworkId
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        const { getLesson } = res.data.Student;
        expect(getLesson.content).to.lengthOf(3);
        expect(getLesson.content[2].answers).to.lengthOf(2);
        expect(getLesson.content[2].answers[0].data).to.equal("My 3rd response");
      });

      it('submits homework', async () => {
        const res = await mutate({
          query: SUBMIT_LESSON,
          variables: {
            id: homeworkId,
            organizationId
          }
        });
        expect(res.errors, res.errors).to.be.undefined;
      });
    });

    describe('Comments', () => {
      const comment1 = "My first comment";
      const comment2 = "My second comment";
      it('comments the lesson', async () => {
        let res = await mutate({
          query: ADD_LESSON_COMMENT,
          variables: {
            input: {
              id: "01",
              userId: "03",
              organizationId,
              lessonId,
              content: comment1
            }
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        expect(res.data.addLessonComment.content).to.equal(comment1);

        res = await mutate({
          query: ADD_LESSON_COMMENT,
          variables: {
            input: {
              id: "02",
              userId: "03",
              organizationId,
              lessonId,
              content: comment2
            }
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        expect(res.data.addLessonComment.content).to.equal(comment2);
      });

      it('findsAll', async () => {
        const res = await mutate({
          query: GET_LESSON_COMMENTS,
          variables: {
            userId: "03",
            lessonId
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        const { items } = res.data.Student.getLesson.comments;
        expect(items).to.be.lengthOf(2);
        expect(items[0].userId).to.equal("03");
      })

      it('pagination', async () => {
        let res = await mutate({
          query: GET_LESSON_COMMENTS,
          variables: {
            userId: "03",
            lessonId,
            commentsLimit: 1
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        const { items, nextToken } = res.data.Student.getLesson.comments;
        expect(items).to.be.lengthOf(1);
        expect(nextToken).to.be.an('string');
        expect(items[0].content).to.equal(comment1);

        res = await mutate({
          query: GET_LESSON_COMMENTS,
          variables: {
            userId: "03",
            lessonId,
            commentsLimit: 1,
            commentsNextToken: nextToken
          }
        });

        expect(res.errors, res.errors).to.be.undefined;
        const comments = res.data.Student.getLesson.comments;
        expect(comments.items).to.be.lengthOf(1);
        expect(comments.items[0].content).to.equal(comment2);
      });
    });
    /*
    it('adds/remove/adds lesson', async () => {
      const res1 = await mutate({
        query: ADD_LESSONS,
        variables: {
          id: "03",
          input: ["01"]
        }
      });
      const res2 = await mutate({
        query: DELETE_LESSONS,
        variables: {
          id: "03",
          input: ["01"]
        }
      });
      const res3 = await mutate({
        query: ADD_LESSONS,
        variables: {
          id: "03",
          input: ["01"]
        }
      });
    });
    */
  });
});

module.exports = {
  getVariables: () => {
    return {
      homeworkId: homeworkId,
      responseFormId3_1,
      responseFormId3_2,
      responseFormId3_3
    }
  }
}
