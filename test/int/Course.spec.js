const gql = require('graphql-tag');
const { query, mutate } = require('./db');
const { expect } = require('chai');
const config = require('config');

const CREATE_LESSON = gql`
  mutation CreateLesson($input: CreateLessonInput!) {
    createLesson(input: $input) {
      id
      courseId
      organizationId
      title
      description
      homework
      dirty
      rev
    }
  }
`;

const DELETE_LESSON = gql`
  mutation DeleteLesson($id: ID!) {
    deleteLesson(id: $id) {
      id
    }
  }
`;

const MOVE_LESSON = gql`
  mutation MoveLesson($courseId: ID!, $id: ID!, $position: Int!) {
    Course(id: $courseId) {
      moveLesson(id: $id, position: $position) {
        position
        lesson {
          id
        }
      }
    }
  }
`

const GET_COURSE = gql`
  query GetCourse($id: ID!) {
    getCourse(id: $id) {
      id
      title
      description
      imageUrl
    }
  }
`;

const GET_COURSE_WITH_LESSONS = gql`
  query GetCourse($id: ID!) {
    getCourse(id: $id) {
      id
      title
      description
      imageUrl
      lessons {
        items {
          id
          title
        }
        nextToken
      }
    }
  }
`;

const { organizationId } = require('./constant');

describe('Course', () => {

  it('returns existed one', async () => {
    const res = await query({query: GET_COURSE, variables: {id: "01"}});

    expect(res.errors, res.errors).to.be.undefined;
    const {data: {getCourse}} = res;
    expect(getCourse).to.have.property('id', "01");
    expect(getCourse).to.have.property('title', "MyCourse");
    expect(getCourse).to.have.property('description', "Description");
  });

  it('doesn\'t return not existed', async () => {
    const promise = query({query: GET_COURSE, variables: {id: "08"}});

    expect(promise).to.throw;
  });

  it('list all lessons', async () => {
    const res = await query({
      query: GET_COURSE_WITH_LESSONS,
      variables: { id: "01" }
    });

    expect(res.errors, res.errors).to.be.undefined;
    expect(res.data.getCourse.lessons.items).to.be.lengthOf(4);
    expect(res.data.getCourse.lessons.items[3].id).to.equal('00');
  });

  describe('Lessons order', () => {
    let lesson1Id;
    let lesson2Id;
    let lesson3Id;
    before(async () => {
      lesson1Id = await createLesson("Lesson #1");
      lesson2Id = await createLesson("Lesson #2");
      lesson3Id = await createLesson("Lesson #3");

      async function createLesson(title) {
        const res = await mutate({
          query: CREATE_LESSON,
          variables: {
            input: {
              courseId: "02",
              title
            }
          }
        });
        expect(res.errors, res.errors).to.be.undefined;
        return res.data.createLesson.id;
      }
    });

    after(async () => {
      return Promise.all([
        deleteLesson(lesson1Id),
        deleteLesson(lesson2Id),
        deleteLesson(lesson3Id)
      ])

      async function deleteLesson (id) {
        const res = await mutate({
          query: DELETE_LESSON,
          variables: { id }
        });
        expect(res.errors, res.errors).to.be.undefined;
      }
    });

    it('move 0 to 1', async () => {
      const res = await mutate({
        query: MOVE_LESSON,
        variables: {
          courseId: "02",
          id: lesson1Id,
          position: 1
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
      expect(res.data.Course.moveLesson.position).to.equal(1);

      const res1 = await mutate({
        query: GET_COURSE_WITH_LESSONS,
        variables: { id: "02" }
      });
      expect(res1.errors, res1.errors).to.be.undefined;
      expect(res1.data.getCourse.lessons.items[1].id).to.equal(lesson1Id);
      expect(res1.data.getCourse.lessons.items[0].id).to.equal(lesson2Id);
    });

    it('move 1 to 10', async () => {
      const res = await mutate({
        query: MOVE_LESSON,
        variables: {
          courseId: "02",
          id: lesson1Id,
          position: 10
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
      expect(res.data.Course.moveLesson.position).to.equal(3);

      const res1 = await mutate({
        query: GET_COURSE_WITH_LESSONS,
        variables: { id: "02" }
      });
      expect(res1.errors, res1.errors).to.be.undefined;
      expect(res1.data.getCourse.lessons.items[1].id).to.equal(lesson3Id);
      expect(res1.data.getCourse.lessons.items[2].id).to.equal(lesson1Id);
    });

    it('move 2 to 0', async () => {
      const res = await mutate({
        query: MOVE_LESSON,
        variables: {
          courseId: "02",
          id: lesson1Id,
          position: 0
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
      expect(res.data.Course.moveLesson.position).to.equal(0);

      const res1 = await mutate({
        query: GET_COURSE_WITH_LESSONS,
        variables: { id: "02" }
      });
      expect(res1.errors, res1.errors).to.be.undefined;
      expect(res1.data.getCourse.lessons.items[0].id).to.equal(lesson1Id);
      expect(res1.data.getCourse.lessons.items[1].id).to.equal(lesson2Id);
      expect(res1.data.getCourse.lessons.items[2].id).to.equal(lesson3Id);
    });

    it('deletes lesson correctly', async () => {
      const res = await mutate({
        query: DELETE_LESSON,
        variables: { id: lesson1Id }
      })
      expect(res.errors, res.errors).to.be.undefined;

      const { DynamoDB } = require('@lms/aws');
      const dynamodb = DynamoDB();
      const params = {
        TableName: config.db.tableName,
        Key: {
          organizationId: { "S": organizationId },
          SK: { "S": `Course#02` }
        }
      }
      const res1 = await dynamodb.getItem(params).promise();
      expect(res1.Item.content.L).to.be.lengthOf(2);
    });
  });
});
