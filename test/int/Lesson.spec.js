const gql = require('graphql-tag');
const { query, mutate } = require('./db');
const { expect } = require('chai');

const GET_LESSON = gql`
  query GetLesson($id: ID!) {
    getLesson(id: $id) {
      id
      title
      description
      homework
      dirty
      rev
    }
  }
`;

const CREATE_LESSON = gql`
  mutation CreateLesson($input: CreateLessonInput!) {
    createLesson(input: $input) {
      id
      courseId
      organizationId
      title
      description
      homework
      dirty
      rev
    }
  }
`;

const DELETE_LESSON = gql`
  mutation DeleteLesson($id: ID!) {
    deleteLesson(id: $id) {
      id
    }
  }
`;

const PUBLISH_LESSON = gql`
  mutation PublishLesson($id: ID!) {
    publishLesson(id: $id) {
      id
      courseId
      organizationId
      title
      description
      homework
      dirty
      rev
    }
  }
`

describe('Lesson', () => {
  it('returns existed one', async () => {
    const res = await query({query: GET_LESSON, variables: {id: "01"}});

    expect(res.errors, res.errors).to.be.undefined;
    expect(res.data.getLesson).to.have.property('id', "01");
  });

  it('throws an error on not existed one', async () => {
    const promise = query({query: GET_LESSON, variables: {id: "010"}});
    expect(promise).to.throw;
  });

  let lessonId;
  it('creates', async () => {
    const input = {
      courseId: "01",
      title: "Test Lesson"
    }
    const res = await mutate({query: CREATE_LESSON, variables: {input}});
    expect(res.errors, res.errors).to.be.undefined;
    const {data: {createLesson}} = res;
    expect(createLesson.courseId).to.equal("01");
    lessonId = createLesson.id;
  })

  it('publish', async () => {
    const res = await mutate({
      query: CREATE_LESSON,
      variables: {
        input: {
          courseId: "01",
          title: "Test Lesson"
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {data: {createLesson}} = res;

    const res1 = await mutate({
      query: PUBLISH_LESSON,
      variables: {
        id: createLesson.id
      }
    });
    expect(res1.errors, res1.errors).to.be.undefined;
    expect(res1.data.publishLesson.dirty).to.be.false;
    expect(res1.data.publishLesson.rev).to.equal(0);
  });

  it('deletes', async () => {
    const res = await mutate({
      query: DELETE_LESSON,
      variables: {
        id: lessonId
      }
    });
    expect(res.errors, res.errors).to.be.undefined;

    const res1 = await query({query: GET_LESSON, variables: {id: lessonId}});
    expect(res1.data.getLesson).to.be.null;
  });

  describe('Section', () => {
    let lessonId;
    const ADD_SECTION = gql`
      mutation AddSection($id: ID!, $input: CreateSectionWithPosInput) {
        Lesson(id: $id) {
          addSection(input: $input) {
            section {
              ... on TextWidget {
                id
                data
              }
            }
            position
          }
        }
      }
    `;

    const DELETE_SECTION = gql`
      mutation DeleteSection($id: ID!, $sectionId: ID!) {
        Lesson(id: $id) {
          deleteSection(id: $sectionId) {
            section {
              ... on TextWidget {
                id
              }
            }
            position
          }
        }
      }
    `;

    const MOVE_SECTION = gql`
      mutation MoveSection($lessonId: ID! $sectionId: ID!, $position: Int!) {
        Lesson(id: $lessonId) {
          moveSection(id: $sectionId, position: $position) {
            section {
              ... on TextWidget {
                id
              }
            }
            position
          }
        }
      }
    `;

    const GET_LESSON_WITH_CONTENT = gql`
      query GetLesson($id: ID!) {
        getLesson(id: $id) {
          id
          title
          description
          homework
          content {
            ... on TextWidget {
              id
              data
            }
          }
        }
      }
    `;

    before(async () => {
      const input = {
        courseId: "01",
        title: "Lesson with content"
      }
      const res = await mutate({query: CREATE_LESSON, variables: {input}});
      const {data: {createLesson}} = res;
      lessonId = createLesson.id;
    });

    it('creates TextWidget section', async () => {
      const input = {
        position: 0,
        section: {
          id: "01",
          type: "TextWidget"
        }
      }
      const res = await mutate({query: ADD_SECTION, variables: {id: lessonId, input}});
      expect(res.errors, res.errors).to.be.undefined;
      const res1 = await query({query: GET_LESSON_WITH_CONTENT, variables: {id: lessonId}});
      expect(res1.errors, res1.errors).to.be.undefined;
    });

    it('creates and deletes', async () => {
      const lessonId = "02";
      const input = {
        position: 0,
        section: {
          id: "02",
          type: "TextWidget"
        }
      }
      const res = await mutate({query: ADD_SECTION, variables: {id: lessonId, input}});
      expect(res.errors, res.errors).to.be.undefined;
      const res1 = await query({query: GET_LESSON_WITH_CONTENT, variables: {id: lessonId}});
      expect(res1.errors, res1.errors).to.be.undefined;
      expect(res1.data.getLesson.content).to.be.lengthOf(1);
      const res2 = await mutate({query: DELETE_SECTION, variables: {id: lessonId, sectionId: input.section.id}});
      expect(res1.errors, res1.errors).to.be.undefined;
      const res3 = await query({query: GET_LESSON_WITH_CONTENT, variables: {id: lessonId}});
      expect(res3.errors, res3.errors).to.be.undefined;
      expect(res3.data.getLesson.content).to.be.lengthOf(0);
    });

    describe('.moveSection()', () => {
      let sectionId;
      function createWidget(id) {
        return {
          position: 0,
          section: {
            id,
            type: "TextWidget"
          }
        }
      };

      before(async () => {
        let res = await mutate({
          query: ADD_SECTION,
          variables: {id: lessonId, input: createWidget("02") }
        });
        const section = res.data.Lesson.addSection.section;
        sectionId = section.id;
        expect(res.errors, res.errors).to.be.undefined;
        res = await mutate({
          query: ADD_SECTION,
          variables: {id: lessonId, input: createWidget("03") }
        });
        expect(res.errors, res.errors).to.be.undefined;
        res = await mutate({
          query: ADD_SECTION,
          variables: {id: lessonId, input: createWidget("04") }
        });
        expect(res.errors, res.errors).to.be.undefined;
      });

      it('Move section to 0 position', async () => {
        const res = await mutate({
          query: MOVE_SECTION,
          variables: {
            lessonId,
            sectionId: sectionId,
            position: 0
          }
        });
        expect(res.errors, res.errors).to.be.undefined;
        expect(res.data.Lesson.moveSection.position).to.equal(0);
      });
    });

    /*
    it('creates LinkWidget section', async () => {
      const input = {
        position: 1,
        section: {
          id: "01",
          widget: "Text"
        }
      }
      const res = await mutate({query: ADD_SECTION, variables: {id: lessonId, input}});
      expect(res.errors, res.errors).to.be.undefined;
    });
*/
  });
});
