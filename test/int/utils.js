const { expect } = require('chai');
const _ = require('lodash');

module.exports = {
  NO_ERRORS: (res) => {
    const errors = res.errors;
    expect(errors, _.map(errors, e => e.message).join('\n'))
      .to.be.undefined;
  }
}
