const gql = require('graphql-tag');
const { query, mutate } = require('./db');
const { expect } = require('chai');

const { organizationId } = require('./constant');
const { getVariables } = require('./Student.spec.js');

const OPEN_LESSON = gql`
  mutation OpenStudentLesson($id: ID!, $organizationId: ID!) {
    reopenStudentLesson(id: $id, organizationId: $organizationId) {
      id
      lessonId
      rev
      status
    }
  }
`;

describe('Teacher', () => {
  const INVITE_TEACHER = gql`
    mutation InviteTeacher($input: InviteTeacherInput) {
      inviteTeacher(input: $input) {
        id
        email
        name
        surname
        phone
        age
      }
    }
  `;
  const DELETE_TEACHER = gql`
    mutation DeleteTeacher($input: DeleteTeacherInput) {
      deleteTeacher(input: $input) {
        id
        email
        name
        surname
        phone
        age
      }
    }
  `;

  it('invites exited teacher', async () => {
    const input = {
      organizationId,
      email: "petr1@example.com"
    };
    const res = await mutate({query: INVITE_TEACHER, variables: {input}});
    expect(res.errors, res.errors).to.be.undefined;
  });

  it('removes invited teacher', async () => {
    const input = {
      organizationId,
      id: "02"
    };
    const res = await mutate({query: DELETE_TEACHER, variables: {input}});
    expect(res.errors, res.errors).to.be.undefined;
    expect(res.data.deleteTeacher.id).to.equal("02");
  });

  describe('Homework', () => {
    it('reopens homework', async () => {
      const homeworkId = getVariables().homeworkId;
      const res = await mutate({
        query: OPEN_LESSON,
        variables: {
          id: homeworkId,
          organizationId
        }
      });

      expect(res.errors, res.errors).to.be.undefined;
      expect(res.data.reopenStudentLesson.status).to.equal("REOPEN");
      expect(res.data.reopenStudentLesson.rev).to.equal(1);
    });
  });
});
