const gql = require('graphql-tag');
const { mutate } = require('./db');
const { expect } = require('chai');
const config = require('config');
const _ = require('lodash');

const { cognito, ses } = require('@lms/aws');
const { organizationId } = require('./constant');

const INVITE_ADMIN = gql`
  mutation InviteAdmin($email: String!, $organizationId: ID!) {
    inviteAdmin(email: $email, organizationId: $organizationId) {
      id
      email
      name
    }
  }
`

const REINVITE_ADMIN = gql`
  mutation ReinviteAdmin($email: String!, $organizationId: ID!) {
    reinviteAdmin(email: $email, organizationId: $organizationId) {
      id
      email
      name
    }
  }
`

describe('Admin', () => {
  let userId;
  let restore = (function () {
    let adminCreateUser = cognito.adminCreateUser;
    let adminSetUserPassword = cognito.adminSetUserPassword;
    let sendEmail = ses.sendEmail;

    return function () {
      cognito.adminCreateUser = adminCreateUser;
      ses.sendEmail = sendEmail;
      cognito.adminSetUserPassword;
    }
  })();

  it('invites non existed admin', async () => {
    const email = "jiojiajiu@gmail.com";
    cognito.adminCreateUser = (params) => ({
      promise: async () => {
        let e = _.find(params.UserAttributes, item => (item.Name === "email"));
        expect(e.Value).to.equal(email);
        userId = params.Username;
      }
    })
    ses.sendEmail = (params) => ({
      promise: async () => {
        expect(params.Source).to.equal(config.email.source);
      }
    });

    const res = await mutate({
      query: INVITE_ADMIN,
      variables: {
        email,
        organizationId
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    expect(res.data.inviteAdmin.id).to.equal(userId);
  });

  it('invtites existed admin', async () => {
    const email = "jiojiajiu@gmail.com";
    ses.sendEmail = (params) => ({
      promise: async () => {
        const SES = require('@lms/services/email');
        expect(params.Message.Subject.Data).to.equal(SES.templates.en['admin-inviteRegistered'].subject);
        expect(params.Source).to.equal(config.email.source);
      }
    });
    const res = await mutate({
      query: INVITE_ADMIN,
      variables: {
        email,
        organizationId
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    expect(res.data.inviteAdmin.id).to.equal(userId);
  });

  it('reinvites existed admin', async () => {
    const email = "jiojiajiu@gmail.com";
    ses.sendEmail = (params) => ({
      promise: async () => {
        const SES = require('@lms/services/email');
        expect(params.Message.Subject.Data).to.equal(SES.templates.en['admin-invite'].subject);
        expect(params.Source).to.equal(config.email.source);
      }
    });
    cognito.adminSetUserPassword = (params) => ({
      promise: async () => {
        expect(params.Permanent).to.equal(false);
      }
    });
    const res = await mutate({
      query: REINVITE_ADMIN,
      variables: {
        email,
        organizationId
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    expect(res.data.reinviteAdmin.id).to.equal(userId);
  });
  after(restore);
});
