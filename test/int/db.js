const { createTestClient } = require('apollo-server-testing');
const { ApolloServer } = require('apollo-server');
const config = require('config');

const typeDefs = require('@lms/typeDefs');
const resolvers = require('@lms/resolvers');
const logger = require('@lms/logger');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  formatError: (error) => {
    config.showErrors && logger.error(error.originalError);
    return error;
  },
});

const client = createTestClient(server)
module.exports=client;
