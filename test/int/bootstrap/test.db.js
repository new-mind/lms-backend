const config = require('config');

module.exports = {
  AttributeDefinitions: [
    {
      AttributeName: "organizationId",
      AttributeType: "S"
    },
    {
      AttributeName: "SK",
      AttributeType: "S"
    },
    {
      AttributeName: "courseId",
      AttributeType: "S"
    },
    {
      AttributeName: "lessonId",
      AttributeType: "S"
    },
    {
      AttributeName: "userId",
      AttributeType: "S"
    }/*,
    {
      AttributeName: "type",
      AttributeType: "S"
    },
    */
  ],
  KeySchema: [
    {
      AttributeName: "organizationId",
      KeyType: "HASH"
    },
    {
      AttributeName: "SK",
      KeyType: "RANGE"
    },
  ],
  GlobalSecondaryIndexes: [
    {
      IndexName: "ByCourse",
      KeySchema: [
        {
          AttributeName: "courseId",
          KeyType: "HASH"
        },
        {
          AttributeName: "SK",
          KeyType: "RANGE"
        },
      ],
      Projection: {
        ProjectionType: "ALL"
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: "5",
        WriteCapacityUnits: "5"
      },
    },
    {
      IndexName: "ByLesson",
      KeySchema: [
        {
          AttributeName: "lessonId",
          KeyType: "HASH"
        },
        {
          AttributeName: "SK",
          KeyType: "RANGE"
        },
      ],
      Projection: {
        ProjectionType: "ALL"
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: "5",
        WriteCapacityUnits: "5"
      },
    },
    {
      IndexName: "ByUser",
      KeySchema: [
        {
          AttributeName: "userId",
          KeyType: "HASH"
        },
        {
          AttributeName: "SK",
          KeyType: "RANGE"
        },
      ],
      Projection: {
        ProjectionType: "ALL"
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: "5",
        WriteCapacityUnits: "5"
      },
    },
    {
      IndexName: "BySK",
      KeySchema: [
        {
          AttributeName: "SK",
          KeyType: "HASH"
        }
      ],
      Projection: {
        ProjectionType: "ALL"
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: "5",
        WriteCapacityUnits: "5"
      },
    }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: "5",
    WriteCapacityUnits: "5"
  },
  TableName: config.db.tableName
}
