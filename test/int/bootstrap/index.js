const AWS = require('aws-sdk');
const AWSMock = require('aws-sdk-mock');
const config = require('config');
const _ = require('lodash');
const moment = require('moment');

const constant = require('../constant');

AWSMock.mock('SES', 'sendTemplatedEmail', () => {});
AWSMock.mock('SES', 'sendEmail', () => {});

before(bootstrap);

const dynamodb = new AWS.DynamoDB({
  apiVersion: '2012-08-10',
  endpoint: config.db.endpoint || null,
  accessKeyId: config.db.accessKeyId,
  secretAccessKey: config.db.secretAccessKey
});

async function bootstrap () {
  this.timeout(5000);
  let params, cmd;
  console.log('Bootstraping...');

  await Promise.all([
    deleteTable(config.db.tableName),
    deleteTable(config.db.userTableName)
  ]);

  await Promise.all([
    createTable(config.db.tableName),
    createTable(config.db.userTableName)
  ]);

  // users
  await createBulk([
    {
      id:    {"S": "01"},
      email: {"S": "test@example.com"}
    },
    {
      id:      {"S": "02"},
      name:    {"S": "Petr"},
      surname: {"S": "I"},
      email:   {"S": "petr1@example.com" }
    },
    {
      id:      {"S": "03"},
      name:    {"S": "Lord"},
      surname: {"S": "Webner"},
      email:   {"S": "lord@eduway.today" }
    },
    {
      id:      {"S": "04"},
      name:    {"S": "Alice"},
      surname: {"S": "InWonderland"},
      email:   {"S": "alice_in_worderland@eduway.today" }
    },
    {
      id:      {"S": "05"},
      name:    {"S": "Arnold"},
      surname: {"S": "Shwarz"},
      email:   {"S": "arnold1@eduway.today" }
    }
  ], config.db.userTableName);
  console.log('    created users');

  await fillOrganization({S: constant.organizationId}, "");
  await fillOrganization({S: constant.organizationId2}, "-01");

  console.log('DONE\n');
}

async function fillOrganization(organizationId, postfix) {
  console.log(`  Fill Organization ${organizationId.S}`);
  // courses
  await createBulk([
    {
      organizationId,
      courseId:    {"S": `01${postfix}`},
      SK:          {"S": `Course#01${postfix}`},
      title:       {"S": "MyCourse"},
      description: {"S": "Description"},
      content:     {"L": [
        {"S": "01"},
        {"S": "02"},
        {"S": "03"},
        {"S": "00"}
      ]}
    },
    {
      organizationId,
      courseId: {"S": `02${postfix}`},
      SK: {"S": `Course#02${postfix}`},
      title: {"S": "Course2"}
    },
    {
      organizationId,
      courseId: {"S": `03${postfix}`},
      SK: {"S": `Course#03${postfix}`},
      title: {"S": "Course3"}
    }
  ])
  console.log(`    created courses in Org: ${organizationId.S}`);

  // sections
  await createBulk([
    {
      organizationId,
      lessonId:    {"S": `01${postfix}`},
      sectionId:   {"S": "01"},
      SK:          {"S": `Lesson#01${postfix}#Section#01`},
      type:        {"S": "TextWidget"},
      data:        {"S": "Yaz"}
    },
    {
      organizationId,
      lessonId:    {"S": `01${postfix}`},
      sectionId:   {"S": "02"},
      SK:          {"S": `Lesson#01${postfix}#Section#02`},
      title:       {"S": "Links"},

      type:        {"S": "LinkWidget"},
      items:       {"L": [
        {
          "M": {
            "id":    {"S":  "01"},
            "title": {"S": "Link first"},
            "url":   {"S": "https://eduway.today"}
          }
        }
      ]}
    },
    {
      organizationId,
      lessonId:    {"S": `01${postfix}`},
      sectionId:   {"S": "03"},
      SK:          {"S": `Lesson#01${postfix}#Section#03`},
      title:       {"S": "Attachments"},

      type:        {"S": "AttachmentWidget"},
      items:       {"L": [
        {
          "M": {
            "id":       {"S":  "01"},
            "fileName": {"S": "spec.pdf"},
            "fileKey":  {"S": "doesnt exist"},
          }
        }
      ]}
    },
  ]);
  console.log(`    created sections in Org: ${organizationId.S}`);

  // lessons
  await createBulk([
    {
      organizationId,
      courseId:    {"S": `01${postfix}`},
      lessonId:    {"S": `01${postfix}`},
      SK:          {"S": `Lesson#01${postfix}`},
      title:       {"S": "MyLesson"},
      description: {"S": "Description"},
      dirty:       {"BOOL": false},
      rev:         {"N": "0"},
      content:     {"L": [
        {"S": "01"},
        {"S": "02"},
        {"S": "03"}
      ]}
    },
    {
      organizationId,
      courseId:    {"S": `01${postfix}`},
      lessonId:    {"S": `02${postfix}`},
      SK:          {"S": `Lesson#02${postfix}`},
      title:       {"S": "MySecondLesson"},
    },
    {
      organizationId,
      courseId:    {"S": `01${postfix}`},
      lessonId:    {"S": `03${postfix}`},
      SK:          {"S": `Lesson#03${postfix}`},
      title:       {"S": "Homework1"},
    },
    {
      organizationId,
      courseId:    {"S": `01${postfix}`},
      lessonId:    {"S": `00${postfix}`},
      SK:          {"S": `Lesson#00${postfix}`},
      title:       {"S": "Homework1"},
    },
  ]);
  console.log(`    created lessons in Org: ${organizationId.S}`);

  // users into Org
  await createBulk([
    {
      organizationId,
      SK:             { "S": `Student#03` },
      userId:         { "S": `03` },
      createdAt:      { "S": moment().format() },
    },
    {
      organizationId,
      SK:             { "S": `Student#04` },
      userId:         { "S": `04` },
      createdAt:      { "S": moment().format() },
    }
  ], config.db.tableName);
  console.log(`    added users in Org: ${organizationId.S}`);
}

async function deleteTable(tableName) {
  const params = { TableName: tableName };
  try {
    await dynamodb.deleteTable(params).promise();
  } catch (e) {
    console.log(e);
  }
}
async function createTable(tableName) {
  const params = require(`./${tableName}.db.js`);
  return dynamodb.createTable(params).promise();
}

async function createBulk(list, tableName=config.db.tableName) {
  const chunks = _.chunk(list, 25);
  const qs = _.map(chunks, async chunk => {
    return dynamodb.batchWriteItem({
        RequestItems: {
          [tableName]: _.map(chunk, item => ({ PutRequest: { Item: item } }))
        }
      }).promise();
  });

  return Promise.all(qs);
}
