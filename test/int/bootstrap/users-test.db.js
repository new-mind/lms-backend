const config = require('config');

module.exports = {
  AttributeDefinitions: [
    {
      AttributeName: "id",
      AttributeType: "S"
    },
    {
      AttributeName: "email",
      AttributeType: "S"
    }
  ],
  KeySchema: [
    {
      AttributeName: "id",
      KeyType: "HASH"
    }
  ],
  GlobalSecondaryIndexes: [
    {
      IndexName: "ByEmail",
      KeySchema: [
        {
          AttributeName: "email",
          KeyType: "HASH"
        }
      ],
      Projection: {
        ProjectionType: "ALL"
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: "5",
        WriteCapacityUnits: "5"
      },
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: "5",
    WriteCapacityUnits: "5"
  },
  TableName: config.db.userTableName
}
