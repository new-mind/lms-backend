const gql = require('graphql-tag');
const { query, mutate } = require('./db');
const { expect } = require('chai');

const GET_USER = gql`
  query GetUser($userId: ID!) {
    getUser(id: $userId) {
      id
      email
      name
      surname
    }
  }
`;

const UPDATE_USER = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      id
      name
      surname
      phone
      age
    }
  }
`;

describe('User', () => {
  it('gets user', async () => {
    const res = await query({
      query: GET_USER,
      variables: {
        userId: "05"
      }
    });

    expect(res.errors, res.errors).to.be.undefined;
    expect(res.data.getUser.email).to.equal("arnold1@eduway.today");
    expect(res.data.getUser.name).to.equal("Arnold");
    expect(res.data.getUser.surname).to.equal("Shwarz");
  });

  it('updates user', async () => {
    const name = "A.";
    const surname = "S.";
    const age = "50";
    const phone = "+1111111111111";
    const res = await mutate({
      query: UPDATE_USER,
      variables: {
        input: {
          id: "05",
          name,
          surname,
          age,
          phone
        }
      }
    });

    expect(res.errors, res.errors).to.be.undefined;
    const { updateUser } = res.data;
    expect(updateUser.name).to.equal(name);
    expect(updateUser.surname).to.equal(surname);
    expect(updateUser.age).to.equal(age);
    expect(updateUser.phone).to.equal(phone);
  });
});
