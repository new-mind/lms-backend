const gql = require('graphql-tag');
const { query, mutate } = require('../db');
const { expect } = require('chai');

const lessonId = "01"
const { organizationId } = require('../constant');
const fragments = require('@lms/schema/Fragments.graphql.js');

const CREATE_SECTION = gql`
  mutation AddSection($lessonId: ID!, $input: CreateSectionWithPosInput!) {
    Lesson(id: $lessonId) {
      addSection(input: $input) {
        position
          section {
          ...MultiChoiceWidget
        }
      }
    }
  }
  ${fragments.MultiChoiceWidget}
`;

const DELETE_SECTION = gql`
  mutation DeleteSection($lessonId: ID!, $id: ID!) {
    Lesson(id: $lessonId) {
      deleteSection(id: $id) {
        section {
          ...MultiChoiceWidget
        }
      }
    }
  }
  ${fragments.MultiChoiceWidget}
`;

const UPDATE_SECTION = gql`
  mutation UpdateSection($input: UpdateSectionInput!) {
    updateSection(input: $input) {
      ...MultiChoiceWidget
    }
  }
  ${fragments.MultiChoiceWidget}
`;

describe('MultiChoiceWidget', () => {
  async function deleteSection(section) {
    const res = await mutate({
      query: DELETE_SECTION,
      variables: {
        lessonId: lessonId,
        id: section.id
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
  }

  it('creates', async () => {
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'MultiChoiceWidget'
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.answers).to.be.lengthOf(0);
    await deleteSection(section);
  });

  it('creates with 1 answer', async () => {
    const correct = true;
    const text = "title1";
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'MultiChoiceWidget',
            answers: [{ id: "01", correct, text }]
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.answers).to.have.lengthOf(1);
    expect(section.answers[0].correct).to.equal(correct);
    expect(section.answers[0].text).to.equal(text);
    await deleteSection(section);
  });

  it('creates with 2 answers', async () => {
    const question = "How much is the fish?";
    const text1 = "cheap";
    const text2 = "expenisve";
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'MultiChoiceWidget',
            question,
            answers: [
              { id: "01", correct: true, text: text1 },
              { id: "02", correct: false }
            ]
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.question).to.equal(question);
    expect(section.answers).to.have.lengthOf(2);
    expect(section.answers[0].correct).to.be.true;
    expect(section.answers[0].text).to.equal(text1);
    expect(section.answers[1].correct).to.false;
    expect(section.answers[1].text).to.be.null;
    await deleteSection(section);
  });

  describe('update', () => {
    let sectionId;
    let title = "Check the correct answer";
    let question = "How much is the fish?"

    beforeEach(async () => {
      const res = await mutate({
        query: CREATE_SECTION,
        variables: {
          lessonId,
          input: {
            section: {
              type: 'MultiChoiceWidget',
              title,
              question,
              answers: [{
                id: "01",
                correct: true,
                text: "FREE"
              }]
            }
          }
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
      sectionId = res.data.Lesson.addSection.section.id;
    });

    afterEach(async () => {
      const res = await mutate({
        query: DELETE_SECTION,
        variables: {
          id: sectionId,
          lessonId
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
    });

    it('set correct=true', async () => { // TODO: it overrides text
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'MultiChoiceWidget',
        answers: [{
          id: "01",
          correct: true
        }]
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      const {data: {updateSection}} = res;
      expect(updateSection.title).to.equal(title);
      expect(updateSection.question).to.equal(question);
      expect(updateSection.answers[0].correct).to.be.true;
      expect(updateSection.answers[0].id).to.equal("01");
      expect(updateSection.answers[0].text).to.be.null; //TODO
    });
  });
});
