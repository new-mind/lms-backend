const gql = require('graphql-tag');
const { query, mutate } = require('../db');
const { expect } = require('chai');

const lessonId = "01"
const { organizationId } = require('../constant');
const fragments = require('@lms/schema/Fragments.graphql.js');

describe('LinkWidget', () => {
  const CREATE_SECTION = gql`
    mutation AddSection($lessonId: ID!, $input: CreateSectionWithPosInput!) {
      Lesson(id: $lessonId) {
        addSection(input: $input) {
          position
            section {
            ...LinkWidget
          }
        }
      }
    }
    ${fragments.LinkWidget}
  `;
  const DELETE_SECTION = gql`
    mutation DeleteSection($lessonId: ID!, $id: ID!) {
      Lesson(id: $lessonId) {
        deleteSection(id: $id) {
          section {
            ...LinkWidget
          }
        }
      }
    }
    ${fragments.LinkWidget}
  `;

  async function deleteSection(section) {
    const res = await mutate({
      query: DELETE_SECTION,
      variables: {
        lessonId: lessonId,
        id: section.id
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
  }

  it('creates', async () => {
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'LinkWidget'
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.items).to.be.lengthOf(0);
    await deleteSection(section);
  });

  it('creates with 1 link', async () => {
    const url = "http://test.link";
    const title = "title1";
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'LinkWidget',
            items: [{ id: "01", url, title }]
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.items).to.have.lengthOf(1);
    expect(section.items[0].url).to.equal(url);
    expect(section.items[0].title).to.equal(title);
    await deleteSection(section);
  });

  it('creates with 2 links', async () => {
    const url1 = "http://test.link";
    const title1 = "title1";
    const url2 = "http://test2.link";
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'LinkWidget',
            items: [
              { id: "01", url: url1, title: title1 },
              { id: "02", url: url2 }
            ]
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.items).to.have.lengthOf(2);
    expect(section.items[0].url).to.equal(url1);
    expect(section.items[0].title).to.equal(title1);
    expect(section.items[1].url).to.equal(url2);
    expect(section.items[1].title).to.be.null;
    await deleteSection(section);
  });

  describe('update', () => {
    let sectionId;
    let title = "Links";
    let url = "https://test.eduway.today/link1";
    let urlTitle = "First Link";

    const UPDATE_SECTION = gql`
      mutation UpdateSection($input: UpdateSectionInput!) {
        updateSection(input: $input) {
          ...LinkWidget
        }
      }
      ${fragments.LinkWidget}
    `;

    beforeEach(async () => {
      const res = await mutate({
        query: CREATE_SECTION,
        variables: {
          lessonId,
          input: {
            section: {
              type: 'LinkWidget',
              title,
              items: [{
                id: "01",
                url,
                title: urlTitle
              }]
            }
          }
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
      sectionId = res.data.Lesson.addSection.section.id;
    });

    afterEach(async () => {
      const res = await mutate({
        query: DELETE_SECTION,
        variables: {
          id: sectionId,
          lessonId
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
    });

    it('updates url', async () => { // TODO: it overrides title
      const url = "https://test.eduway.today/link2";
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'LinkWidget',
        items: [{
          id: "01",
          url
        }]
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      const {data: {updateSection}} = res;
      expect(updateSection.title).to.equal(title);
      expect(updateSection.items[0].url).to.equal(url);
      expect(updateSection.items[0].id).to.equal("01");
      expect(updateSection.items[0].title).to.be.null; //TODO
    });
  });
});
