const gql = require('graphql-tag');
const { query, mutate } = require('../db');
const { expect } = require('chai');

const lessonId = "01"
const { organizationId } = require('../constant');
const fragments = require('@lms/schema/Fragments.graphql.js');

describe('TextWidget', () => {
  const CREATE_SECTION = gql`
    mutation AddSection($lessonId: ID!, $input: CreateSectionWithPosInput!) {
      Lesson(id: $lessonId) {
        addSection(input: $input) {
          position
            section {
            ...TextWidget
          }
        }
      }
    }
    ${fragments.TextWidget}
  `;
  const DELETE_SECTION = gql`
    mutation DeleteSection($lessonId: ID!, $id: ID!) {
      Lesson(id: $lessonId) {
        deleteSection(id: $id) {
          section {
            ...TextWidget
          }
        }
      }
    }
    ${fragments.TextWidget}
  `;
  async function deleteSection(section) {
    const res = await mutate({
      query: DELETE_SECTION,
      variables: {
        lessonId: lessonId,
        id: section.id
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
  }
  it('creates', async () => {
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'TextWidget'
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.data).to.be.null;
    await deleteSection(section);
  });

  it('creates with data', async () => {
    const data = "There is a data";
    const input = {
      lessonId,
      organizationId,
      type: 'TextWidget',
      data
    }
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'TextWidget',
            data
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.data).to.equal(data);
    await deleteSection(section);
  });

  describe('update', () => {
    let sectionId;
    const UPDATE_SECTION = gql`
      mutation UpdateSection($input: UpdateSectionInput!) {
        updateSection(input: $input) {
          ...TextWidget
        }
      }
      ${fragments.TextWidget}
    `;
    before(async () => {
      const res = await mutate({
        query: CREATE_SECTION,
        variables: {
          lessonId,
          input: {
            section: {
              type: 'TextWidget',
              data: "There is a data"
            }
          }
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
      sectionId = res.data.Lesson.addSection.section.id;
    });
    it('updates data', async () => {
      const data = "Updated data";
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'TextWidget',
        data
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      expect(res.data.updateSection.data).to.equal(data);
    });
    it('sets title', async () => {
      const title = "Text title";
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'TextWidget',
        title
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      expect(res.data.updateSection.title).to.equal(title);
      expect(res.data.updateSection.data).to.equal("Updated data");
    });
    it('removes title', async () => {
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'TextWidget',
        title: null
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      expect(res.data.updateSection.title).to.be.null;
      expect(res.data.updateSection.data).to.equal("Updated data");
    });
    it('removes data', async () => {
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'TextWidget',
        data: null
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      expect(res.data.updateSection.data).to.be.null;
    });
  });
});
