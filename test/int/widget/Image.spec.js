const gql = require('graphql-tag');
const { query, mutate } = require('../db');
const { expect } = require('chai');

const lessonId = "01"
const { organizationId } = require('../constant');
const fragments = require('@lms/schema/Fragments.graphql.js');

describe('ImageWidget', () => {
  const CREATE_SECTION = gql`
    mutation AddSection($lessonId: ID!, $input: CreateSectionWithPosInput!) {
      Lesson(id: $lessonId) {
        addSection(input: $input) {
          position
            section {
            ...ImageWidget
          }
        }
      }
    }
    ${fragments.ImageWidget}
  `;
  const DELETE_SECTION = gql`
    mutation DeleteSection($lessonId: ID!, $id: ID!) {
      Lesson(id: $lessonId) {
        deleteSection(id: $id) {
          section {
            ...ImageWidget
          }
        }
      }
    }
    ${fragments.ImageWidget}
  `;

  async function deleteSection(section) {
    const res = await mutate({
      query: DELETE_SECTION,
      variables: {
        lessonId: lessonId,
        id: section.id
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    return res;
  }

  it('creates', async () => {
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'ImageWidget'
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.title).to.be.null;
    await deleteSection(section);
  });

  it('creates with url', async () => {
    const url = "http://image.link"
    const input = {
      lessonId,
      organizationId,
      type: 'ImageWidget',
      url
    }
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'ImageWidget',
            url
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.title).to.be.null;
    expect(section.width).to.be.null;
    expect(section.height).to.be.null;
    expect(section.url).to.equal(url);
    await deleteSection(section);
  });

  it('creates with title', async () => {
    const title = "title of my image";
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'ImageWidget',
            title
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.url).to.be.null;
    expect(section.width).to.be.null;
    expect(section.height).to.be.null;
    expect(section.title).to.equal(title);
    await deleteSection(section);
  });

  it('creates with width and height', async () => {
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'ImageWidget',
            width: '500',
            height: '500'
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.url).to.be.null;
    expect(section.width).to.equal("500");
    expect(section.height).to.equal("500");
    await deleteSection(section);
  });

  it('creates with auto width', async () => {
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'ImageWidget',
            width: "auto"
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.url).to.be.null;
    expect(section.width).to.equal("auto");
    expect(section.height).to.be.null;
    await deleteSection(section);
  });

  describe('update', () => {
    let sectionId;
    let url = "https://test.eduway.today/image.png";
    let width = "300";
    let height = "300";
    let title = "Image";

    const UPDATE_SECTION = gql`
      mutation UpdateSection($input: UpdateSectionInput!) {
        updateSection(input: $input) {
          ...ImageWidget
        }
      }
      ${fragments.ImageWidget}
    `;

    beforeEach(async () => {
      const res = await mutate({
        query: CREATE_SECTION,
        variables: {
          lessonId,
          input: {
            section: {
              type: 'ImageWidget',
              title,
              url,
              width,
              height
            }
          }
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
      sectionId = res.data.Lesson.addSection.section.id;
    });

    afterEach(async () => {
      const res = await mutate({
        query: DELETE_SECTION,
        variables: {
          id: sectionId,
          lessonId
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
    });

    it('updates url', async () => {
      const url = "https://test.eduway.today/updated_image.png";
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'ImageWidget',
        url
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      const {data: {updateSection}} = res;
      expect(updateSection.url).to.equal(url);
      expect(updateSection.title).to.equal(title);
      expect(updateSection.width).to.equal(width);
      expect(updateSection.height).to.equal(height);
    });

    it('updates title', async () => {
      const title = "New title";
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'ImageWidget',
        title
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      const {data: {updateSection}} = res;
      expect(updateSection.url).to.equal(url);
      expect(updateSection.title).to.equal(title);
      expect(updateSection.width).to.equal(width);
      expect(updateSection.height).to.equal(height);
    });

    it('updates width and height', async () => {
      const width = "707";
      const height = "808";
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'ImageWidget',
        width,
        height
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      const {data: {updateSection}} = res;
      expect(updateSection.url).to.equal(url);
      expect(updateSection.title).to.equal(title);
      expect(updateSection.width).to.equal(width);
      expect(updateSection.height).to.equal(height);
    });

    it('removes width and height', async () => {
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'ImageWidget',
        width: null,
        height: null
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      const {data: {updateSection}} = res;
      expect(updateSection.url).to.equal(url);
      expect(updateSection.title).to.equal(title);
      expect(updateSection.width).to.be.null;
      expect(updateSection.height).to.be.null;
    });
  });
});
