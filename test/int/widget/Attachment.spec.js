const gql = require('graphql-tag');
const { query, mutate } = require('../db');
const { expect } = require('chai');

const lessonId = "01"
const { organizationId } = require('../constant');
const fragments = require('@lms/schema/Fragments.graphql.js');

describe('AttachmentWidget', () => {
  const CREATE_SECTION = gql`
    mutation AddSection($lessonId: ID!, $input: CreateSectionWithPosInput!) {
      Lesson(id: $lessonId) {
        addSection(input: $input) {
          position
          section {
            ...AttachmentWidget
          }
        }
      }
    }
    ${fragments.AttachmentWidget}
  `;
  const DELETE_SECTION = gql`
    mutation DeleteSection($lessonId: ID!, $id: ID!) {
      Lesson(id: $lessonId) {
        deleteSection(id: $id) {
          section {
            ...AttachmentWidget
          }
        }
      }
    }
    ${fragments.AttachmentWidget}
  `;

  async function deleteSection(section) {
    const res = await mutate({
      query: DELETE_SECTION,
      variables: {
        lessonId: lessonId,
        id: section.id
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
  }

  it('creates', async () => {
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'AttachmentWidget'
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.title).to.be.null;
    await deleteSection(section);
  });

  it('creates with title', async () => {
    const title = "My attachment";
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'AttachmentWidget',
            title
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.title).to.equal(title);
    await deleteSection(section);
  });

  it('creates with 1 item', async () => {
    const fileKey = "fileKey";
    const fileName = "fileName";
    const title = "file title";
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'AttachmentWidget',
            items: [ { id: "01", fileKey, fileName, title } ]
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.items).to.be.lengthOf(1);
    expect(section.items[0].fileKey).to.equal(fileKey);
    expect(section.items[0].fileName).to.equal(fileName);
    expect(section.items[0].title).to.equal(title);
    await deleteSection(section);
  });

  it('creates with 2 items', async () => {
    const fileKey1 = "fileKey1";
    const fileName1 = "fileName1";
    const fileKey2 = "fileKey2";
    const fileName2 = "fileName2";
    const title = "file title";
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'AttachmentWidget',
            items: [
              { id: "01", fileKey: fileKey1, fileName: fileName1, title },
              { id: "02", fileKey: fileKey2, fileName: fileName2 }
            ]
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.items).to.be.lengthOf(2);
    expect(section.items[0].fileKey).to.equal(fileKey1);
    expect(section.items[0].fileName).to.equal(fileName1);
    expect(section.items[0].title).to.equal(title);
    expect(section.items[1].fileKey).to.equal(fileKey2);
    expect(section.items[1].fileName).to.equal(fileName2);
    await deleteSection(section);
  });

  it('creates with identityId', async () => {
    const fileKey = "fileKey";
    const fileName = "fileName";
    const title = "file title";
    const identityId = "11111111111"
    const res = await mutate({
      query: CREATE_SECTION,
      variables: {
        lessonId,
        input: {
          section: {
            type: 'AttachmentWidget',
            items: [ { id: "01", fileKey, fileName, title, identityId } ]
          }
        }
      }
    });
    expect(res.errors, res.errors).to.be.undefined;
    const {section} = res.data.Lesson.addSection;
    expect(section.items).to.be.lengthOf(1);
    expect(section.items[0].fileKey).to.equal(fileKey);
    expect(section.items[0].fileName).to.equal(fileName);
    expect(section.items[0].title).to.equal(title);
    expect(section.items[0].identityId).to.equal(identityId);
    await deleteSection(section);
  });

  describe('update', () => {
    let sectionId;
    let title = "Attachments";
    let fileKey = "fileKey";
    let fileName = "fileName";
    let fileTitle = "fileTitle";

    const UPDATE_SECTION = gql`
      mutation UpdateSection($input: UpdateSectionInput!) {
        updateSection(input: $input) {
          ... on AttachmentWidget {
            id
            title
            items {
              id
              title
              fileKey
              fileName
            }
          }
        }
      }
    `;

    beforeEach(async () => {
      const res = await mutate({
        query: CREATE_SECTION,
        variables: {
          lessonId,
          input: {
            section: {
              type: 'AttachmentWidget',
              title,
              items: [{
                id: "01",
                title: fileTitle,
                fileName,
                fileKey
              }]
            }
          }
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
      sectionId = res.data.Lesson.addSection.section.id;
    });

    afterEach(async () => {
      const res = await mutate({
        query: DELETE_SECTION,
        variables: {
          id: sectionId,
          lessonId
        }
      });
      expect(res.errors, res.errors).to.be.undefined;
    });

    it('updates fileName', async () => { // TODO: it overrides title, fileKey
      const fileName = "my new fileName";
      const input = {
        id: sectionId,
        lessonId,
        organizationId,
        type: 'AttachmentWidget',
        items: [{
          id: "01",
          fileName
        }]
      };
      const res = await mutate({query: UPDATE_SECTION, variables: {input}});
      expect(res.errors, res.errors).to.be.undefined;
      const {data: {updateSection}} = res;
      expect(updateSection.title).to.equal(title);
      expect(updateSection.items[0].fileName).to.equal(fileName);
      expect(updateSection.items[0].id).to.equal("01");
      expect(updateSection.items[0].title).to.be.null; //TODO
      expect(updateSection.items[0].fileKey).to.be.null; //TODO
    });
  });
});
