FROM node:latest

RUN mkdir -p /usr/app
WORKDIR /usr/app

COPY package.json package-lock.json ./
RUN npm ci

COPY config ./config
COPY src ./src

EXPOSE 3000
CMD ["npm", "start"]
